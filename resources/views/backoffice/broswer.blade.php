<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Browser</title>
    
    <link href="{{ asset('backoffice_resource/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('backoffice_resource/dist/css/sb-admin-2.css') }}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{ asset('backoffice_resource/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.blImageCenter.js')}}"></script>
    <script type='text/javascript'>
        function imgcenterrecall()
        {
            $(".imgcenter-inside img").centerImage('inside').animate({"opacity":1}, "fast");
        }
        $(window).load(imgcenterrecall);
        var resizecount;
        $(window).resize(function(){
            clearInterval( resizecount );
            resizecount = setTimeout(imgcenterrecall, 100);
        });
    </script>


    <!-- MetisMenu CSS -->	
    <script >
    function getUrlParam(paramName)
    {
        var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
        var match = window.location.search.match(reParam) ;
         return (match && match.length > 1) ? match[1] : '' ;
    }
   
   
   function showimage(src){
        
         var funcNum = getUrlParam('CKEditorFuncNum');
         var fileUrl =src;
        window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
        window.close();
   }
   function deleteimage(div,key){
       
        if(confirm('ต้องการลบรูปภาพนี้ใช่หรือไม่ ?')){
            $.ajax({
            type:'POST',
            url: '{{ asset('browsermanagement/deleteimage') }}',
            data:{
                 _token: '{{ csrf_token()}}',
                key:key
            },
            success: function(data){               
                if(data){ // ลบ
                     $(div).parent().parent().parent().fadeOut('fast');
                }
                
            }
        });
        }
        
   }
  function checkfile(){
        if($('#images').val()=== null){
            return false;        
        }
        
    }
    </script>
    <style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.imgcenter-inside {
    display: inline-block;
    width: 240px;
    height: 180px;
}

/* Extra Small Devices, Phones */ 
@media only screen and (max-width : 480px) {
    .imgcenter-inside {
        width: 150px;
        height: 200px;
    }
}

@media only screen and (max-width : 1200px) {
    .imgcenter-inside {
        width: 280px;
        height: 150px;
    }
}
@media only screen and (max-width : 992px) {
    .imgcenter-inside {
        width: 200px;
        height: 150px;
    }
}
@media only screen and (max-width : 768px) {
    .imgcenter-inside {
        width: 200px;
        height: 200px;
    }
    .thumbnail {
        text-align: center;
    }
}

.imgcenter-inside img {
    opacity: 0;
    max-width: 100%;
}
.imgcenter-inside:hover {
    opacity: 0.8;
}
</style>
</head>
<body>

<div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand">Browse Image</a>
            </div> 
             <div class="navbar-right" style='margin-top:7px;margin-right: 15px;'>
             <?php 
                echo \Form::open(array('onsubmit'=>'return checkfile();','files' =>1,'url' => asset('browsermanagement/submitimage')));
                    echo '<span class="btn btn-default btn-file">Upload File &nbsp;<span class="glyphicon glyphicon glyphicon-folder-open"></span>';
                    echo \Form::file('images[]',array('multiple'=>true,'accept'=>'image/x-png, image/gif, image/jpeg','id'=>'images'));
                    echo '</span>';    
                    echo '&nbsp;';  
                    echo Form::submit('Submit',array('class'=>'btn btn-primary'));            
                echo \Form::close();                
             ?>
              
            </div> 
        </nav>
     <!-- /#page-wrapper -->

</div>
<br/>
<div class="container">
    <div class="row">
    <?php foreach ($result as $key => $value): ?>
         <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="thumbnail">
                <div align="right">
                    <span onclick='deleteimage(this,"{{$value['Key']}}")' style='cursor:pointer;' class="glyphicon glyphicon-remove" ></span>
                </div>
                <div class="imgcenter-inside">
                    <img onclick="showimage(this.src);" src="{{S3::get($value['Key'],$bucket)}}" style="cursor:pointer;">
                </div>
                <div >
                    <br/>
                </div>
            </div>
        </div>

    <?php endforeach ?>
        

    </div>
 </div>

</body>
</html>