@extends('backoffice/layout/main')

@section('body')
<?php 
	echo Form::open(array('url'=>asset('settingmanagement/bank'),"class"=>"form-horizontal"));
?>



<input name="table" type="hidden" value="setting">
<input name="success" type="hidden" value="settingmanagement/bank">

<input name="rule[sale_email]" type="hidden" value="required">
<div class="row">
	<div class="form-group">
		<label id="label_line" class="col-sm-2 control-label">Line</label>
		<div class="col-sm-6">
			<input id="line" class="form-control" placeholder="Line" name="data[line]" type="text" value="{{$result_data->line}}">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<label id="label_sale_email" class="col-sm-2 control-label">Sale_email</label>
		<div class="col-sm-6">
			<input id="sale_email" class="form-control" placeholder="Sale_email" name="data[sale_email]" type="text" value="{{$result_data->sale_email}}">
		</div>
	</div>
</div>
<hr>

<div class="row">
	<div class="form-group">
		<label id="label_bank" class="col-sm-2 control-label">Bank</label>
		<div  class="col-sm-6">
		<div id='bank'>
<?php 
	$arr_bank = json_decode($result_data->bank,true);

	foreach($arr_bank as $key => $value) {  
		
		?>
		<p>
		<input  class="form-control" placeholder="Bank" name="data[bank][]" type="text" value="{{$value}}">		
		</p>
		<?php
	}

?>
		
		</div>
		
		<a style='cursor:pointer;' onclick="bank_plus();">เพิ่ม</a> &nbsp;&nbsp;&nbsp;<a style='cursor:pointer;' onclick="bank_del();">ลบ</a>
		</div>
	</div>
</div>

	<hr>
<div class="row"><div class="form-group"><div class="col-sm-offset-2 col-sm-6"><input class="btn btn-primary" type="submit" value="Submit"></div></div></div>
<?php 
	echo Form::close();
?>


<script>
	function bank_plus(){
		$('#bank').append('<p><input  class="form-control" placeholder="Bank" name="data[bank][]" type="text"></p>');
	}
	function bank_del(){
		if($('#bank').children().length>1){
			$('#bank').children().last().remove();
		}
	}
</script>
@endsection