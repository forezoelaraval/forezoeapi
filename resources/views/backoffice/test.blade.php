@extends('backoffice/layout/main')

@section('body')
<br/>
 <div class="row">
    <div class="col-lg-4 col-md-4">
         <div class="panel panel-primary">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-user fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$arr_dashboard['customer']}}</div>
                         <div>Customers</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('customermanagement') }}">
                 <div class="panel-footer">
                     <span class="pull-left">View Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
     <div class="col-lg-4 col-md-4">
         <div class="panel panel-green">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-users fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$arr_dashboard['supplier']}}</div>
                         <div>Suppliers</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('suppliermangement/home') }}">
                 <div class="panel-footer">
                     <span class="pull-left">View Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
     <div class="col-lg-4 col-md-4">
         <div class="panel panel-yellow">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-cube fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$arr_dashboard['product']}}</div>
                         <div>Product</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('productmanagement') }}">
                 <div class="panel-footer">
                     <span class="pull-left">View Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
    
 </div>
<div class="row">
    <div class="col-sm-8">
    	<div class="chat-panel panel panel-default">
    		<div class="panel-heading">
    		    <i class="fa fa-comments fa-fw"></i>
    			    Review
    		</div>
    		 <!-- /.panel-heading -->
             <div class="panel-body" style='height:460px; overflow:auto;'>
	             <table class='table'>
	                <thead>
	                    <tr>                                         
	                        <th>สินค้า</th>                      
	                        <th>รีวิว</th> 
	                        <th>user</th> 
	                        
	                        <th></th>                                          
	                    </tr>           
	                </thead>
	                <tbody>
	                <?php foreach ($arr_review as $key => $value_product_review): ?>
	                	 <tr style="cursor:pointer;" class='clickable-row' data-href='{{ asset("productmanagement/review/".$value_product_review->product_id) }}'>
	                		<td>
	                			
	                			<img style='max-width:75px;' src='{{ asset($value_product_review->product_picture) }}'>
	                		
	                		</td>
	                		<td>
	                			{{$value_product_review->review}}
	                			<br/>
	                			<?php 
	                				echo Formate::board_date($value_product_review->created_at);
	                			?>
	                		</td>
	                		<td>
	                			{{$value_product_review->name}}
	                			<br/>
	                			{{$value_product_review->ip}}
	                		</td>
	                		<td></td>
	                		
	                		
	                	</tr>
	                <?php endforeach ?>
	                </tbody>
	            </table>
            </div>
    	</div>
 	</div>
 	<div class="col-sm-4">
		<div class="chat-panel panel panel-default">
	       <div class="panel-heading">
	      	    <i class="fa fa-user fa-fw"></i>
		           Recent Partners
		        	<a href='{{ asset('partnermanagement') }}'>
		            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
		            </a>
		    </div>
		    <div class="panel-body"  style='height:460px; overflow:auto;'>
		       <ul class="chat">
		              <?php foreach ($arr_partner as $key => $value): ?>
		                  <li class="left clearfix">
		                      <span class="chat-img pull-left">		                        
		                         <i style='color:#337ab7;' class="fa fa-user fa-3x"></i>
		                      
		                      </span>
		                      <div class="chat-body clearfix">
		                          <div class="header">
		                              <strong class="primary-font"><?php echo $value->name; ?></strong>
		                              <small>
		                              <a href='{{ asset('partnermanagement/profile/'.$value->id) }}'>
		                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
		               </a>
		                              </small>
		                           </div>
                                         <p >
                                            <?php echo $value->type; ?>
                                        </p>
                                        <hr style='margin: 1px;'>
                                        <p>
                                            <?php echo $value->email; ?>
                                        </p>
                                        <p>
                                            <?php echo $value->phone; ?>
                                        </p>
		                      </div>
		                  </li>
		              <?php endforeach ?>
		                  
		          </ul>
		      </div>
	    </div>
 	</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
@endsection