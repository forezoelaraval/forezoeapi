<div class="row"> 
    <div class="col-lg-12">
         <h1 class="page-header">{{ucfirst($title)}}</h1>
           <?php          
           if (isset($breadcrumb)){
          Backofficetemplate::breadcrumb($breadcrumb);
           }
            
          ?>

    	
    </div>
                <!-- /.col-lg-12 -->
</div>

<!-- Error -->
<div class="col-md-12">
  @if(Session::has('message'))
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('message') }}
    </div>
  @endif
  @if($errors->all())      
    <?php  $arr_error= $errors->all();  ?>   
     <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
        {{ $arr_error[0] }}
    </div>
  @endif
  @if(Session::has('error'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('error') }}
    </div>
  @endif
</div>
<!-- Error -->
<?php if(isset($button)){?>
<div class="row">
    <div class="col-md-12">
        <div class="btn-toolbar list-toolbar ">
           <?php Backofficetemplate::rightbutton($button); ?>   
        </div>

    </div>
</div>

<?php } ?>