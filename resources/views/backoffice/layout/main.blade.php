<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>FOREZOE | Admin</title>

    <!-- Favicons-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">

    <link href="{{ asset('backoffice_resource/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('backoffice_resource/bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ asset('backoffice_resource/dist/css/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('backoffice_resource/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('backoffice_resource/bower_components/morrisjs/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('backoffice_resource/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /#wrapper -->
    
     <!-- jQuery -->
     <script src="{{ asset('backoffice_resource/js/previewupload.js')}}"></script>
    <script src="{{ asset('backoffice_resource/bower_components/jquery/dist/jquery.min.js')}}"></script>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ asset('backoffice') }}">FOREZOE : ระบบจัดการข้อมูล</a>
            </div>
            <!-- /.navbar-header -->           
            @include('backoffice/layout/topnav')
           
            <!-- /.navbar-top-links -->
            @include('backoffice/layout/leftnav')

            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
             @include('backoffice/layout/headpage')
            <!-- /.row -->
                @yield('body')
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('backoffice_resource/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('backoffice_resource/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>

    <!-- Morris Charts JavaScript -->
 <script src="{{ asset('backoffice_resource/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>
 
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('backoffice_resource/dist/js/sb-admin-2.js')}}"></script>

</body>
</html>