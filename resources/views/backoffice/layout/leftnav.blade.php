 <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                     
                       

                        <li id='nav-transaction'>
                            <a id='nav-transaction-a' href="{{ asset('transactionmanagement') }}"><i class="fa fa-file-text-o "></i> Transaction</a>
                           
                            <!-- /.nav-second-level -->
                        </li>
                        <li id='nav-user'>
                            <a id='nav-user-a' href="#"><i class="fa fa-user"></i> All User<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                 <li>
                                    <a id='nav-user-1' href="{{ asset('usermanagement') }}">user</a>
                                </li>
                                <li>
                                    <a id='nav-user-2' href="{{ asset('idolmanagement') }}">idol</a>
                                </li>                               
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li id='nav-product'>
                            <a id='nav-user-a' href="#"><i class="fa fa-cube"></i> Shop & Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                 <li>
                                    <a id='nav-product-1' href="{{ asset('productmanagement') }}">Product</a>
                                </li>
                                <li>
                                    <a id='nav-product-3' href="{{ asset('shopmanagement') }}">Shop</a>
                                </li>                               
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                       
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
</div>

<?php
    if(isset($sidebar)){
        Backofficetemplate::activeSidebar($sidebar);
    }
?>
