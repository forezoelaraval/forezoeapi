@extends('backoffice/layout/main')

@section('body')

<div class='row' align="right">	

</div>
<div class='row'>	
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>ชื่อ</th>
				<th>รายละเอียด</th>
				<th>สถานะ</th>
				<th>เวลา</th>
			</tr>
		</thead>
		<tbody>
		<?php 
				$i=1;
		?>
			@foreach ($result_data as $value_order)
			<?php

				$arr_description = json_decode($value_order->description,true);
			?>
			<tr class="">
				<td>
				<?php echo $i++;?>
                </td>
				<td><?php echo $arr_description['user_name']?></td>
				<td><?php echo $arr_description['notes']?></td>
				<td>{{Formate::order_status($arr_description['status'])}} </td>
				<td>
					<?php 
                    	echo \Formate::board_date($value_order->created_at);
                    ?>
                </td>
				<td>
					
				</td>
			</tr>
	
			@endforeach
		
		</tbody>
	</table>

	<?php
			//echo '<div align="center">'.str_replace('/?', '?',$result_data->render()).'</div>';
		
		?>
</div>

 
@endsection