@extends('backoffice/layout/main')
@section('body')




<div class='row'>	
	<div class='col-xs-8'>	
		<table class='table'>
			<thead>	
				<tr>	
					<th>#</th>
					<th width="150px;">รูปภาพ</th>
					<th>สินค้า</th>
					<th>ราคา</th>
					<th>จำนวน</th>
					<th>รวม</th>
				</tr>
			</thead>
			<tbody>	
			<?php 
			$i=1;
			?>
			@foreach ($arr_cart_list as $value_cart_list)
				<tr>	
					<td>{{$i++}}</td>
					<td><img style='width:150px;' src="{{ asset($value_cart_list['picture']) }}" alt=""></td>
					<td>{{ number_format($value_cart_list['price'],2) }}</td>
					<td>
						<?php
							echo '<b>'.$value_cart_list['title'].'</b>';
						echo '</small>';
						?>
					</td>
					
					<td>{{$value_cart_list['qty']}}</td>
					<td>{{ number_format($value_cart_list['price']*$value_cart_list['qty'],2) }}</td>
				</tr>
				<?php 
				$total = $total + $value_cart_list['price']*$value_cart_list['qty'];
				?>
			@endforeach
			</tbody>
		
		</table>
		<?php 
		
		?>
	</div>	
	<div class='col-xs-4'>

		<h2>{{Formate::order_status($result_data->status)}} </h2>	
		<?php 
			echo \Form::open(array('url'=>asset('order/changestatus'),'onsubmit'=>'return check_form();'));
			echo \Form::hidden('id',$result_data->id);

			echo \Form::select('status',array('pendding'=>'คำสั่งซื้อใหม่','processing'=>'กำลังดำเนินการ','completed'=>'เสร็จสิ้น','cancelled'=>'ยกเลิก'),$result_data->status);
			echo '<br/>';
			echo \Form::text('order_status[user_name]',null,array('id'=>'form_user_name','style'=>'width:100%;','placeholder'=>'กรุณากรอกชื่อ'));
			echo '<br/>';
			echo \Form::textarea('order_status[notes]',null,array('id'=>'form_notes','style'=>'width:100%;height:75px;','placeholder'=>'กรุณากรอกรายละเอียด'));
		?>
		
			<input type="submit" value="แก้ไข">
			</form>
			<hr>	
			<table>
			<tr>	
					<th><h4>ยอมรวม<h4></th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo number_format($total); ?> .-</td>
			</tr>

			<tr>	
					<th><h4>ภาษี<h4></th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo number_format($total*$vat);?> .-</td>
			</tr>
			<tr>	
				<th><h4>รวม<h4></th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><b><?php echo number_format($total*(1+$vat)); ?> .-</b></td>
			</tr>
		</table>
  			order notes : {{$order_notes}}
		<hr>	
		<table>
		<tr>	
				<th>ชื่อ</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_profile['name'].' '.$arr_profile['lastname']; ?></td>
			</tr>
			<tr>	
				<th>บริษัท</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_profile['companyname']; ?></td>
			</tr>

			<tr>	
				<th>อีเมล์</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_profile['email']; ?></td>
			</tr>
			<tr>	
				<th>โทรศัพท์</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_profile['phone']; ?></td>
			</tr>
			
			<tr>	
				<th colspan="3">ที่อยู่</th>
			</tr>
			<tr>
				<td colspan="3"><p> &nbsp;<?php echo $arr_profile['address'].' '.$arr_profile['province'].' '.$arr_profile['postcode']; ?></p></td>
			</tr>
			
			
		</table>
		<h4><b>ข้อมูลใบกำกับภาษี</b></h4>
		<table>
			<tr>	
				<th>ชื่อ</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_vat['name'].' '.$arr_vat['lastname']; ?></td>
			</tr>
			<tr>	
				<th>บริษัท</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_vat['companyname']; ?></td>
			</tr>
			<tr>	
				<th colspan="3">เลขที่ประจำตัวผู้เสียภาษี</th>
			</tr>
			<tr>
				<td colspan="3"><p><?php echo $arr_vat['taxpayers']; ?></p></td>
			</tr>
	
			<tr>	
				<th>อีเมล์</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_vat['email']; ?></td>
			</tr>
			<tr>	
				<th>โทรศัพท์</th>
				<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
				<td><?php echo $arr_vat['phone']; ?></td>
			</tr>
			
			<tr>	
				<th colspan="3">ที่อยู่</th>
			</tr>
			<tr>
				<td colspan="3"><p> &nbsp;<?php echo $arr_vat['address'].' '.$arr_vat['province'].' '.$arr_vat['postcode']; ?></p></td>
			</tr>
			
			
		</table>
	</div>	
</div>
<script>	
function check_form(){
	if($('#form_user_name').val()==''){
		$('#form_user_name').focus();
		return false;
	}
	if($('#form_notes').val()==''){
		$('#form_notes').focus();
		return false;
	}
		 return true;
}
</script>
@endsection