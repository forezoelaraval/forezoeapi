@extends('backoffice/layout/main')

@section('body')

<?php 
	echo HTML::script('jquery-ui-1.11.4.custom/jquery-ui.js');
	echo HTML::style('jquery-ui-1.11.4.custom/jquery-ui.css');
	echo HTML::style('jquery-ui-1.11.4.custom/jquery-ui.theme.css');
	?>
<script>
  $(function() {
    $( "#birthday" ).datepicker({
    	changeMonth: true,
        changeYear: true,
         yearRange: "-100:+0",
        dateFormat:"yy-mm-dd"
    });
  });
  </script>
<?php
	Backofficetemplate::form($form);
?>

@endsection