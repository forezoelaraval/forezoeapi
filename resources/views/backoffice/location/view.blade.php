@extends('backoffice/layout/main')

@section('body')
<div class='row'>
	<h1>{{$result_data->title}}</h1>	
</div>
<hr>
<div class='row'>
	<div class='col-sm-12'>
		<?php 
		echo $result_data->description;
		?>
	</div>
	<br>
	<div class='col-sm-12'>
		<?php 
		echo $result_data->map_ifream;
		?>

	</div>
	<div class='col-sm-12' >
		Map url : <br/>
		<a href='{{$result_data->map_url}}' target="_blank">{{$result_data->map_url}}</a>
	</div>
</div>
@endsection'