@extends('backoffice/layout/main')

@section('body')
<br/>
<div class='row' >
	
<?php if (isset($search)): ?>
    <form action="{{ asset($search['post']) }}" style="padding-right:15px;" method="get" >
  <div class="input-group pull-right" style="width:300px;">
  
      <input type="text" class="form-control" name='search' placeholder="{{$search['placeholder']}}">
      <span class="input-group-btn">
        <input value="Search" class="btn btn-primary" type="submit" />
      </span>

    </div><!-- /input-group -->
</form>
<?php endif ?>

		
			
		
</div>
<div class="row"> 
    <div class="col-lg-12">
 		<?php Backofficetemplate::table($table); ?>
    </div>
</div>
 
 @endsection
