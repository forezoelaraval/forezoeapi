@extends('backoffice/layout/main')

@section('body')
<div class="row">
    <div class="col-lg-10">
        <?php
        echo HTML::script('ckeditor/ckeditor.js');
        ?>


{!! Form::open() !!}	
<?php
echo Form::textarea('test','',array('id'=>'content','class'=>'form-control'));	
?>
		{!! Form::close() !!}



	<script>

	 var config = {	
			height: 300,
			filebrowserBrowseUrl: '{{ asset('browsermanagement') }}',
			toolbar : [

			 { name: 'basicstyles', items: [ 'Bold', 'Italic'] },
			 { name: 'basicstyles', items: [ 'NewPage'] },
			 { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
			 items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-' ,'Outdent', 'Indent','-', 'NumberedList', 'BulletedList', '-', '-', 'Blockquote', 'CreateDiv', '-'] },
			 { name: 'links', items: [ 'Link', 'Unlink' ] },
			 { name: 'document', items: [ 'Maximize','Source' ] },    		
    		'/',
    		{ name: 'styles', items: ['Format', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor'] },   			
   			{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule',  'SpecialChar', 'PageBreak' ] },
   			{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
			]
		};			
		CKEDITOR.skinName = 'bootstrapck';
		CKEDITOR.replace( 'content', config);
	</script>
	
    </div>
                <!-- /.col-lg-12 -->
</div>
@endsection