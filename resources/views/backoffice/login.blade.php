<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Forezoe | Admin</title>

     <link href="{{ asset('backoffice_resource/bower_components/bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
     <link href="{{ asset('backoffice_resource/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->	
</head>
<body>
	
    <div class="container">
    <br/>
                <br/>
                <br/>
                <br/>
        <div class="row">

            <div class="col-md-4 col-md-offset-4">
                
                    <div class="panel-heading">
                        <div  >
                        <h3><b>Forezoe Backoffice</b></h3>
                        <b>ระบบจัดการข้อมูลหลังบ้าน</b>
                        </div>
                    </div>
                  <div style='clear:both;'></div>
                    <div class="panel-body">
<?php
  if($errors->all()){
     $arr_error= $errors->all();  
     echo '<ul><li>'. $arr_error[0].'</li></ul>';
  }
  ?>
                        <form action="{{ asset('backoffice/login') }}" method="POST">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <fieldset>
                                <div class="form-group">
                                    <input value="admin@forzoe.com" class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input value='111111' class="form-control" placeholder="Password" name="password" type="password" value="">
                                    
                                </div>
                                  <hr></hr>
                                <!-- Change this to a button or input when using this as a form -->
                                <button  class="btn btn-lg btn-primary btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                
            </div>
        </div>
    </div>
     
    <!-- /#wrapper -->
	 <!-- jQuery -->
    <script src="{{ asset('backoffice_resource/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('backoffice_resource/bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
</body>
</html>