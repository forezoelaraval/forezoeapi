@extends('backoffice/layout/main')

@section('body')
<div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$arr_dashboard['customer']}}</div>
                                    <div>Customers</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ asset('customermanagement') }}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$arr_dashboard['supplier']}}</div>
                                    <div>Suppliers</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ asset('suppliermangement/home') }}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$arr_dashboard['trip']}}</div>
                                    <div>Trips</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ asset('tripmanagement') }}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-default" >
                        <div class="panel-heading">
                           
                                <i class="fa fa-list-alt"></i> Recent Transactions
                                 <a href='{{ asset('transaction') }}'>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                             </a>
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body" style='height:460px; overflow:auto;'>
                        <table class='table'>
                                <thead>
                                    <tr>                                         
                                        <th>หมายเลขการจอง</th>                      
                                        <th>ยอดเงิน</th> 
                                        <th>วันที่ / เวลา</th>  
                                        <th>สถานะ</th>                                          
                                        <th></th>                                          
                                                                                
                                    </tr>           
                                </thead>
                                <tbody>
                                <?php foreach ($arr_transaction as $key => $value): ?>
                                <tr style="cursor:pointer;" class='clickable-row' data-href='{{ asset("transaction/view?user_booking_id=".$value->user_booking_id) }}'>
               
                                        <td>                    
                                            <?php echo sprintf('%06d',$value->user_booking_id); ?>               
                                        </td>
                                       
                                        
                                        <td> 
                                        <?php 
                                            echo number_format($value->total_cost+$value->amount);
                                            ?>
                                        </td> 
                                        <td>
                                            <?php 
                                            echo $value->created_at;
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                            if($value->aprovel){
                                                echo 'ชำระเงินเรียบร้อย';
                                            }else{
                                                echo 'รอโอนเงิน';
                                            }
                                            ?>
                                        </td>
                                        
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                        </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                 <div class="col-sm-4">
                 <div class="chat-panel panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-fw"></i>
                            Recent Partners
                             <a href='{{ asset('requestmanagement') }}'>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                             </a>
                        </div>
                  <div class="panel-body"  style='height:460px; overflow:auto;'>
                     <ul class="chat">
                            <?php foreach ($arr_request as $key => $value): ?>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                      
                                       <i style='color:#337ab7;' class="fa fa-user fa-3x"></i>
                                      
                                    
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font"><?php echo $value->name.' '.$value->lastname; ?></strong>
                                            <small>
                                            <a href='{{ asset('requestmanagement/profile/'.$value->id) }}'>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                             </a>
                                            </small>
                                        </div>
                                         <p >
                                            <?php echo $value->title; ?>
                                        </p>
                                        <hr style='margin: 1px;'>
                                        <p>
                                            <?php echo $value->desciption; ?>
                                        </p>
                                    </div>
                                </li>
                            <?php endforeach ?>
                                
                        </ul>
                    </div>
                    </div>
                 </div>
            </div>
            <!-- /.row -->




<script type="text/javascript">
    jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>           
@endsection