@extends('backoffice/layout/main')

@section('body')
<script>
	function changestatus(value){
		if(value!=''){
			window.location.href = '{{ asset("order/home") }}'+'/'+value;	
		}
	}
	
</script>
<div class='row' align="right">	
<form action="{{ asset('order/home') }}" method="get">
<?php  
/*$arr_select = array(''=>'กรุณาเลือก','pendding'=>'คำสั่งซื้อใหม่','processing'=>'กำลังดำเนินการ','completed'=>'เสร็จสิ้น','cancelled'=>'ยกเลิก');
echo \Form::select('status',$arr_select,$status,array('onchange'=>'changestatus(this.value);','class'=>'form-control','style'=>'width:300px;'));
*/
?>
	
</form>
</div>
<div class='row'>	
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>ชื่อ </th>
				<th>โทรศัพท์ </th>
				<th>อีเมล์ </th>
				<th>สถานะ</th>
				<th>เวลา</th>
				<th width="250px"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($result_data as $value_order){
			$arr_profile = json_decode($value_order->profile,true);
			$class='';
			if($value_order->read==0){
				$class='info';
			}
		?>
			<tr class="{{$class}}">
				<td>
					<?php
                        echo  sprintf("%07d",$value_order->id);
                    ?>
                </td>
				<td>{{$arr_profile['name']}}</td>
				<td>{{$arr_profile['phone']}} </td>
				<td>{{$arr_profile['email']}} </td>
				<td>
					{{Formate::order_status($value_order->status)}}
				</td>
				<td>
					<?php 
                    	echo \Formate::board_date($value_order->created_at);
                    ?>
                </td>
				<td>
					<a target="_blank" title="Profile" href="{{ asset('order/view/'.$value_order->id) }}" class="btn btn-primary">
						<i class="fa fa-file-text-o"></i>
					</a>
					<button onclick="set_model_del({{$value_order->id}});" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash-o"></i></button>
				</td>
			</tr>
		<?php 
			}
		?>
			
		</tbody>
	</table>

	<?php
			echo '<div align="center">'.str_replace('/?', '?',$result_data->render()).'</div>';
		
		?>
</div>

 <?php 
 	$array['table']='order';
		$default_post='backoffice/del';
		
 ?>
 <script>
    function set_model_del(id){
        $('#model_del_id').val(id);
    }
 </script>
  <div class="modal small fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Delete Confirmation</h3>
                      </div>
                      <div class="modal-body" style="font-size:18px;">
                        <p class="error-text"><i class="fa fa-warning modal-icon"></i><span id='model-del-text'> &nbsp;Do you want to delete this item?</span></p>
                      </div>
                      <div class="modal-footer">                      
                      <form id="tab"  method="post"  action='<?php echo  asset($default_post); ?>'>
                       <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
                      <?php if(isset($array['table'])){ ?>
                        <input name='table' id='model_del_table' type='hidden' value='<?php echo $array['table'];?>'>
                      <?php } ?>
                      <?php if(isset($array['input'])){ 
                        foreach ($array['input'] as $key => $value) { ?>
                        <input name='<?php echo $key?>' id='<?php echo $key?>' type='hidden' value='<?php echo $value;?>'>
                       <?php }
                        ?>
                       
                      <?php } ?>
                      <input name='id' id='model_del_id' type='hidden' value=''>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-danger" type="submit">Delete</button>                        
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
@endsection