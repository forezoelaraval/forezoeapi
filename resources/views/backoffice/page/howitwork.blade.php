@extends('backoffice/layout/main')

@section('body')
 

<?php 
    $index= -1;

	echo Form::open(array('url' => asset('pagemanagement/aboutsubmit'), 'files' => 1,'class'=>'form-horizontal'));		
?>
    <?php 
    echo Form::hidden('page',$arr_howitwork['id']);
    

    foreach ($rule as $key => $value) {
         echo Form::hidden('rule['.$key.']',$value);
    }
    ?>
   
  
     @for ($i = 1; $i <=3 ; $i++)
     <h3>Column # {{$i}}</h3>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">tilte</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[title_'.$i.']',$arr_howitwork['title_'.$i],array('class'=>'form-control','placeholder'=>'title','id'=>'title_'.$i));
            ?>

            </div>
        </div>
    </div>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">description</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[description_'.$i.']',$arr_howitwork['description_'.$i],array('class'=>'form-control','placeholder'=>'title','id'=>'description_'.$i));
            ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-6">
            <div class='single_img' id="img_image_{{$i}}">
               <img style='max-width:100%;' class="img-rounded" src="{{S3::get($arr_howitwork['image_'.$i])}}">
            </div>
               <hr>
            <?php            
                echo Form::file('data[image_'.$i.']',array('onchange'=>'handleFiles(this.id,"img_image_'.$i.'")','id'=>'image_'.$i,'accept'=>'image/x-png, image/gif, image/jpeg'));
                echo Form::hidden('old[image_'.$i.']',$arr_howitwork['image_'.$i]);
                echo Form::hidden('file['.$index++.']','image_'.$i);
            ?>
            </div>
        </div>
    </div>
        <hr>
 @endfor


    

    


    
    <hr></hr>
	<div class="row">
		<div class="form-group">        				
        	<div class="col-sm-offset-2 col-sm-6">  
        	<?php 
      			echo Form::submit('Submit',array('class'=>'btn btn-primary'));	
      		?>
        	</div>
        </div>			   
    </div>
	<?php 
		echo Form::close();
	?>




    <?php
    echo HTML::script('ckeditor/ckeditor.js');
   
  ?>
  
  <script>
   // $('#title_1').parent().attr('class','col-sm-6');
 /*
     var config = { 
            height: 200,
            filebrowserBrowseUrl: '{{ asset('browsermanagement') }}',
            toolbar : [

             { name: 'basicstyles', items: [ 'Bold', 'Italic'] },
             { name: 'basicstyles', items: [ 'NewPage'] },
             { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
             items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-' ,'Outdent', 'Indent','-', 'NumberedList', 'BulletedList', '-', '-', 'Blockquote', 'CreateDiv', '-'] },

            ]
        };          
        CKEDITOR.skinName = 'bootstrapck';
        CKEDITOR.replace( 'title_1', config);
        CKEDITOR.replace( 'title_2', config);
        CKEDITOR.replace( 'title_3', config);
        CKEDITOR.replace( 'title_4', config);
        CKEDITOR.replace( 'description_1', config);
        CKEDITOR.replace( 'description_2', config);
        CKEDITOR.replace( 'description_3', config);
        CKEDITOR.replace( 'description_4', config);
     */
        
    </script>
@endsection