@extends('backoffice/layout/main')

@section('body')

<script src="{{ asset('jquery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
<script>	
$(document).ready(function () {
    $("#sortable" ).sortable({ containment: "#containment-wrapper",cursor: "move"});
    $("#sortable" ).bind( "sortupdate", function(event, ui) { 
    	 var sortable= $('#sortable').sortable('toArray');       
    	 console.log(sortable);
    	 $.ajax({
            url:'{{ asset('backoffice/ajaxorder') }}',
            data:{
              table:'trip_home',
              sortable:sortable,  
               _token: '{{ csrf_token()}}',
              },
             type:'POST',
             success:function(j){
             //  console.log(j);
             } 
         });
  	});
});
</script>
<div>
<div class="col-md-12">
        <div class="btn-toolbar list-toolbar ">
            <?php 
            echo Form::open();
            ?>
           <a href="http://localhost/hivesters/public/pagemanagement/insert" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
           <?php 
            echo Form::close();
            ?>
        </div>

    </div>
</div>
<div class="row"> 
    <div id='containment-wrapper' style='width:750px;'>
     	<div  class="row">
    		<div id ='sortable'>
     <?php foreach ($data as $key => $value): ?>
     			<div style="cursor:pointer;" id="{{$value->id}}" class='col-sm-4'>
     				<div class='thumbnail'>
     				<div >	
     				<?php 
     				echo '<img style="width:100%;" src="'.S3::get($value->picture).'" />';
     				?>
     				</div>
     				<h4>{{$value->title}}</h4>
     				</div>
     			</div>
     <?php endforeach ?>
			</div>
		</div>
    </div>
</div>
@endsection