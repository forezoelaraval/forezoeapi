@extends('backoffice/layout/main')

@section('body')
 

<?php 
    $index= -1;

	echo Form::open(array('url' => asset('pagemanagement/aboutsubmit'), 'files' => 1,'class'=>'form-horizontal'));		
?>
    <?php 
    echo Form::hidden('page',$arr_partner['id']);
    

    foreach ($rule as $key => $value) {
         echo Form::hidden('rule['.$key.']',$value);
    }
    ?>
    <!-- picture -->
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Banner</label>
            <div class="col-sm-6">
            <div class='single_img' id="img_background">
               <img style='max-width:100%;' class="img-rounded" src="{{S3::get($arr_partner['background'])}}">
            </div>
            <hr>
            <?php            
                echo Form::file('data[background]',array('id'=>'background','onchange'=>'handleFiles(this.id,"img_background")','accept'=>'image/x-png, image/gif, image/jpeg'));
                echo Form::hidden('old[background]',$arr_partner['background']);
                echo Form::hidden('file['.$index++.']','background');
            ?>
            </div>
        </div>
    </div>
  <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">head title</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[head_title]',$arr_partner['head_title'],array('class'=>'form-control','placeholder'=>'title'));
            ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">center title</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[center_title]',$arr_partner['center_title'],array('class'=>'form-control','placeholder'=>'title'));
            ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">center sub title</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[center_sub_title]',$arr_partner['center_sub_title'],array('class'=>'form-control','placeholder'=>'title'));
            ?>

            </div>
        </div>
    </div>
  <hr>
  <!-- picture -->
  
     @for ($i = 1; $i <=4 ; $i++)
     <h3>Topic # {{$i}}</h3>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">tilte</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[col_title_'.$i.']',$arr_partner['col_title_'.$i],array('class'=>'form-control','placeholder'=>'title','id'=>'col_title_'.$i));
            ?>

            </div>
        </div>
    </div>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">description</label>

            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[col_description_'.$i.']',$arr_partner['col_description_'.$i],array('class'=>'form-control','placeholder'=>'title','id'=>'col_description_'.$i));
            ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-6">
             <div class='single_img' id="img_col_image_{{$i}}">
               <img style='max-width:100%;' class="img-rounded" src="{{S3::get($arr_partner['col_image_'.$i])}}">
            </div>
            <hr>
            <?php            
                echo Form::file('data[col_image_'.$i.']',array('onchange'=>'handleFiles(this.id,"img_col_image_'.$i.'")','id'=>'col_image_'.$i,'accept'=>'image/x-png, image/gif, image/jpeg'));
                echo Form::hidden('old[col_image_'.$i.']',$arr_partner['col_image_'.$i]);
                echo Form::hidden('file['.$index++.']','col_image_'.$i);
            ?>
            </div>
        </div>
    </div>
        <hr>
 @endfor


     <!-- picture -->
     @for ($i = 1; $i <=8 ; $i++)
       <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Picture <?php echo $i; ?></label>
            <div class="col-sm-6">
             <div class='single_img' id="img_partnerpicture{{$i}}">
             <img style='max-width:100%;' class="img-rounded" src="{{S3::get($arr_partner['partnerpicture'.$i])}}">
             </div>
             <hr>
            <?php 
                echo Form::file('data[partnerpicture'.$i.']',array('onchange'=>'handleFiles(this.id,"img_partnerpicture'.$i.'")','id'=>'partnerpicture'.$i,'accept'=>'image/x-png, image/gif, image/jpeg'));
                echo Form::hidden('old[partnerpicture'.$i.']',$arr_partner['partnerpicture'.$i]);
                echo Form::hidden('file['.$index++.']','partnerpicture'.$i);
            ?>
            </div>
        </div>
    </div>
     @endfor

    


    
    <hr></hr>
	<div class="row">
		<div class="form-group">        				
        	<div class="col-sm-offset-2 col-sm-6">  
        	<?php 
      			echo Form::submit('Submit',array('class'=>'btn btn-primary'));	
      		?>
        	</div>
        </div>			   
    </div>
	<?php 
		echo Form::close();
	?>




    <?php
    echo HTML::script('ckeditor/ckeditor.js');
   
  ?>
  
  <script>
   // $('#col_title_1').parent().attr('class','col-sm-6');
 
     var config = { 
            height: 200,
            filebrowserBrowseUrl: '{{ asset('browsermanagement') }}',
            toolbar : [

             { name: 'basicstyles', items: [ 'Bold', 'Italic'] },
             { name: 'basicstyles', items: [ 'NewPage'] },
             { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
             items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-' ,'Outdent', 'Indent','-', 'NumberedList', 'BulletedList', '-', '-', 'Blockquote', 'CreateDiv', '-'] },

            ]
        };          
        CKEDITOR.skinName = 'bootstrapck';
        CKEDITOR.replace( 'col_title_1', config);
        CKEDITOR.replace( 'col_title_2', config);
        CKEDITOR.replace( 'col_title_3', config);
        CKEDITOR.replace( 'col_title_4', config);
        CKEDITOR.replace( 'col_description_1', config);
        CKEDITOR.replace( 'col_description_2', config);
        CKEDITOR.replace( 'col_description_3', config);
        CKEDITOR.replace( 'col_description_4', config);
     
        
    </script>
@endsection