@extends('backoffice/layout/main')

@section('body')
 
<script src="{{ asset('jquery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
<script>	
$(function() {
    
     $( "#sortable" ).sortable({ 
     	axis: "y",
     	containment: "#containment-wrapper",
     	cursor: "move"});
     	$("#sortable" ).bind( "sortupdate", function(event, ui) { 
     		 var sortable= $('#sortable').sortable('toArray');
     		// console.log(sortable);  
     		  $.ajax({
           		 url:'{{ asset('backoffice/ajaxorder') }}',
           		 data:{
              table:'trip_home',
              sortable:sortable,  
               _token: '{{ csrf_token()}}',
              },
             type:'POST',
             success:function(j){
               //console.log(j);
             } 
         });     
  		});
    $( "#sortable" ).disableSelection();
  });


</script>
<div>

<br/>
<div class='col-sm-12'>

<?php 
            echo Form::open(array('url' => asset('backoffice/submit'),'class'=>'form-horizontal'));
      echo Form::hidden('success','pagemanagement/triporder');           
      echo Form::hidden('table','trip_home');           
      echo Form::hidden('rule[trip_id]','required|unique:trip_home');
      echo Form::hidden('data[order]',$data->count()+1);  
      echo Form::hidden('data[trip_id]',null,array('id'=>'trip_id'));         
            ?>

<div class="row">
  <div class="form-group">
    <label id="label_recommend_trip_title" class="col-sm-2 control-label">
     Search
    </label>
    <div class="col-sm-6">
    <input id="recommend_trip" class="form-control" placeholder="Recommend_trip"  type="text" value="" onkeyup="search_trip(this.value)">
    </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <label id="label_recommend_trip_title" class="col-sm-2 control-label">Recommend Trip</label>
    <div class="col-sm-6">
    <input id="recommend_trip_title" class="form-control" placeholder="Recommend_trip_title"  type="text" value="" disabled="disabled">
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
      <input class="btn btn-primary" type="submit" value="Submit">
    </div>
  </div>
</div>
   <?php 

           
            echo Form::close();
            ?>

</div>

</div>
<div class="row"> 
    <div class="col-lg-12">
    
     
 		<table id='containment-wrapper'  class="table">
 			<thead>
 				<tr>
 					
 					<th class=''>Trip</th>
					<th width="250px"></th>
				</tr>
			</thead>
			<tbody id='sortable' class='row'>
			<?php foreach ($data as $key => $value): ?>
				<tr id='{{$value->id}}'>
					
					<td style="min-width:550px;">{{$value->title}}</td>
					<td>
						<button onclick="set_model_del({{$value->id}});" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash-o"></i></button>
						
					</td>
				</tr>
			<?php endforeach ?>
				
			</tbody>
			</table>
    </div>
</div>
<?php 
Backofficetemplate::modaldel(array('table'=>'trip_home'));
?>
<script>
 
  $('#recommend_trip_title').attr('disabled','1');
  $('#recommend_trip_title').parent().append('<div align="right" onclick="deletetrip();" style="cursor:pointer;">Remove trip <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>');
  
  $('#recommend_trip').parent().append('<div id="show_trip" style="display:none;" class="user_search"></div>');
  $('#recommend_trip').attr('onkeyup','search_trip(this.value)');

  function deletetrip(){
    $('#recommend_trip_title').val('');
    $('#trip_id').val('');
  }
  function settrip(id,title){
    $('#show_trip').slideUp('fast');
    $('#recommend_trip_title').val(title);
    $('#trip_id').val(id);
  }
  function search_trip(key){
        if(key.length!=0){
          setTimeout(function(){ 
            $.ajax({
                url:'{{ asset('tripmanagement/gettrip') }}',
                method:'POST',
                data:{          
                     _token: '{{ csrf_token()}}', 
                    key: key,             
                },
                success:function(data){
                  
                  $('#show_trip').slideDown('fast');
                  $('#show_trip').html(data);
                }

            });
           }, 500);
        }
        else{
                $('#show_trip').slideUp('fast');
        }
    }
  
</script>
@endsection