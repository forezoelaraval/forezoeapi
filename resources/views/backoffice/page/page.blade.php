@extends('backoffice/layout/main')

@section('body')
<br/>
<style>
.content img{
		max-width: 100%;
		height: auto !important;
}
</style>
<?php 
	switch ($data['type']) {
		case 'width100%':
			echo '<div class="row">';
				echo '<div class="col-sm-10 content">';
						echo $data['textarea1'];
				echo '</div>';
			echo '</div>';
			break;
		case 'width25%':
			echo '<div class="row">';
			echo '<div class="col-sm-10" style="padding: 0px;">';
				echo '<div class="col-sm-4 content">';				
						echo $data['textarea1'];
				echo '</div>';
				echo '<div class="col-sm-8 content">';				
						echo $data['textarea2'];
				echo '</div>';
			echo '</div>';
			echo '</div>';
			
			break;
		case 'width50%':
			echo '<div class="row">';
				echo '<div class="col-sm-5 content">';				
						echo $data['textarea1'];
				echo '</div>';
				echo '<div class="col-sm-5 content">';				
						echo $data['textarea2'];
				echo '</div>';
			echo '</div>';
			
			break;
		case 'width75%':
			echo '<div class="row">';
			echo '<div class="col-sm-10" style="padding: 0px;">';
				echo '<div class="col-sm-8 content">';				
						echo $data['textarea1'];
				echo '</div>';
				echo '<div class="col-sm-4 content">';				
						echo $data['textarea2'];
				echo '</div>';
			echo '</div>';
			echo '</div>';
			
			break;
		default:
			# code...
			break;
	}
	
?>
@endsection