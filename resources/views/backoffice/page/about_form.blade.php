@extends('backoffice/layout/main')

@section('body')
 

<?php 
    echo Form::open(array('url' => asset($form['post']), 'files' => 1,'class'=>'form-horizontal'));     
?>
    <?php 
    echo Form::hidden('data[page_id]',$page_id);
    

    foreach ($form['rule'] as $key => $value) {
         echo Form::hidden('rule['.$key.']',$value);
    }
    ?>

    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_1_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_1_title]',$form['data']->block_1_title,array('class'=>'form-control','placeholder'=>'Title'));
            ?>

            </div>
        </div>
    </div>
 
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_1_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_1_description]',$form['data']->block_1_description,array('class'=>'form-control'));
            ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_1_image</label>
            <div class="col-sm-6">
            <div class='single_img' id="img_image_block_1_image">
               <img style='max-width:66%;' class="img-rounded" src="{{ asset($form['data']->block_1_image) }}">
            </div>
               <hr>
            <?php            
                echo Form::file('data[block_1_image]',array('onchange'=>'handleFiles(this.id,"img_image_block_1_image")','id'=>'image_block_1_image','accept'=>'image/x-png, image/gif, image/jpeg'));
                echo Form::hidden('old[file][block_1_image]',$form['data']->block_1_image);
               echo Form::hidden('file[image_1]','block_1_image');
            ?>
            </div>
        </div>
    </div>
      <hr/>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_2_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_2_title]',$form['data']->block_2_title,array('class'=>'form-control','placeholder'=>'Title'));
            ?>

            </div>
        </div>
    </div>
 
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_2_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_2_description]',$form['data']->block_2_description,array('class'=>'form-control'));
            ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_2_image</label>
            <div class="col-sm-6">
            <div class='single_img' id="img_image_block_1_image">
               <img style='max-width:66%;' class="img-rounded" src="{{ asset($form['data']->block_2_image) }}">
            </div>
               <hr>
            <?php            
                echo Form::file('data[block_2_image]',array('onchange'=>'handleFiles(this.id,"img_image_block_2_image")','id'=>'image_block_2_image','accept'=>'image/x-png, image/gif, image/jpeg'));
                echo Form::hidden('old[file][block_2_image]',$form['data']->block_2_image);
                echo Form::hidden('file[image_2]','block_2_image');
               
            ?>
            </div>
        </div>
    </div>
    <hr/>
<div class="row"><div class="form-group"><div class="col-sm-offset-2 col-sm-6"><input class="btn btn-primary" type="submit" value="ตกลง"></div></div></div>
</form>
@endsection