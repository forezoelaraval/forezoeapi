@extends('backoffice/layout/main')

@section('body')
<?php 
echo HTML::script('ckeditor/ckeditor.js');
	Backofficetemplate::form($form);
?>
<script>
	
	// CKEDITOR
	$('#text').parent().attr('class','col-sm-9');
	 var config = {	
			height: 300,
			filebrowserBrowseUrl: '{{ asset('browsermanagement') }}',
			toolbar : [

			 { name: 'basicstyles', items: [ 'Bold', 'Italic'] },
			 { name: 'basicstyles', items: [ 'NewPage'] },
			 { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
			 items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-' ,'Outdent', 'Indent','-', 'NumberedList', 'BulletedList', '-', '-', 'Blockquote', 'CreateDiv', '-'] },
			 { name: 'links', items: [ 'Link', 'Unlink' ] },
			 { name: 'document', items: [ 'Maximize','Source' ] },    		
    		'/',
    		{ name: 'styles', items: ['Format', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor'] },   			
   			{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule',  'SpecialChar', 'PageBreak' ] },
   			{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
			]
		};			
		CKEDITOR.skinName = 'bootstrapck';
		CKEDITOR.replace( 'text', config);
	// CKEDITOR

	$('#recommend_trip_title').attr('disabled','1');
	$('#recommend_trip_title').parent().append('<div align="right" onclick="deletetrip();" style="cursor:pointer;">Remove trip <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>');
	$('#label_recommend_trip').html('Search');
	$('#label_recommend_trip_title').html('Recommend Trip');
	$('#recommend_trip').parent().append('<div id="show_trip" style="display:none;" class="user_search"></div>');
	
	$('#recommend_trip').attr('onkeyup','search_trip(this.value)');

	function deletetrip(){
		$('#recommend_trip_title').val('');
		$('#recommend_trip_id').val('');
	}
	function settrip(id,title){
		$('#show_trip').slideUp('fast');
		$('#recommend_trip_title').val(title);
		$('#recommend_trip_id').val(id);
	}
	function search_trip(key){
        if(key.length!=0){
        	setTimeout(function(){ 
		        $.ajax({
		            url:'{{ asset('tripmanagement/gettrip') }}',
		            method:'POST',
		            data:{
		                _token: '{{ csrf_token()}}',
		                key: key,               
		            },
		            success:function(data){
		            	$('#show_trip').slideDown('fast');
		            	$('#show_trip').html(data);
		            }

		        });
	   		}, 500);
        }
        else{
                $('#show_trip').slideUp('fast');
        }
    }
	
</script>
@endsection