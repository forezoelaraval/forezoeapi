@extends('backoffice/layout/main')

@section('body')
 <?php
	echo HTML::script('ckeditor/ckeditor.js');
	Backofficetemplate::form($form);
  ?>
  
  <script>
  	$('#textarea1').parent().attr('class','col-sm-9');
  	$('#textarea2').parent().attr('class','col-sm-9');
	 var config = {	
			height: 300,
			filebrowserBrowseUrl: '{{ asset('browsermanagement') }}',
			toolbar : [

			 { name: 'basicstyles', items: [ 'Bold', 'Italic'] },
			 { name: 'basicstyles', items: [ 'NewPage'] },
			 { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
			 items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-' ,'Outdent', 'Indent','-', 'NumberedList', 'BulletedList', '-', '-', 'Blockquote', 'CreateDiv', '-'] },
			 { name: 'links', items: [ 'Link', 'Unlink' ] },
			 { name: 'document', items: [ 'Maximize','Source' ] },    		
    		'/',
    		{ name: 'styles', items: ['Format', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor'] },   			
   			{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule',  'SpecialChar', 'PageBreak' ] },
   			{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
			]
		};			
		CKEDITOR.skinName = 'bootstrapck';
		CKEDITOR.replace( 'textarea1', config);
		CKEDITOR.replace( 'textarea2', config);
	</script>

@endsection