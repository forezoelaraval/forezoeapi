@extends('backoffice/layout/main')

@section('body')
 

<?php 
	echo Form::open(array('url' => asset('pagemanagement/homesubmit'), 'files' => 1,'class'=>'form-horizontal'));		
?>
    <?php 
    echo Form::hidden('page',$arr_page['id']);
    

    foreach ($rule as $key => $value) {
         echo Form::hidden('rule['.$key.']',$value);
    }
    ?>
    <h3></h3>

    <!--  block_title_1 -->
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_title_1</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_title_1]',$arr_page['block_title_1'],array('class'=>'form-control','placeholder'=>'Title'));
            ?>

            </div>
        </div>
    </div>

 <!--  subtitle -->
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Sub Title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[subtitle]',$arr_page['subtitle'],array('class'=>'form-control','placeholder'=>'Sub Title'));
            ?>

            </div>
        </div>
    </div>
    



     <!-- picture -->
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Picture</label>
            <div class="col-sm-6">
            <div class='single_img' id="img_box1picture">
              <img style='max-width:100%;' class="img-rounded" src="">
            </div>
            <hr>
            <?php 
                echo Form::file('data[box1picture]',array('onchange'=>'handleFiles(this.id,"img_box1picture")','id'=>'box1picture','accept'=>'image/x-png, image/gif, image/jpeg'));
                 echo Form::hidden('old[box1picture]',$arr_page['box1picture']);
                 echo Form::hidden('file[1]','box1picture');
            ?>
            </div>
        </div>
    </div>

<script>
    function set_model_del(id){
        $('#model_del_id').val(id);
    }
 </script>
  <div class="modal small fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Delete Confirmation</h3>
                      </div>
                      <div class="modal-body" style="font-size:18px;">
                        <p class="error-text"><i class="fa fa-warning modal-icon"></i><span id='model-del-text'> &nbsp;Do you want to delete this item?</span></p>
                      </div>
                      <div class="modal-footer">                      
                      <form id="tab"  method="post"  action='<?php echo  asset('backoffice/del'); ?>'>
                       <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
                      <?php if(isset($array['table'])){ ?>
                        <input name='table' id='model_del_table' type='hidden' value='<?php echo $array['table'];?>'>
                      <?php } ?>
                      <input name='id' id='model_del_id' type='hidden' value=''>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-danger" type="submit">Delete</button>                        
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

    <?php
    echo HTML::script('ckeditor/ckeditor.js');
   
  ?>
  
  <script>
    $('#sponsor').parent().attr('class','col-sm-9');
 
     var config = { 
            height: 300,
            filebrowserBrowseUrl: '{{ asset('browsermanagement') }}',
            toolbar : [

             { name: 'basicstyles', items: [ 'Bold', 'Italic'] },
             { name: 'basicstyles', items: [ 'NewPage'] },
             { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
             items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-' ,'Outdent', 'Indent','-', 'NumberedList', 'BulletedList', '-', '-', 'Blockquote', 'CreateDiv', '-'] },
             { name: 'links', items: [ 'Link', 'Unlink' ] },
             { name: 'document', items: [ 'Maximize','Source' ] },          
            '/',
            { name: 'styles', items: ['Format', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor'] },              
            { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule',  'SpecialChar', 'PageBreak' ] },
            { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
            ]
        };          
        CKEDITOR.skinName = 'bootstrapck';
        CKEDITOR.replace( 'sponsor', config);
        
    </script>
@endsection