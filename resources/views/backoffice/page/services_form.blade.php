@extends('backoffice/layout/main')

@section('body')
 

<?php 
	echo Form::open(array('url' => asset($form['post']), 'files' => 1,'class'=>'form-horizontal'));		
?>
    <?php 
    echo Form::hidden('data[page_id]',$page_id);
    

    foreach ($form['rule'] as $key => $value) {
         echo Form::hidden('rule['.$key.']',$value);
    }
    ?>

    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_1_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_1_title]',$form['data']->block_1_title,array('class'=>'form-control','placeholder'=>'Title'));
            ?>

            </div>
        </div>
    </div>
 
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_1_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_1_description]',$form['data']->block_1_description,array('class'=>'form-control'));
            ?>

            </div>
        </div>
    </div>
      <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_2_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_2_title]',$form['data']->block_2_title,array('class'=>'form-control','placeholder'=>'Title'));
            ?>

            </div>
        </div>
    </div>
 
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_2_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_2_description]',$form['data']->block_2_description,array('class'=>'form-control'));
            ?>

            </div>
        </div>
    </div>
      <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_3_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_3_title]',$form['data']->block_3_title,array('class'=>'form-control','placeholder'=>'Title'));
            ?>

            </div>
        </div>
    </div>
 
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_3_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_3_description]',$form['data']->block_3_description,array('class'=>'form-control'));
            ?>

            </div>
        </div>
    </div>
     
<hr/>
  <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_4_description]',$form['data']->block_4_description,array('class'=>'form-control'));
            ?>

            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_1_icon</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_1_icon]',$form['data']->block_4_1_icon,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_1_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_1_title]',$form['data']->block_4_1_title,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_1_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_4_1_description]',$form['data']->block_4_1_description,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_2_icon</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_2_icon]',$form['data']->block_4_2_icon,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_2_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_2_title]',$form['data']->block_4_2_title,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_2_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_4_2_description]',$form['data']->block_4_2_description,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
<hr/>
<div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_3_icon</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_3_icon]',$form['data']->block_4_3_icon,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_3_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_3_title]',$form['data']->block_4_3_title,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_3_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_4_3_description]',$form['data']->block_4_3_description,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
<hr/>
<div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_4_icon</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_4_icon]',$form['data']->block_4_4_icon,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_4_title</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::text('data[block_4_4_title]',$form['data']->block_4_4_title,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
     <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">block_4_4_description</label>
            <div class="col-sm-6">          
            <?php 
                echo Form::textarea('data[block_4_4_description]',$form['data']->block_4_4_description,array('class'=>'form-control'));
            ?>
            </div>
        </div>
    </div>
    <hr/>
<div class="row"><div class="form-group"><div class="col-sm-offset-2 col-sm-6"><input class="btn btn-primary" type="submit" value="ตกลง"></div></div></div>
</form>
@endsection