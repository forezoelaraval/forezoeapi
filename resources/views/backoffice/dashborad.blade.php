@extends('backoffice/layout/main')

@section('body')
<br/>
 <div class="row">
    
     <div class="col-lg-3 col-md-3  ">
         <div class="panel panel-primary">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-users fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$int_user}}</div>
                         <div>User</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('usermanagement') }}">
                 <div class="panel-footer">
                     <span class="pull-left">ดูรายละเอียด</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
     
    <div class="col-lg-3 col-md-3">
         <div class="panel panel-yellow">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-user fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$int_idol}}</div>
                         <div>Idol</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('idolmanagement') }}">
                 <div class="panel-footer">
                     <span class="pull-left">ดูรายละเอียด</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
     <div class="col-lg-3 col-md-3">
         <div class="panel panel-green">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-home fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$int_supplier}}</div>
                         <div>Shop</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('shopmanagement') }}">
                 <div class="panel-footer">
                     <span class="pull-left">ดูรายละเอียด</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
     <div class="col-lg-3 col-md-3">
         <div class="panel panel-red">
             <div class="panel-heading">
                 <div class="row">
                     <div class="col-xs-3">
                         <i class="fa fa-cube fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                         <div class="huge">{{$int_product}}</div>
                         <div>Product</div>
                     </div>
                 </div>
             </div>
             <a href="{{ asset('productmanagement') }}">
                 <div class="panel-footer">
                     <span class="pull-left">ดูรายละเอียด</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                 </div>
             </a>
         </div>
     </div>
    
 </div>
<div class="row">
    <div class="col-sm-8">
    	<div class="chat-panel panel panel-default">
    		<div class="panel-heading">
    		    <i class="fa fa-comments fa-fw"></i>
    			    รายการเติมเงินล่าสุด
                <a href='{{ asset('transactionmanagement') }}'>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                </a>
    		</div>
    		 <!-- /.panel-heading -->
             <div class="panel-body" style='height:460px; overflow:auto;'>
	             <table class='table'>
	                <thead>
	                    <tr>                                                                     
	                        <th>ชื่อ</th>                      
                            <th>รายละดอียด</th> 
                            <th>ยอดชำระ</th> 
	                        
	                        <th></th>                                          
	                    </tr>           
	                </thead>
	                <tbody>
	                   <?php foreach ($arr_payment as $key_payment => $value_payment): ?>
                           <tr>                                                                     
                               <td>{{ $value_payment->username}}</td>                      
                               <td>{{ $value_payment->coin}}</td> 
                               <td>{{ $value_payment->remark}}</td> 
                               <td>{{ $value_payment->created_at}}</td>                                          
                           </tr>  
                       <?php endforeach ?>
	                	 
	               
	                </tbody>
	            </table>
            </div>
    	</div>
 	</div>
 	<div class="col-sm-4">
		<div class="chat-panel panel panel-default">
	       <div class="panel-heading">
	      	    <i class="fa fa-user fa-fw"></i>
		          แจ้งเหตุขัดข้อง
		        	<a href='#'>
		            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
		            </a>
		    </div>
		    <div class="panel-body"  style='height:460px; overflow:auto;'>
		       <ul class="chat">
		            
		                 
		              
		                  
		          </ul>
		      </div>
	    </div>
 	</div>
</div>

@endsection