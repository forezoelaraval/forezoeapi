@extends('backoffice/layout/main')

@section('body')

<?php 
 echo HTML::script('packages\frozennode\administrator\js\ckeditor/ckeditor.js');
  echo Form::open(array('url' => asset('productmanagement/successproduct'),'class'=>'form-horizontal'));    

  
?>
    
<?php 
    if(isset($process)){
         Backofficetemplate::process($process);    
    }
    
?>
    <input type="hidden" name='product_id' value='{{$product_id}}'>
   
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">status</label>
            <div class="col-sm-6">
                <input type="radio" name='data[active]' class='' value='1' checked> Active &nbsp;
                <input type="radio" name='data[active]' value="0" > Pending
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">New release</label>
            <div class="col-sm-6">
                <input type="radio" name='data[new_release]' class='' value='1' checked> Active &nbsp;
                <input type="radio" name='data[new_release]' value="0" > Pending
            </div>
        </div>
    </div>
    
   
   <hr></hr>
    <div class="row">
        <div class="form-group">                        
            <div class="col-sm-offset-2 col-sm-6">  
            <?php 
                echo Form::submit('Submit ',array('class'=>'btn btn-primary'));  

            ?> 
            </div>
        </div>             
    </div>
 <?php 
        echo Form::close();
    ?>


@endsection