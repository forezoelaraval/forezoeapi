@extends('backoffice/layout/main')

@section('body')
<?php 
    echo HTML::script('ckeditor/ckeditor.js');
?>
<link rel="stylesheet" href="{{ asset('bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<script src='{{ asset("bootstrap-tagsinput/bootstrap-tagsinput.min.js") }}'></script>


<script type="text/javascript">
/*--------     upload -----*/


/*--------     upload -----*/
</script>
<?php 
	echo Form::open(array('url' => asset('productmanagement/submitproductgallery'), 'files' => 1,'class'=>'form-horizontal'));		
    echo Form::hidden('data[product_id]',$product_id);
  
?>
 <?php 
    if(isset($process)){
         Backofficetemplate::process($process);    
    }
    
?>
    
 <!-- Gallery -->
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">Upload image</label>
            <div class="col-sm-8" >
                <div class='multiple_img' id="img_gallery"></div>
                <hr/>
                <div >
                    <?php 
                        echo Form::file('data[picture]',array('id'=>'gallery','accept'=>'image/x-png, image/gif, image/jpeg','onchange'=>'handleFiles(this.id,"img_gallery")'));
                        ?>
                </div>    

            </div>
        </div>
    </div>

 <div class="row">
        <div class="form-group">                        
            <div class="col-sm-offset-2 col-sm-6">  
            <?php 
                echo Form::submit('Upload First!!',array('class'=>'btn btn-success'));  

            ?>
            </div>
        </div>             
    </div>


    <?php 
        echo Form::close();
    ?>
    <hr/>
   <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>image</th>

                <th width="250px"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($arr_product_gallery as $key => $value): ?>
            <tr>
                <td>{{$key+1}}</td>
                <td><?php $picture=  $value->picture; 
                $picture_path = asset($picture);

                ?>
                <img src="{{$picture_path}}" style='max-width:200px;'alt="">
                </td>

                <td><button onclick="set_model_del({{$value->id}});" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
        <?php endforeach ?>
            
        <tbody>
    </table>
    <?php if(isset($process)){?>
   <hr></hr>
   <div class="row">
        <div class="form-group">                        
            <div class="col-sm-offset-8 col-sm-2">  
            <a style='float:right;' class='btn btn-primary' href='{{ asset($process["nextpath"]) }}' >Next >></a>
            </div>
        </div>             
    </div>
    <?php }?>
 <br/>
 <script>
    function set_model_del(id){
        $('#model_del_id').val(id);
    }
 </script>
  <div class="modal small fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Delete Confirmation</h3>
                      </div>
                      <div class="modal-body" style="font-size:18px;">
                        <p class="error-text"><i class="fa fa-warning modal-icon"></i><span id='model-del-text'> &nbsp;Do you want to delete this item?</span></p>
                      </div>
                      <div class="modal-footer">                      
                      <form id="tab"  method="post"  action='<?php echo  asset('productmanagement/gallerydel'); ?>'>
                       <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
                    
                      <input name='id' id='model_del_id' type='hidden' value=''>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-danger" type="submit">Delete</button>                        
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
@endsection