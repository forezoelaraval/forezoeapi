@extends('backoffice/layout/main')

@section('body')

<br>
<div class='row'>
   
    <?php if (isset($profile['picture'])): ?>
    <div class='col-sm-4'>

			<img  onclick="display('<?php echo asset($profile["picture"]) ?>');" style='max-width:100%;cursor: pointer;' class='img-rounded' src="<?php echo asset($profile['picture']) ?>" >
			<hr></hr>
		<div class='row'>
			<?php 
			foreach ($arr_product_gallery as $key_product_gallery => $value_product_gallery)
			{
				echo '<div class="col-xs-4">';
				?>
				<img  onclick="display('<?php echo asset($value_product_gallery->picture) ?>');" style='max-width:100%;cursor: pointer;height:75px;' class='img-rounded' src="<?php echo asset($value_product_gallery->picture) ?>" >

				<?php echo '</div>';
			}
		?> 	  		
		</div>
	</div>	
	<?php endif ?>
	
	<div class='col-sm-8'>
<h2><?php echo $profile['title']; ?></h2>
		<h4><?php echo $arr_category->title; ?></h4>		
			<table>
				<tr>
					<td><b>รหัสสินค้า</b></td>
					<td>&nbsp;:&nbsp;</td>
					<td><?php echo $arr_product->sku; ?></td>
				</tr>
				<?php if(isset($arr_product->score)){?>
				<tr>
					<td><b>score</b></td>
					<td>&nbsp;:&nbsp;</td>
					<td><?php echo $arr_product->score; ?></td>
				</tr>
				<?php }?>
			</table>
		<hr></hr>
		<div class='row' style="padding-top:5px;padding-bottom:5px;">		
			 <div class="form-group">
			 	<div  class="col-xs-3 control-label"><b>ราคาจริง</b></div>
			 	<div class="col-xs-9">: <?php echo number_format($arr_product->coin_raw); ?> บาท</div>
			 	<div  class="col-xs-3 control-label"><b>ราคา</b></div>
			 	<div class="col-xs-9">: <?php echo number_format($arr_product->coin); ?> บาท</div>
			 	<div  class="col-xs-3 control-label"><b>ร้านค้า</b></div>
			 	<div class="col-xs-9">: <a href='{{asset("shopmanagement/profile/". $arr_shop->id)}}'><i class="fa fa-home" aria-hidden="true"></i> <?php echo $arr_shop->title; ?></a> </div>
			</div>

			<div class="form-group">
			<div  class="col-xs-12 "><b><h4>รายละเอียด</h4></b></div>
			<div  class="col-xs-12 "><?php echo nl2br($arr_product->description); ?></div>
			 	
			</div>
		</div>
	
	</div>
</div>
<script type="text/javascript">
  function display(image){
    $('#modal-image').attr('src',image);
    $('#myModal').modal('show');
  }
</script>
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
     		<div class="modal-titleer">
      			<div class="modal-titleer">
       			 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">image</h4>
      			</div>
      			<div class="modal-body" align="center">
       				<img style='width:100%;' id='modal-image' src=" " alt="">
      			</div>
     			<div class="modal-footer">
        			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      			</div>
    		</div>
  		</div>
	</div>
</div>
@endsection
