@extends('backoffice/layout/main')

@section('body')
<?php 
echo HTML::script('packages\frozennode\administrator\js\ckeditor/ckeditor.js');
	Backofficetemplate::form($form);
?>
<script>
    //-------CKEDITOR
   /* CKEDITOR.config.toolbar = [
  
   '/',
   ['Bold','Italic','Underline','StrikeThrough','Subscript','Superscript','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'], ['Styles','Format','Font','FontSize'],
   ['Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
] ;
        CKEDITOR.replace('description');
        $('#description').parent().attr('class','col-sm-8');*/
    //-------CKEDITOR    
$('#div_size_remark').append('<hr>');
$('#div_discount').append('<hr>');
$('#label_title').html('ชื่อ');
$('#label_description').html('รายละเอียด');
$('#label_product_code').html('รหัสสินค้า');
$('#label_subtitle').html('คำอธิบาย');
$('#label_size_remark').html('ขนาดสินค้า');
$('#label_description').html('รายละเอียด');
$('#label_price').html('ราคา');
$('#label_discount').html('ลดราคา');
$('#label_category_id').html('หมวดหมู่สินค้า');
$('#label_picture').html('รูปภาพสินค้า');
$('#label_picture_spec').html('รูปภาพสเปคสินค้า');
</script>
@endsection