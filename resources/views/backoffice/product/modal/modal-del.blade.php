 
 <script>
    function set_modal_del(id){
        $('#model_del_id').val(id);
  
    }
 </script>
  <div class="modal small fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Delete Confirmation</h3>
                      </div>
                      <div class="modal-body" style="font-size:18px;">
                        <p class="error-text"><i class="fa fa-warning modal-icon"></i><span id='model-del-text'> &nbsp;Do you want to delete this item?</span></p>
                      </div>
                      <div class="modal-footer">                      
                      <form   method="post"  action='{{asset("backoffice/del")}}'>
                       <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
                        <input name='table' id='model_del_table' type='hidden' value='product_size'>
                      <input name='id' id='model_del_id' type='hidden' value=''>
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-danger" type="submit">Delete</button>                        
                        </form>
                      </div>
                    </div>
                  </div>
                </div>