 
 <script>
    function set_modal_editattibute_edit(attribute,sub_attribute=''){
      var textarea_input;
      $('#textarea_input').html('');     
      $('#update_attribute').html('');     
      if(sub_attribute==''){ // แก้ parnet
         textarea_input =  ' <textarea id="update_attribute" name="update[attribute]" class="form-control ckeditor" placeholder=" Attibute type"  >'+attribute+'</textarea>';
      }else{
          textarea_input =  ' <textarea id="update_attribute" name="update[sub_attribute]" class="form-control ckeditor" placeholder=" Attibute type"  >'+sub_attribute+'</textarea>';
      }
        $('#textarea_input').html(textarea_input);
        $('#edit_attribute').val('');   
        $('#edit_sub_attribute').val(''); 
        $('#edit_attribute').val(attribute);   
        $('#edit_sub_attribute').val(sub_attribute);  
       CKEDITOR.replace( 'update_attribute',config);
    }
 </script>
  <div class="modal small fade" id="Modal-editattibute-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">แก้ไขสเปค</h3>
        </div>
         <form   method="post"  action='{{asset("productmanagement/submiteditattibute")}}'>
        <div class="modal-body" style="font-size:18px;">
             <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Attibute</label>
                    <div id='textarea_input' class="col-sm-10">          
                      
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">                      
         
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
            <input name='data[product_id]'  type='hidden' value='{{$product_id}}'>
            <input name='data[attribute]' id='edit_attribute' type='hidden' value=''>
            <input name='data[sub_attribute]' id='edit_sub_attribute' type='hidden' value=''>
            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Edit</button>                        
          </form>
        </div>
      </div>
    </div>
  </div>