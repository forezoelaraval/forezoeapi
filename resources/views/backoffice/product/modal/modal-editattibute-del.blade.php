 
 <script>
    function set_modal_editattibute_del(attribute,sub_attribute=''){
        $('#del_attribute').val('');   
        $('#del_sub_attribute').val(''); 
        $('#del_attribute').val(attribute);   
        $('#del_sub_attribute').val(sub_attribute);   
  
    }
 </script>
  <div class="modal small fade" id="Modal-editattibute-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Delete Confirmation</h3>
        </div>
        <div class="modal-body" style="font-size:18px;">
          <p class="error-text"><i class="fa fa-warning modal-icon"></i><span id='model-del-text'> &nbsp;Do you want to delete this item?</span></p>
        </div>
        <div class="modal-footer">                      
          <form id="tab"  method="post"  action='{{asset("productmanagement/submitdelattibute")}}'>
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
            <input name='data[product_id]'  type='hidden' value='{{$product_id}}'>
            <input name='data[attribute]' id='del_attribute' type='hidden' value=''>
            <input name='data[sub_attribute]' id='del_sub_attribute' type='hidden' value=''>
            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-danger" type="submit">Delete</button>                        
          </form>
        </div>
      </div>
    </div>
  </div>