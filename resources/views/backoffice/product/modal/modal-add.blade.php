<div class="modal fade" id="Modal_add_row" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add Row</h4>
          </div>
          <div class="modal-body">
          <?php 
           echo Form::open(array('url' => asset('productsizemanagement/insertsize')));    
           echo Form::hidden('data[product_id]',$product_id);  
           echo Form::hidden('data[product_type_id]',$product_type_id);  
           echo Form::hidden('data[category_id]',$arr_product->category_id);  
          ?>
           <?php 
           if(isset($arr_row)&&count($arr_row)>0){
              foreach ($arr_row as $key_row => $value_row){ ?>
               <div class="row">
                   <div class="form-group">
                       <label class="col-sm-6 control-label"><?php echo str_replace("_"," ",$value_row); ?></label>
                       <div class="col-sm-6">
                           <?php
                          echo Form::text('data[value]['.$value_row.']',null,array('class'=>'form-control'));  
                           ?>
                         
                       </div>
                   </div>
                   <br/>
                   <br/>
               </div>
           <?php }
            }
           ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value='Save changes'/>
          </div>
          </form>
        </div>
      </div>
    </div>  