
@extends('backoffice/layout/main')

@section('body')
<?php 
  use App\Models\Menu;

    echo HTML::script('jquery-ui-1.11.4.custom/jquery-ui.js');
    echo HTML::style('jquery-ui-1.11.4.custom/jquery-ui.css');
    echo HTML::style('jquery-ui-1.11.4.custom/jquery-ui.theme.css'); 
    echo HTML::script('backoffice_resource/Nestable-master/jquery.nestable.js');
     echo HTML::style('backoffice_resource/Nestable-master/nestable.css'); 

  
?>
<script>

$(document).ready(function()
{
         

    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            var array= window.JSON.stringify(list.nestable('serialize'));//, null, 2));       
          if(list.nestable('serialize').length){
              $.ajax({               
                url: '{{ asset('menumanagement/sortmenu') }}',
                type:'POST',
                data:{
                  _token: '{{ csrf_token()}}',
                   data:array
                },
                success: function(data){
                  console.log(data);
                }
              });
             
           }
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

   

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
  
   
   /* $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
*/
   

});
function changetype(value){
    
    window.location="{{ asset('menumanagement/home') }}"+'/'+value;
}
function deletemenu(id){
  if(confirm('ต้องการลบ menu นี้ใช่หรือไม่')){
    $('#delete_id').val(id);
  $('#deletemenu').submit();
  }
  
}
function addmenu(title,url){
  if(title==''){
     $('#title').focus();
    return false;
  }
  if(url==''){
    $('#url').focus();
   return false; 
  }
  $('#url').val('<?php echo asset(""); ?>'+url);
  $('#title').val(title);
  $('form#addmenu').submit(); 
 // console.log(title+','+key);
  //console.log($('#input-title-'+key).val());
  //console.log($('#input-url-'+key).val());
 
}

</script>
<?php 

  echo Form::open(array('id'=>'deletemenu','url'=>asset('menumanagement/delete'),'method'=>'POST'));
    echo Form::hidden('type',$type);
    echo Form::hidden('id',null,array('id'=>'delete_id'));
  echo Form::close();
?>
<div class='row'>
<label class="col-sm-2">Type :</label>
<div class='col-sm-6'>
<?php echo Form::select('type',array('head'=>'head menu','top_left'=>'top left','foot_left'=>'footer menu left','foot_right'=>'footer menu right'),$type,array('style'=>'width:250px;','class'=>'form-control','onchange'=>'changetype(this.value)')); ?>
</div>
</div>
<hr>  
<div class='row'>
  <div class='col-sm-4'>
     <!-- arr home -->
     <button onclick="$('#page_select').slideToggle('fast');" align='left' style='width:100%;' class="btn-click btn btn-default dropdown-toggle" type="button" >
       <div align="left"> Page
        <span class="caret"></span>
        </div>
      </button>
      <div id='page_select'  class="list">
        
          <div class="list-group">   
          <?php

           foreach ($arr_menuhome as $key => $value): ?>
            <a  class="list-group-item">
            <span>{{$value->title}}</span>
            
            <span onclick='addmenu("{{$value->title}}","{{$value->url}}");' class="glyphicon glyphicon-plus" aria-hidden="true"></span>
           
           
            </a>    
           <?php 
           
           endforeach ?>         
                        
          </div>
        
      </div>
     <!-- -->
     <!-- arr_menucustom -->
     <button onclick="$('#pagecustomer_select').slideToggle('fast');" align='left' style='width:100%;' class="btn-click btn btn-default dropdown-toggle" type="button" >
       <div align="left"> Page Customer
        <span class="caret"></span>
        </div>
      </button>
      <div id='pagecustomer_select'  class="list">
        
          <div class="list-group">   
          <?php

           foreach ($arr_menucustom as $key => $value): ?>
            <a  class="list-group-item">
            <span   >{{$value->title}}</span>
            
            <span onclick='addmenu("{{$value->title}}","{{$value->url}}");' class="glyphicon glyphicon-plus" aria-hidden="true"></span>
           
            
            </a>    
           <?php 
            
           endforeach ?>         
                        
          </div>
        
      </div>
     <!-- -->
<!-- arr_menucategory -->
<button onclick="$('#pagecategory_select').slideToggle('fast');" align='left' style='width:100%;' class="btn-click btn btn-default dropdown-toggle" type="button" >
       <div align="left"> Category & Sub Category
        <span class="caret"></span>
        </div>
      </button>
      <div id='pagecategory_select'  class="list">
        
          <div class="list-group">   
          <?php
           foreach ($arr_menucategory as $key => $value): ?>
            <a  class="list-group-item">
            <span onclick="$('#div-{{$value['parent_id']}}').slideToggle('fast');" class='span-click' >{{$value['title']}} <span class="caret"></span></span>
            <span onclick='addmenu("{{$value['title']}}","{{$value['url']}}");' class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          
          <div style="display:none;" id='div-{{$value['parent_id']}}'>
            <hr>
            <ul class='menucategory'>
            <?php foreach ($value['subcategory'] as $key => $valuesubcategory): ?>
               <li> - <?php echo $valuesubcategory['title']; ?>   
               <span onclick='addmenu("{{$valuesubcategory['title']}}","{{$valuesubcategory['url']}}");' class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          </li>
            <?php endforeach ?>
            </ul>
            </div>
            </a>    
           <?php endforeach ?>         
                        
          </div>
        
      </div>

<!-- -->
<!-- custom-->
<button onclick="$('#customurl_select').slideToggle('fast');" align='left' style='width:100%;' class="btn-click btn btn-default dropdown-toggle" type="button" >
       <div align="left"> Custom URL
        <span class="caret"></span>
        </div>
      </button>
      <div id='customurl_select'  class="list">
        
          <div class="list-group">   
          
            <a  class="list-group-item">
           
             <span onclick='addmenu($("#title").val(),$("#url").val());' class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            <span >
            <?php
            echo Form::open(array('id'=>'addmenu','url'=>asset('menumanagement/insert'),'method'=>'POST'));
              echo Form::hidden('data[type]',$type);
              echo Form::text('data[title]',null,array('id'=>'title','placeholder'=>'title : ','style'=>'width:250px;'));
              echo Form::textarea('data[url]',null,array('rows'=>'2','id'=>'url','placeholder'=>'url : ','style'=>'width:250px;max-width:100%;'));
            ?>            
            </span>                       
            <?php 
              echo Form::close();
            ?>          
            </a>    
          
                        
          </div>
        
      </div>
<!-- -->
  </div>
  <div class='col-sm-8'>
     <div class="cf nestable-lists">
      <div class="dd" id='nestable'>
        <?php     gensubmenu($type, 0);   ?>            
      </div>
    </div>
  </div>

</div>
  
<?php 
  function gensubmenu($type,$parent_id){
    
    $arr_menu =Menu::querymenu($type,$parent_id);
    if(count($arr_menu)){
        echo '<ol class="dd-list">';
        
    
        foreach ($arr_menu as $key => $value) {
           echo '<li class="dd-item" data-id="'.$value->id.'">';
                 echo '<div class="dd-handle">'.$value->title.'</div>';
        ?>
          <span onclick='$("#form-{{$value->id}}").slideToggle("fast")' class="caret caret-edit"></span>
                  <div id='form-{{$value->id}}' class="list-group list-edit" >  
                      <a  class="list-group-item">
                      
                        <?php 
                        
                        echo Form::open(array('url'=>asset('menumanagement/edit')));
                         echo Form::hidden('id',$value->id);
                         echo Form::text('data[title]',$value->title,array('id'=>'title','placeholder'=>'title : '));
                         echo Form::textarea('data[url]',$value->url,array('rows'=>'2','id'=>'url','placeholder'=>'url : ','style'=>'max-width:100%;'));
                         echo "<span onclick='$(this).parent().submit();'>Edit</span>&nbsp;
                        <span onclick='deletemenu(".$value->id.")'>Delete</span>";
                         echo Form::close();
                        
                        ?>
                      </a>
                    </div>
                   
        <?php
         $arr_sub_menu =Menu::querymenu($type,$value->id);
         if(count($arr_sub_menu)){
             gensubmenu($type,$value->id);
           } 
              echo '</li>';  
        }
       echo '</ol>'; 
    }
  }
?>
   

 

      

    </div>
@endsection