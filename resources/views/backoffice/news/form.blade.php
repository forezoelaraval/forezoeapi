@extends('backoffice/layout/main')

@section('body')

<?php 
 echo HTML::script('packages\frozennode\administrator\js\ckeditor/ckeditor.js');
	
?>
<?php 

	Backofficetemplate::form($form);
?>

<script>
	$('#label_title').html('ชื่อ');
	$('#label_subtitle').html('คำอธิบาย');
	$('#label_description').html('รายละเอียด');
	//$('#input_description').attr('class','col-sm-8');
	$('#label_picture').html('รูปภาพ');
	/*
	 //-------CKEDITOR
      CKEDITOR.config.toolbar = [
  
   '/',
   ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'], ['Styles','Format','Font','FontSize'],
   ['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
] ;
        CKEDITOR.replace('description');
    //-------CKEDITOR    
    */
</script>
@endsection