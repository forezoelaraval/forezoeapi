@extends('backoffice/layout/main')

@section('body')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
         
       <?php foreach ($arr_topten as $key => $value_wishlist) {
       	echo "['".$value_wishlist->title."',".$value_wishlist->product_count."],";
       }?>
         
        ]);

        var options = {
         	 title: 'Top ten product wishlist',
          pieHole: 0.2,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
<div class='row'>	
	<div class="col-sm-7">	
		<table class='table'>
			<thead>	
				<tr>	
					<th>Picture</th>
					<th>title</th>
					<th>count</th>
					<th style="width:150px;"></th>
				</tr>
			</thead>	
			<tbody>	
			@foreach ($result_data as $value_wishlist)
				<tr>	
					<td>
						<img style='width:150px;' src="{{asset($value_wishlist->picture)}}" alt="">
					</td>
					<td>{{$value_wishlist->title}}</td>
					<td>{{$value_wishlist->product_count}}</td>
					<td>
						<a target="_blank" title="Product" href="{{ asset('productmanagement/view/'.$value_wishlist->product_id) }}" class="btn btn-primary">
						<i class="fa fa-file-text-o"></i>
					</a>
					<a target="_blank" title="User" href="{{ asset('wishlistmanagement/wishlist/'.$value_wishlist->product_id) }}" class="btn btn-primary">
						<i class="fa fa-user"></i>
					</a>
					</td>
				</tr>
			@endforeach
				
			</tbody>
		</table>
	</div>
	<div class="col-sm-5">	
	 <div id="donutchart" style="width: 100%;"></div>
	</div>
</div>
@endsection