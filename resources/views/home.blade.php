
<form method="post">
<div class="payment_step" id="payment_credit_card" style="display: block;">
						<div class="form-group">
							<label>Card holder's name</label>
							<input value="" type="text" data-omise="card_name" class="form-control" id="holder_name" name="holder_name">
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Credit card number</label>
									<input maxlength="16" value="" data-omise="card_number" type="text" id="card_number" name="card_number" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Expiry date</label>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input maxlength="2" data-omise="expiration_month" type="text" value="" id="expire_month" name="expire_month" class="form-control" placeholder="Month">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input maxlength="4" data-omise="expiration_year" type="text" value="" id="expire_year" name="expire_year" class="form-control" placeholder="Year">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Security code (CVC)</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<input maxlength="3" value="" size="8" data-omise="security_code" type="text" id="ccv" name="ccv" class="form-control" placeholder="CCV">
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>amount</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<input  value=""  type="text" id="amount" name="amount" class="form-control" placeholder="amount">
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>description</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<input  value=""type="text" id="description" name="description" class="form-control" placeholder="description">
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div><!--End row -->
					</div>
					<input type="submit">
					</form>