


<table width="650" align="center" cellpadding="0" cellspacing="0" border="1" bordercolor="#eee">
<tbody>
<tr>
<td>
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td rowspan="2"></td>
<td colspan="2">
</tr>
<tr>
<td>
<td>

</td>
</tr>
<tr>
<td valign="top">
</td>
<td valign="top">
<table width="100%" border="0" cellpadding="1" cellspacing="1" style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#666666">
<tbody>
<tr>
<td width="59%" valign="top">
  <img style="margin-top:25px;margin-left: 10px;" src="https://thaitaxivip.com/webservice/logo-mail.jpg" height="50" class="CToWUd"></td>
<td width="41%"><br>
<strong>ThaiTaxiVIP</strong> <br>
1350/259-262, 15th Fl. Thairong Tower B,Pattanakarn 36,
SuangLuang SuanLuang Bangkok 10250 Thailand<br>
<br>
<strong>Phone / โทรศัพท์</strong> : +66 2 784 7320<br>
<strong>Email</strong> : booking@thaitaxivip.com</td>
</tr>
</tbody>
</table>
</td>
<td valign="top">
</tr>
<tr>
<td valign="top">
<td height="450" valign="top">
<table width="96%" border="0" align="center" cellpadding="1" cellspacing="1">
<tbody>
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="1" style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#666666">
<tbody>
<tr>
<td width="97%">
<p>&nbsp;</p>
<p>ขอขอบคุณที่ท่านเลือกเดินทางกับ ThaiTaxiVIP"</p>
<p>&nbsp;&nbsp;&nbsp;Email / อีเมล์ฉบับนี้เป็นการยืนยันการจองรถกับ ThaiTaxiVIP โปรดศึกษาเงื่อนไขและข้อกำหนดการเดินทางให้ละเอียด ซึ่งการจองนี้เป็นระบบการจอง ทาง ThaiTaxiVIP
</p>
<p>&nbsp;&nbsp;&nbsp;หากท่านต้องการเปลี่ยนแปลงการจอง กรุณาโทรติดต่อ ศูนย์บริการผู้โดยสาร (Call Center) ที่โทรศัพท์ +66 81 917 4449 ได้ตลอด 24 ชม. โดยแจ้งรหัสสำรองที่นั่ง( Booking ID ) ไม่สามารถเปลี่ยนแปลงชื่อผู้โดยสารได้ </p>
<p>กรุณาอย่าตอบกลับ Email / อีเมล์ฉบับนี้ หากมีข้อสงสัยหรือข้อเสนอแนะกรุณาติดต่อ ศูนย์บริการผู้โดยสาร (Call Center) ที่โทรศัพท์ +66 81 917 4449 หรือทาง
<a href="mailto:booking@thaitaxivip.com" target="_blank">booking@thaitaxivip.com</a></p>
<p>ขอแสดงความนับถือ<br>
แผนกรับจอง</p>
</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#222"><font color="#000099"><strong><u>เอกสารยืนยันการจอง
</u></strong></font></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="1">
<tbody>
<tr>
<td width="48%">
<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#222">
<tbody>
<tr>
<td bgcolor="#ffcb3d"  style="padding:5px;"><strong>Contact/Passenger Information</strong></td>
</tr>
</tbody>
</table>
</td>
<td width="4%">&nbsp;</td>
<td width="48%">
<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#222">
<tbody>
<tr>
<td bgcolor="#ffcb3d"  style="padding:5px;"><strong>Payment Informations</strong></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td valign="top" bgcolor="#FFF">
<table width="100%" border="0" align="right" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF" style="border: solid 1px #ddd; FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#666666">
<tbody>
<tr>
<td width="38%" valign="top">
<div align="right"><strong>Name / ชื่อ</strong></div>
</td>
<td width="4%" valign="top"><strong>:</strong></td>
<td width="58%" valign="top">{{$name}} </td>
</tr>
<tr>
<td valign="top">
<div align="right"><strong>Email / อีเมล์</strong></div>
</td>
<td valign="top"><strong>:</strong></td>
<td valign="top"><a href="mailto:{{$email}}" target="_blank">{{$email}}</a></td>
</tr>
<tr>
<td valign="top">
<div align="right"><strong>Phone / โทรศัพท์</strong></div>
</td>
<td valign="top"><strong>:</strong></td>
<td valign="top">{{$phone}}</td>
</tr>
<tr>
<td valign="top">
<div align="right"><strong>Car Type / ประเภทรถ</strong></div>
</td>
<td valign="top">&nbsp;</td>
<td valign="top">{{$taxi_type_text}}</td>
</tr>
<tr>
<td valign="top">
<div align="right"><strong><span style="FONT-SIZE:10px;FONT-FAMILY:Verdana,Arial,Helvetica,sans-serif;">
  <font>Booking Date/วันที่ทำการจอง</font></span></strong></div>
</td>
<td valign="top"><strong>:</strong></td>
<td valign="top"><font><strong> <?php echo date('d/m/Y  H:i:s');  ?></strong></font></td>
</tr>

<tr>
<td valign="top">
<div align="right"><strong><span style="FONT-SIZE:11px;FONT-FAMILY:Verdana,Arial,Helvetica,sans-serif;COLOR:#666666"><font color="#003366">รหัสการสำรองที่นั่ง<br>
(</font></span><font color="#003366">Booking ID</font><span style="FONT-SIZE:11px;FONT-FAMILY:Verdana,Arial,Helvetica,sans-serif;COLOR:#666666"><font color="#003366"> )</font></span></strong></div>
</td>
<td valign="top"><strong>:</strong></td>
<td valign="top"><font color="#CC0000"><strong>{{$order_id}}</strong></font></td>
</tr>

</tbody>
</table>
</td>
<td>&nbsp;</td>
<td valign="top" bgcolor="#FFF">
<table width="100%" border="0" align="right" cellpadding="1" cellspacing="1" bgcolor="#FFFFFF" style="border: solid 1px #ddd; FONT-SIZE:11px;FONT-FAMILY:Verdana,Arial,Helvetica,sans-serif;COLOR:#666666">
<tbody>

<tr>
<td valign="top"><strong>Note. &nbsp;</strong></td>
<td valign="top"><strong>:</strong></td>
<td valign="top">{{$remark}}</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>


<tr>
<td valign="top"><strong>Credit ID / บัตรเครดิตเลขที่</strong></td>
<td valign="top"><strong>:</strong></td>
<?php $card_number2 = substr($card_number, 0, -4) . 'xxxx'; ?>
<td valign="top">{{$card_number2}}</td>
</tr>






<tr>
<td height="24" colspan="3" valign="bottom"><font color="#009933">Credit Card By Payment Gateway</font></td>
</tr>

<tr>
<td height="24" colspan="3" valign="bottom"><strong>
  <BR><font color="#003366" size="1">Total / รวมเป็นเงินทั้งหมด {{$price}} Baht<br>  <span style="height:10px;">&nbsp;</span>
</font></strong>

</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>

<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#222">
<tbody>
<tr>
<td bgcolor="#ffcb3d" style="padding:5px;"><strong>Travel Information</strong></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC" style="FONT-SIZE:10px;FONT-FAMILY:MS Sans Serif,Tahoma;COLOR:#666666">
<tbody>
<tr>
<td width="180" align="center" bgcolor="#e6e6e6"><strong>Route (from - to) / เส้นทางการเดินทาง (จาก - ถึง)</strong></td>

<td width="85" align="center" bgcolor="#e6e6e6"><strong>Date / วันเดินทาง</strong></td>
<td width="90" align="center" bgcolor="#e6e6e6"><strong>Boarding Time / เวลาเดินทาง</strong></td>
<td width="90" align="center" bgcolor="#e6e6e6"><strong>Distance / ระยะทาง </strong></td>

</tr>
<tr>
<td width="24" align="center" bgcolor="#ffffff">{{$p1TextAddress}} - {{$p2TextAddress}}</td>

<td width="24" align="center" bgcolor="#ffffff">{{$date}}</td>
<td width="24" align="center" bgcolor="#ffffff">{{$time}}</td>
<td width="24" align="center" bgcolor="#ffffff">{{$distance}} กม.</td>


</tr>

</tbody>
</table>
</td>
</tr>


</tbody>
</table>

<p style="margin-bottom:10px;">&nbsp;</p>
</td>
<td valign="top" >
</tr>
<tr>
<td colspan="3">
<a href="https://thaitaxivip.com"><img src="https://thaitaxivip.com/webservice/footer-mail.jpg" width="650" alt="" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01;">
</a>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table></td></tr></tbody></table>
