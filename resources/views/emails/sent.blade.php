<p>สวัสดีครับ,</p>

<p>นี่เป็นระบบแจ้งเตือนว่ามีการติดต่อเข้ามาใหม่ จากระบบ Mobile Application</p>
<p>ID: {{$order_id}}</p>

ลูกค้าผู้ติดต่อ:<br/>
ชื่อ : {{$name}}<br/>
เบอร์ : {{$phone}}<br/>


email : {{$email}}<br/>
<br/>
ข้อมูลการขอบริการ<br/>
วันที่ : {{$date}}<br/>
เวลา : {{$time}}<br/>
ประเภทรถ : {{$taxi_type_text}}<br/>
ระยะทาง : {{$distance}}<br/>
ค่าใช้จ่าย : {{$price}}<br/>
<br/>
จุดรับ : <br/>
{{$p1TextAddress}}<br/>
- Lat : {{$p1Lat}}<br/>
- Lng : {{$p1Lng}}<br/>
จุดส่ง : <br/>
{{$p2TextAddress}}<br/>
- Lat : {{$p2Lat}}<br/>
- Lng : {{$p2Lng}}<br/>
<br/>
รายละเอียดอื่นๆ<br/>
{{$remark}}<br/>
<br/>
วันที่ทำรายการ <br>
<?php echo date('d/m/Y  H:i:s');  ?>
<br>Best Regards,<br/>
Mobile Application
