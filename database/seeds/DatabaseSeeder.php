<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Userprofile;
use App\Models\Userfollow;
use App\Models\Userrole;
use App\Models\Userlike;
use App\Models\Setting;
use App\Models\Postlike;
use App\Models\Post;
use App\Models\Postcomment;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Productcategory;
use App\Models\Usergift;
use App\Models\Payment;





class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('TestTableSeeder');
	}
}

	class TestTableSeeder extends Seeder {
	public function run()
	{	
		Userrole::create(array(
			'id'=>1,
			'title'=>'admin',
			'slug'=>'admin',
			'category'=>'admin'
		));
		Userrole::create(array(
			'id'=>2,
			'title'=>'user',
			'slug'=>'user',
			'category'=>'user'
		));
		Userrole::create(array(
			'id'=>3,
			'title'=>'VIP',
			'slug'=>'vip',
			'category'=>'user'
		));
		Userrole::create(array(
			'id'=>4,
			'title'=>'idol',
			'slug'=>'idol',
			'category'=>'idol'
		));
		Userrole::create(array(
			'id'=>5,
			'title'=>'Super idol',
			'slug'=>'super_idol',
			'category'=>'idol'
		));
		Userrole::create(array(
			'id'=>6,
			'title'=>'Supplier',
			'slug'=>'Supplier',
			'category'=>'supplier'
		));
		User::create(array(
			'username'=>'admin',			
			'email'=>'admin@forzoe.com',
			'password'=>Hash::make('111111'),
			'picture'=>'http://lorempixel.com/400/400/',
			'role'=>'1'
		));
		User::create(array(
			'username'=>'supplier',			
			'email'=>'supplier@forzoe.com',
			'password'=>Hash::make('111111'),
			'picture'=>'http://lorempixel.com/400/400/',
			'role'=>'6'
		));
		Userprofile::create(array(
			'firstname'=>'firstname',		
			'lastname'=>'lastname',		
			'user_id'=>2,			
			'birthdate'=>'2014-12-01',			
			'gender'=>1,	
			
		));


		for($i=3;$i<6;$i++){
			$obj_user = User::create(array(
				
				'username'=>'user_'.$i,			
				'email'=>'u'.$i.'@1.com',
				'password'=>Hash::make('111111'),
				'picture'=>'http://lorempixel.com/400/400/',
				'role'=>'2'
			));
			$user_id= $obj_user->id;
			Userprofile::create(array(
				'firstname'=>'user_'.$user_id,		
				'lastname'=>'l_user_'.$user_id,		
				'user_id'=>$user_id,			
				'birthdate'=>'2014-12-01',			
				'gender'=>1,	
				'bio'=>'nice to meet you',	
				'config'=> json_encode(array('chat_request_received'=>1,'gift_received'=>1,'donation_completed'=>1 )),	
				'phone'=>'021231234',	
			));

			$obj_user = User::create(array(
			
			'username'=>'idol_'.$i,				
			'email'=>'i'.$i.'@1.com',
			'password'=>Hash::make('111111'),
			'picture'=>'http://lorempixel.com/400/400/',
			'role'=>'4'
			));
			$user_id= $obj_user->id;
			Userprofile::create(array(
				'firstname'=>'user_'.$user_id,		
				'lastname'=>'l_user_'.$user_id,	
				'user_id'=>$user_id,			
				'birthdate'=>'2014-12-01',			
				'gender'=>2,
				'bio'=>'nice to meet you',	
				'config'=> json_encode(array('chat_request_received'=>1,'gift_received'=>1,'donation_completed'=>1 )),	
				'phone'=>'021231234',	
			));
			
			Payment::create(array(
				'user_id'=>3,
				'coin'=>'100',
				'remark'=>'by credit card : OMISE'
			));
		}
		

		//bank_test
		$obj_user = User::create(array(
			
			'username'=>'banktest_user',			
			'email'=>'banktest_user@gmail.com',
			'password'=>Hash::make('111111'),
			'picture'=>'http://lorempixel.com/400/400/',
			'role'=>'2'
		));
		$user_id= $obj_user->id;
		Userprofile::create(array(
			'firstname'=>'user_'.$user_id,		
			'lastname'=>'l_user_'.$user_id,		
			'user_id'=>$user_id,			
			'birthdate'=>'2014-12-01',			
			'gender'=>1,	
			'bio'=>'nice to meet you',	
			'config'=> json_encode(array('chat_request_received'=>1,'gift_received'=>1,'donation_completed'=>1 )),	
			'phone'=>'021231234',	
		));

		$obj_user = User::create(array(
		
		'username'=>'banktest_idol',				
		'email'=>'banktest_idol@gmail.com',
		'password'=>Hash::make('111111'),
		'picture'=>'http://lorempixel.com/400/400/',
		'role'=>'4'
		));
		$user_id= $obj_user->id;
		Userprofile::create(array(
			'firstname'=>'user_'.$user_id,		
			'lastname'=>'l_user_'.$user_id,	
			'user_id'=>$user_id,			
			'birthdate'=>'2014-12-01',			
			'gender'=>2,
			'bio'=>'nice to meet you',	
			'config'=> json_encode(array('chat_request_received'=>1,'gift_received'=>1,'donation_completed'=>1 )),	
			'phone'=>'021231234',	
		));
		//bank_test

		User::create(array(
			'username'=>'supplier_2',			
			'email'=>'supplier2@forzoe.com',
			'password'=>Hash::make('111111'),
			'picture'=>'http://lorempixel.com/400/400/',
			'role'=>'6'
		));
		User::create(array(
			'username'=>'supplier_3',			
			'email'=>'supplier3@forzoe.com',
			'password'=>Hash::make('111111'),
			'picture'=>'http://lorempixel.com/400/400/',
			'role'=>'6'
		));




		Userlike::create(array(
			'user_id'=>3,				
			'user_like_id'=>4
		));
		Userfollow::create(array(
			'user_id'=>3,				
			'user_follow_id'=>4
		));
		Userlike::create(array(
			'user_id'=>3,				
			'user_like_id'=>8
		));
		Userfollow::create(array(
			'user_id'=>3,				
			'user_follow_id'=>8
		));
		Userlike::create(array(
			'user_id'=>3,				
			'user_like_id'=>6
		));
		Userfollow::create(array(
			'user_id'=>3,				
			'user_follow_id'=>6
		));


		for($i=1;$i<3;$i++){
			Post::create(array(
				'user_id'=>4,
				'like'=>1,
				'latitude'=>13.7468189,
				'longitude'=>100.5327357,
				'post_image'=>'http://lorempixel.com/400/400/',
				'post_text'=>$i.'____Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
			));
			Postcomment::create(array(
				'user_id'=>3,				
				'post_id'=>$i,
				'comment'=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco",
			));

			Postlike::create(array(
				'user_id'=>3,				
				'post_id'=>$i
			));

		}
		for($i=1;$i<3;$i++){
			Post::create(array(
				'user_id'=>3,
				'like'=>1,
				'latitude'=>13.7468189,
				'longitude'=>100.5327357,
				'post_image'=>'http://lorempixel.com/400/400/',
				'post_text'=>$i.'____Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
			));
			Postcomment::create(array(
				'user_id'=>3,				
				'post_id'=>$i,
				'comment'=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco",
			));

			Postlike::create(array(
				'user_id'=>3,				
				'post_id'=>$i
			));
			
		}

		Shop::create(array(
			'user_id'=>'2',
			'title'=>'brand_1',		
			'slug'=>'brand_1',
			'description'=>'2121',
			'picture'=>'http://lorempixel.com/400/400/',
		));


		Shop::create(array(
			'user_id'=>'11',
			'title'=>'brand_2',
				'recommend'=>1,
			'slug'=>'brand_2',
			'description'=>'2121',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Shop::create(array(
			'user_id'=>'12',
			'title'=>'brand_3',
			'slug'=>'brand_3',
			'description'=>'2121',
			'picture'=>'http://lorempixel.com/400/400/',
		));

		Productcategory::create(array(
			'title'=>'For Him',
			'slug'=>'for_him',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));

		Productcategory::create(array(
			'title'=>'For Her',
			'slug'=>'for_her',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'For kid',
			'slug'=>'for_kid',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'Birthday',
			'slug'=>'birthday',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'Anniversary',
			'slug'=>'anniversary',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'Congratulation',
			'slug'=>'congratulation',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'Love',
			'slug'=>'love',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'Missing',
			'slug'=>'missing',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));
		Productcategory::create(array(
			'title'=>'Sorry',
			'slug'=>'sorry',
			'description'=>'',
			'picture'=>'http://lorempixel.com/400/400/',
		));


		for($i=1;$i<7;$i++){
			Product::create(array(
				'title' =>'Product_'.$i,
				'sub_title' =>'Color your day',
				'type' =>0,
				'sku' =>111111,
				'slug' =>'Product_'.$i,
				'coin' =>'20',
				'shop_id' =>'1',
				'category_id'=>'1',
				'description' =>'',
				'picture'=>'http://lorempixel.com/400/400/',
			));
			Usergift::create(array(
				'product_id'=>$i,
				'user_id'=>3,
				'user_get_id'=>4
			));
		}
		for($i=1;$i<7;$i++){
			Product::create(array(
				'title' =>'ElectronicProduct_'.$i,
				'sub_title' =>'Color your day',
				'type' =>1,
				'recommend'=>'1',
				'sku' =>111111,
				'slug' =>'e_Product_'.$i,
				'coin' =>'20',
				'shop_id' =>'1',
				'category_id'=>'1',
				'description' =>'',
				'picture'=>'http://lorempixel.com/400/400/',
			));
			
		}
		
	}
}