<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{



		Schema::create('users_role', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->string('category')->default('user');
			$table->text('remark')->nullable();
			$table->integer('level')->default(0);
			$table->timestamps();
		});


		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('username')->unique();
			$table->integer('score')->default(0); 
			$table->integer('coin')->default(0); 
			$table->string('email')->unique();
			$table->string('password', 60)->nullable();
			$table->string('api_key', 32)->nullable();
			$table->integer('role')->default(1); 			
			$table->string('status')->default(1); 
			$table->string('fbappid', 60)->nullable();
			$table->string('fbsecret', 60)->nullable();
			$table->string('picture')->nullable();
			$table->rememberToken();
			$table->timestamps();
		});
		Schema::create('users_gift', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->references('id')->on('product');
			$table->text('order_descption')->nullable(); //json			
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('user_get_id')->references('id')->on('users');
			$table->text('remark')->nullable();

			$table->timestamps();
		});
		Schema::create('users_profile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');

			$table->string('firstname')->nullable();	
			$table->string('lastname')->nullable();	
			$table->date('birthdate')->nullable();	
			$table->string('phone')->nullable();		
			$table->integer('gender')->nullable();		
			$table->text('profile')->nullable();
			$table->text('bio')->nullable();
			$table->text('config')->nullable();
			$table->timestamps();
		});

		Schema::create('users_follow', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->string('user_follow_id')->references('id')->on('users');
			$table->timestamps();
		});

		Schema::create('users_like', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('user_like_id')->references('id')->on('users');
			$table->timestamps();
		});

		Schema::create('payment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('coin');
			$table->text('remark')->nullable();
			$table->timestamps();
		});

		/*Schema::create('users_score', function(Blueprint $table)
		{
			//$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('user_got_id')->references('id')->on('users');
			$table->integer('score');
			$table->text('description');
			$table->timestamps();
		});*/

		Schema::create('product_category', function(Blueprint $table)
		{

			$table->increments('id');
			$table->string('title');

			$table->string('slug');
			$table->text('description')->nullable();
			$table->string('picture')->nullable();
			$table->timestamps();
		});

		Schema::create('shop', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->string('title');
			$table->boolean('recommend')->default(0); 
			$table->boolean('active')->default(0); 
			$table->string('slug')->nullable();;
			$table->text('description')->nullable();
			$table->string('picture')->nullable();
			$table->timestamps();
		});

		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->string('sub_title');
			$table->boolean('recommend')->default(0); 
			$table->integer('type');  //type : 0 -> gift , 1 -> electrinuc gift
			$table->string('sku');
			$table->integer('score')->default(0); 
			$table->integer('coin_raw')->default(0); 
			$table->boolean('active')->default(0); 
			$table->integer('coin')->default(0); 
			$table->integer('shop_id')->references('id')->on('shop');
			$table->integer('category_id')->references('id')->on('product_category');
			$table->text('description')->nullable();
			$table->string('picture')->nullable();
			$table->timestamps();
		});
		

		Schema::create('post', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('like')->default(0);
			$table->integer('view')->default(0);
			$table->integer('comment_count')->default(0);
			$table->text('post_image')->nullable();
			$table->text('post_text');
			$table->double('latitude', 15, 8);
			$table->double('longitude', 15, 8);

			$table->timestamps();
		});

		
		Schema::create('post_like', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->references('id')->on('post');
			$table->integer('user_id')->references('id')->on('users');

			$table->timestamps();
		});

		Schema::create('post_comment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->references('id')->on('post');
			$table->integer('user_id')->references('id')->on('users');
			$table->text('comment')	;

			$table->timestamps();
		});
		Schema::create('setting',function(Blueprint $table){
			$table->increments('id');
			$table->string('meta_key',255);				
			$table->text('meta_value');	
			$table->text('remark')->nullable();							
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */

	public function down()
	{
		Schema::drop('payment');
		Schema::drop('users_gift');
		Schema::drop('product_category');
		Schema::drop('shop');
		Schema::drop('product');
		Schema::drop('setting');
		Schema::drop('post_like');
		Schema::drop('post_comment');
		Schema::drop('post');
		Schema::drop('users_profile');
		//Schema::drop('users_score');
		Schema::drop('users_like');
		Schema::drop('users_follow');
		Schema::drop('users');
		Schema::drop('users_role');
	}

}
