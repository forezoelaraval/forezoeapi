<?php namespace App\Http\Controllers;
use Debug;
use SubmitImage;
use App\Models\User;
use App\Models\Userprofile;
use App\Models\Post;
use App\Models\Postlike;
use App\Models\Postcomment;

class PostController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	
	public function get_comment(){
		/*
			input 
				int page :
				int post_id :
						
		*/
		if((!isset($_GET['post_id']))){
			return response()->json(array('status' => 0,'error'=>array('post_id'=>'post_id not found')));
		}
		if((!isset($_GET['page']))){
			return response()->json(array('status' => 0,'error'=>array('page'=>'page not found')));
		}


		$arr_return = Postcomment::querycomment($_GET['post_id'],$_GET['page']);
		return response()->json(array('status' => 1,'return'=>$arr_return));


	}
	public function post_delcomment(){
		/*
			input 
				int post_comment_id :
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);
		//$arr_post = \Input::all(); 

		$result  = Postcomment::select('post_id')->where('id',$arr_post['post_comment_id'])->first();

		if($result){
		
			Post::updatecomment($result->post_id,'del');
			Postcomment::select('post_id')->where('id',$arr_post['post_comment_id'])->delete();
		}
		return response()->json(array('status' => 1));
	}

	public function post_comment(){
		/*
			input 
				int user_id :
				int post_id :
				var comment :
						
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);
		//$arr_post = \Input::all(); 
		$result = Postcomment::create(array(
					'user_id'=>$arr_post['user_id'],
					'post_id'=>$arr_post['post_id'],
					'comment'=>$arr_post['comment']
					)
				);

		if($result){
			Post::updatecomment($arr_post['post_id']);
			return response()->json(array('status' => 1));
		}else{
			return response()->json(array('status' => 0,'error'=>array('message'=>'Oops, something wrong happened :( Please try again later!')));
		}
	}

	public function post_like(){
		/*
			input 
				int user_id :
				int post_id :
						
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);
		//$arr_post = \Input::all(); 
		$result = Postlike::create(array(
					'user_id'=>$arr_post['user_id'],
					'post_id'=>$arr_post['post_id']
					)
				);

		if($result){
			Post::updatelike($arr_post['post_id']);
			return response()->json(array('status' => 1));
		}else{
			return response()->json(array('status' => 0,'error'=>array('message'=>'Oops, something wrong happened :( Please try again later!')));
		}
	}
	public function post_upstatus(){
		/*
			input 
				user_id (String)
				comment (String)
				photo (String)
				lat (String)
				long (String)
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);
		//$arr_post = \Input::all(); 

		$arr_insert = array(
			'post_text'=>$arr_post['comment'],			
			'latitude'=>$arr_post['lat'],	
			'longitude'=>$arr_post['long'],	
			'user_id'=>$arr_post['user_id'],	
		);

		// insert image
		if(isset($arr_post['photo'])){
			try{
  				$var_image_url = SubmitImage::insert($arr_post['photo']);
  			} catch (\Exception $e) {
				return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
			}	

			$arr_insert['post_image']=$var_image_url;

		}
		// insert image


		
		try {
			$obj_user = Post::create($arr_insert);
		} catch (\Exception $e) {
			return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
		}	

		return response()->json(array('status' => 1));
	}
	public function post_unlike(){
		/*
			input 
				int user_id :
				int post_id :
						
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);
		//$arr_post = \Input::all(); 
		$result = Postlike::where('user_id',$arr_post['user_id'])
							->where('post_id',$arr_post['post_id'])
							->delete();
		if($result){
			Post::updatelike($arr_post['post_id'],'unlike');
		}
		return response()->json(array('status' => 1));
	}

	public function get_timeline(){
		/*
			input 
				int user_id :
				double lat :
				double lon :
				var page : default 1
				var filter  : global deful
						: nearby  // ยังไม่เสร็จ
						: favorite
						: follow
						
		*/
		if((!isset($_GET['user_id']))){
			return response()->json(array('status' => 0,'error'=>array('user_id'=>'user_id not found')));
		}
		if((!isset($_GET['lat']))){
			return response()->json(array('status' => 0,'error'=>array('latitude'=>'latitude not found')));
		}
		if((!isset($_GET['lon']))){
			return response()->json(array('status' => 0,'error'=>array('longitude'=>'longitude not found')));
		}
		
		$user_id = $_GET['user_id'];
		$lat = $_GET['lat'];
		$lon= $_GET['lon'];

		//---default
			$page=1;
			$filter = 'global';
		//---default
		if(isset($_GET['page'])){
			$page=$_GET['page'];
		}
		if(isset($_GET['filter'])){
			$filter=$_GET['filter'];
		}

		$arr_timeline =	Post::queryTimeline($user_id,$filter,$lat,$lon,$page);
		$arr_return = array();
		foreach ($arr_timeline as $key_timeline => $value_timeline) {
			$arr =array();
			$arr['post_id']=$value_timeline->post_id;
			$arr['like']=$value_timeline->like;
			$arr['view']=$value_timeline->view;
			$arr['comment']=Postcomment::querycomment($value_timeline->post_id);
			$arr['comment_count']=$value_timeline->comment_count;
			$arr['is_like']=Postlike::checklike($user_id,$value_timeline->post_id);
			$arr['post_user_id']=$value_timeline->user_post_id;
			$arr['post_username']=$value_timeline->user_username_post;
			if($value_timeline->picture!=''){
				$arr['post_user_picture']=asset($value_timeline->picture); //---> url
			}else{
				$arr['post_user_picture']='';
			}
			$arr['post_image']=$value_timeline->post_image;
			$arr['post_text']=$value_timeline->post_text;
			$arr['created_at']=$value_timeline->created_at->format('Y-m-d H:i:s');
			array_push($arr_return, $arr);
		}
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	public function post_index(){
		return response()->json(array('status'=>'0','error_message'=>'hi'));
			exit;
		$postdata = file_get_contents("php://input");
   		$arr_post = json_decode($postdata,true);
	}

	public function post_view(){
		/*
			input 
				int post_id :
		*/
		$postdata = file_get_contents("php://input");
   		$arr_post = json_decode($postdata,true);

   			Post::updateview($arr_post['post_id']);
			return response()->json(array('status' => 1));

	}

}
