<?php namespace App\Http\Controllers;

use Debug;
use Formate;
use Auth;
use SubmitImage;
use Socialize;

use UserController;

use App\Models\User;
use App\Models\Userprofile;
class AccountController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	

	public function post_register(){
		/*
			input 
				username
				birthdate
				gender
				email
				password
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);

    	$rules = array(
    	    'email' => 'email|unique:users',
    	    'username' => 'unique:users',
    	);
    	$arr_input =array(
    		'email'=>$arr_post['email'],
    		'username'=>$arr_post['username']
    	);

    	$message =  array();
    	$validator = \Validator::make($arr_input,$rules,$message);
    	if($validator->fails()) {
    		$arr_return_error = Formate::error_arr($validator); // ใช้สำหรับ สร้าง arr ของ error input
    		return response()->json(array('status' => 0,'error'=>$arr_return_error));
    		exit;
    	}

		$arr_user = array(
			'username'=>$arr_post['username'],			
			'email'=>$arr_post['email'],	
			'password'=>\Hash::make($arr_post['password']),	
		);
		try {
			$obj_user = User::create($arr_user);
		} catch (\Exception $e) {
			return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
		}	
		
		$user_id= $obj_user->id;

		$arr_user_profile = array(
			'user_id'=>$user_id,
			'birthdate'=>$arr_post['birthdate'],
			'gender'=>$arr_post['gender'],	
		);

		try {
			Userprofile::create($arr_user_profile);
		} catch (\Exception $e) {
			return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
		}	

		$userdata = array(
			'email'     => $arr_post['email'],
			'password'  => $arr_post['password']
		);
		

		if (\Auth::attempt($userdata)) {
			$arr_return = User::getprofile(Auth::user()->id);
			return response()->json(array('status' => 1,'return'=>$arr_return));
		}else{
			return response()->json(array('status' => 0,'error'=>array('emaill'=>'Oops, something wrong happened :( Please try again later!')));
		}

	}
	public function post_login(){
		/*
			input 
				var email : เก็บ email และ username
				var password :
		*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);


		//$arr_post = \Input::all();
		$rules = array(
			'email' =>'required|login:'.$arr_post['email'].','.$arr_post['password'],
			'password' =>'required'
		);
		$message =  array('login' => 'Invalid email username or password, please try' );
		\Validator::extend('login', function($attribute, $value, $parameters)
		{
			//echo $parameters[0];
			$arr_user = User::select('password','email')->orwhere('email',$parameters[0])->orwhere('username',$parameters[0])->first();
			
			if($arr_user){	
				if(\Hash::check($parameters[1],$arr_user->password ))
				return true;	
			}
			return false;
		});
		$validator = \Validator::make($arr_post,$rules,$message);
		if($validator->fails()) {
			$arr_return_error = \Formate::error_arr($validator); // ใช้สำหรับ สร้าง arr ของ error input

			return response()->json(array('status' => 0,'error'=>$arr_return_error));
		}else{
			
			//----- after login

			if(Debug::checkemail($arr_post['email'])){
				$var_email = $arr_post['email'];
			}else{
				$arr_user =  User::select('email')->orwhere('email',$arr_post['email'])->orwhere('username',$arr_post['email'])->first();
				$var_email=$arr_user->email;
			}

				$userdata = array(
					'email'     => $var_email,
					'password'  => $arr_post['password']
				);
				

				if (\Auth::attempt($userdata)) {

					$arr_return = User::getprofile(Auth::user()->id);

					return response()->json(array('status' => 1,'return'=>$arr_return));
				}else{
					return response()->json(array('status' => 0,'error'=>array('emaill'=>'Oops, something wrong happened :( Please try again later!')));
				}
			//----- after login
		}


	}
	public function post_index(){
		return response()->json(array('status'=>'0','error_message'=>'hi'));
			exit;
		$postdata = file_get_contents("php://input");
   		$arr_post = json_decode($postdata,true);
	}


}
