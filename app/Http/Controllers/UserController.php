<?php namespace App\Http\Controllers;

use Auth;
use Formate;
use Debug;
use SubmitImage;
use App\Models\User;
use App\Models\Post;
use App\Models\Userfollow;
use App\Models\Userlike;
use App\Models\Postcomment;
use App\Models\Postlike;
use App\Models\Userprofile;
class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function get_followers(){
		/*
			input 
				(int) user_id :
		*/
		if(!isset($_GET['user_id'])){
			return response()->json(array('status' => 0,'error'=>array('user_id'=>'user_id not found')));
		}
		$user_id = $_GET['user_id'];

		$int_count =  Userfollow::where('user_id',$user_id)->count();
		return response()->json(array('status' => 1,'return'=>$int_count));

	}
	public function get_following(){
		/*
			input 
				(int) user_id :
		*/
		if(!isset($_GET['user_id'])){
			return response()->json(array('status' => 0,'error'=>array('user_id'=>'user_id not found')));
		}
		$user_id = $_GET['user_id'];

		$int_count =  Userfollow::where('user_follow_id',$user_id)->count();
		return response()->json(array('status' => 1,'return'=>$int_count));

	} 
	public function get_mypic(){
		/*
			input 
				(int) user_id :
				(int) page :
		*/
		$page = 1;
		if(!isset($_GET['user_id'])){
			return response()->json(array('status' => 0,'error'=>array('user_id'=>'user_id not found')));
		}
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}
		$user_id = $_GET['user_id'];

		$arr_timeline =	Post::querymypic($user_id,$page);
		$arr_return= array();
		foreach ($arr_timeline as $key_timeline => $value_timeline) {
			$arr =array();
			$arr['post_id']=$value_timeline->post_id;
			$arr['like']=$value_timeline->like;
			$arr['view']=$value_timeline->view;
			$arr['comment']=Postcomment::querycomment($value_timeline->post_id);
			$arr['comment_count']=$value_timeline->comment_count;
			$arr['is_like']=Postlike::checklike($user_id,$value_timeline->post_id);
			$arr['post_user_id']=$value_timeline->user_post_id;
			$arr['post_username']=$value_timeline->user_username_post;
			if($value_timeline->picture!=''){
				$arr['post_user_picture']=asset($value_timeline->picture); //---> url
			}else{
				$arr['post_user_picture']='';
			}
			$arr['post_image']=$value_timeline->post_image;
			$arr['post_text']=$value_timeline->post_text;
			$arr['created_at']=$value_timeline->created_at->format('Y-m-d H:i:s');
			array_push($arr_return, $arr);
		}
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	public function get_mypost(){
		/*
			input 
				(int) user_id :
				(int) page :
		*/
		$page = 1;
		if(!isset($_GET['user_id'])){
			return response()->json(array('status' => 0,'error'=>array('user_id'=>'user_id not found')));
		}
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}
		$user_id = $_GET['user_id'];

		$arr_timeline =	Post::querymypost($user_id,$page);
		$arr_return= array();
		foreach ($arr_timeline as $key_timeline => $value_timeline) {
			$arr =array();
			$arr['post_id']=$value_timeline->post_id;
			$arr['like']=$value_timeline->like;
			$arr['view']=$value_timeline->view;
			$arr['comment']=Postcomment::querycomment($value_timeline->post_id);
			$arr['comment_count']=$value_timeline->comment_count;
			$arr['is_like']=Postlike::checklike($user_id,$value_timeline->post_id);
			$arr['post_user_id']=$value_timeline->user_post_id;
			$arr['post_username']=$value_timeline->user_username_post;
			if($value_timeline->picture!=''){
				$arr['post_user_picture']=asset($value_timeline->picture); //---> url
			}else{
				$arr['post_user_picture']='';
			}
			$arr['post_image']=$value_timeline->post_image;
			$arr['post_text']=$value_timeline->post_text;
			$arr['created_at']=$value_timeline->created_at->format('Y-m-d H:i:s');
			array_push($arr_return, $arr);
		}
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}

	public function post_like(){
		/*
			input 
				user_id
				user_like_id
		*/
			$postdata = file_get_contents("php://input");
	   		$arr_post = json_decode($postdata,true);

	   		$int_count  = Userlike::where('user_id',$arr_post['user_id'])->where('user_like_id',$arr_post['user_like_id'])->get()->count();	   				
	   		if($int_count==0){
	   			$result = Userlike::create(array(
	   						'user_id'=>$arr_post['user_id'],
	   						'user_like_id'=>$arr_post['user_like_id']
	   						)
	   			);	
	   			if($result){
	   				return response()->json(array('status' => 1));
	   			}else{
	   				return response()->json(array('status' => 0,'error'=>array('message'=>'Oops, something wrong happened :( Please try again later!')));
	   			}

	   		}else{
	   			return response()->json(array('status' => 1));
	   		}

	   		
	}
	public function post_unlike(){
				/*
				input 
					user_id
					user_like_id
			*/
				$postdata = file_get_contents("php://input");
		   		$arr_post = json_decode($postdata,true);

		   		$result = Userlike::where('user_id',$arr_post['user_id'])->where('user_like_id',$arr_post['user_like_id'])->delete();	   				

		   		if($result){
		   			return response()->json(array('status' => 1));
		   		}else{
		   			return response()->json(array('status' => 0,'error'=>array('message'=>'Oops, something wrong happened :( Please try again later!')));
		   		}
	}

	public function post_follow(){
		/*
		input 
			user_id
			user_follow_id
	*/
		$postdata = file_get_contents("php://input");
   		$arr_post = json_decode($postdata,true);

   		$int_count  = Userfollow::where('user_id',$arr_post['user_id'])->where('user_follow_id',$arr_post['user_follow_id'])->get()->count();	   				
   		if($int_count==0){
   			$result = Userfollow::create(array(
   						'user_id'=>$arr_post['user_id'],
   						'user_follow_id'=>$arr_post['user_follow_id']
   						)
   			);	
   			if($result){
   				return response()->json(array('status' => 1));
   			}else{
   				return response()->json(array('status' => 0,'error'=>array('message'=>'Oops, something wrong happened :( Please try again later!')));
   			}

   		}else{
   			return response()->json(array('status' => 1));
   		}

   		
	}
	public function post_unfollow(){
			/*
			input 
				user_id
				user_follow_id
		*/
			$postdata = file_get_contents("php://input");
	   		$arr_post = json_decode($postdata,true);

	   		$result = Userfollow::where('user_id',$arr_post['user_id'])->where('user_follow_id',$arr_post['user_follow_id'])->delete();	   				

	   		if($result){
	   			return response()->json(array('status' => 1));
	   		}else{
	   			return response()->json(array('status' => 0,'error'=>array('message'=>'Oops, something wrong happened :( Please try again later!')));
	   		}
	}   
	public function post_updatepicture(){
	/*
		input 
			user_id
			upload  : file image
	*/
		$arr_post =  \Input::all();		

	
	//	$postdata = file_get_contents("php://input");
	//	$arr_post = json_decode($postdata,true);
		
		$user_id=$arr_post['user_id'];
		$arr_user_old = User::select('username','email','picture')
					->where('id',$user_id)
					->first();

		if(isset($arr_post['upload'])){

			// insert image
			   try {
			  		$var_image_url = SubmitImage::insert($arr_post['upload']);
			  		} catch (\Exception $e) {
						return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
					}	
			// insert image
			$arr_user['picture']=$var_image_url;
			//มีค่า  ให้ delete รูปเก่า แล้ว uploadใหม่
			SubmitImage::delete($arr_user_old->picture);	
		}


		$user_id=$arr_post['user_id'];
		try {
			$obj_user = User::where('id',$user_id)->update($arr_user);
		} catch (\Exception $e) {
			return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
		}

		$arr_return = User::getprofile($user_id);
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	public function post_editprofile(){
	/*
		input 
			user_id
			username
			firstname
			lastname
			birthdate
			gender
			upload  : file image
			email
			phone
			password
	*/
		$postdata = file_get_contents("php://input");
		$arr_post = json_decode($postdata,true);
		//$arr_post = \Input::all(); 
			
		$arr_user =array();
		$arr_user_profile =array();
	
		$user_id=$arr_post['user_id'];
		$arr_user_old = User::select('username','email')
					->where('id',$user_id)
					->first();
		
		// get old data
			//password 
				if(isset($arr_post['password'])&&$arr_post['password']!=''){
					$arr_user['password'] =\Hash::make($arr_post['password']);	
				}
			//username
				if(isset($arr_post['username'])){
					if($arr_post['username']!=$arr_user_old->username){
						if(User::hasUsername($arr_post['username'])){
							return response()->json(array('status' => 0,'error'=>array('username'=>"The username has already been taken.")));
						}
					}

					$arr_user['username']=$arr_post['username'];
				}

			//email
				if(isset($arr_post['email'])){
					if($arr_post['email']!=$arr_user_old->email){
						if(User::hasEmail($arr_post['email'])){
							return response()->json(array('status' => 0,'error'=>array('email'=>"The email has already been taken.")));
						}
					}
					$arr_user['email']=$arr_post['email'];
				}	

			//image
				if(isset($arr_post['upload'])){

					// insert image
					   try {
					  		$var_image_url = SubmitImage::insert($arr_post['upload']);
					  		} catch (\Exception $e) {
								return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
							}	
					// insert image
					$arr_user['picture']=$var_image_url;
					//มีค่า  ให้ delete รูปเก่า แล้ว uploadใหม่
					SubmitImage::delete($arr_user_old->picture);	
				}
				
			//firstname
				if(isset($arr_post['firstname'])){
					$arr_user_profile['firstname'] = $arr_post['firstname'];	
				}

			//birthdate
				if(isset($arr_post['birthdate'])){
					$arr_user_profile['birthdate'] = $arr_post['birthdate'];	
				}

			//lastname
				if(isset($arr_post['lastname'])){
					$arr_user_profile['lastname'] = $arr_post['lastname'];	
				}

			//gender
				if(isset($arr_post['gender'])){
					$arr_user_profile['gender'] = $arr_post['gender'];	
				}

			//phone
				if(isset($arr_post['phone'])){
					$arr_user_profile['phone'] = $arr_post['phone'];	
				}
			//bio
				if(isset($arr_post['bio'])){
					$arr_user_profile['bio'] = $arr_post['bio'];	
				}

		


		//edit
		if(count($arr_user)>0){
			
			try {
				$obj_user = User::where('id',$user_id)->update($arr_user);
			} catch (\Exception $e) {
				return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
			}
		}
		if(count($arr_user_profile)>0){
			try {
				Userprofile::where('user_id',$user_id)->update($arr_user_profile);
			} catch (\Exception $e) {
				return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
			}	
		}

		
			$arr_return = User::getprofile($user_id);
			return response()->json(array('status' => 1,'return'=>$arr_return));
		
	}
	public function post_test(){
		$arr_post =  \Input::all();		
		if(isset($arr_post['picture'])){

			// insert image
			   try {
			  		$var_image_url = SubmitImage::insert($arr_post['picture']);
			  		} catch (\Exception $e) {
						return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
					}	
			// insert image
			$arr_user['picture']=$var_image_url;
			//มีค่า  ให้ delete รูปเก่า แล้ว pictureใหม่
			//SubmitImage::delete($arr_user_old->picture);	
		}
		//$postdata = file_get_contents("php://input");
		//$arr_post = json_decode($postdata,true);
		return response()->json(array('status' => 1,'return'=>$arr_user));
	}

	public function get_profile(){
		/*
			input 
				(int) user_id :
		*/
		if(!isset($_GET['user_id'])){
			return response()->json(array('status' => 0,'error'=>array('user_id'=>'user_id not found')));
		}
		$user_id = $_GET['user_id'];
		$arr_user = User::select('username','picture','email','coin')
					->where('id',$user_id)
					->first();
		if(!$arr_user){
			return response()->json(array('status' => 0,'error'=>array('id'=>'user date not found')));	
		}

		$arr_user_profile = Userprofile::select('firstname','lastname','birthdate','phone','gender','bio','config')
					->where('user_id',$user_id)
					->first();

		$arr_return['user_id']=$user_id;
		$arr_return['followers']=Userfollow::where('user_id',$user_id)->count();
		$arr_return['following']=Userfollow::where('user_follow_id',$user_id)->count();
		$arr_return['bio']=$arr_user_profile->bio;
		$arr_return['username']=$arr_user->username;
		$arr_return['firstname']=$arr_user_profile->firstname;
		$arr_return['lastname']=$arr_user_profile->lastname;
		if($arr_user->picture!=''){
			$arr_return['picture']=asset($arr_user->picture); //---> url
		}else{
			$arr_return['picture']='';
		}
		
		$arr_return['email']=$arr_user->email;
		$arr_return['coin']=$arr_user->coin;
		$arr_return['birthdate']=$arr_user_profile->birthdate;
		$arr_return['age']=User::getAge($arr_user_profile->birthdate);
		$arr_return['phone']=$arr_user_profile->phone;
		$arr_return['gender']=$arr_user_profile->gender;
		
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	
	public function post_index(){
		
		$postdata = file_get_contents("php://input");
   		$arr_post = json_decode($postdata,true);
	}


}
