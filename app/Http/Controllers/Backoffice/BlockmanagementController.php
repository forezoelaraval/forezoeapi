<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Block;
use App\Models\Page;

use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class BlockmanagementController extends Controller {

	public $TITLE = 'Block ';
	public $CONTROLLER = 'blockmanagement';
	public $TABLE = 'block';
	public $NAV = 'nav-home-a';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	
	public function get_home($page_id){
		$result_data = Block::where('page_id',$page_id)->get();
		$arr_page = Page::queryprofile($page_id);
		
		/*-------head ---------*/
			$title= $this->TITLE.' : '.$arr_page['title'];
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-plus',
					'href' => $this->CONTROLLER.'/insert/'.$page_id,
					'title' => 'เพิ่ม',
					),				
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('Picture'=>'picture','Title'=>'title_1','Sub title'=>'title_2','Description'=>'description'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>$this->CONTROLLER.'/edit/'.$page_id,
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						//'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}
public function get_edit($page_id,$id){
		$result_data =\DB::table($this->TABLE)->where('id',$id)->first();
		$page_id = $result_data->page_id;
		
		$title= 'Edit';
					$sidebar = array(
						'sidebar'=>''							
					);
					$breadcrumb = array(
						array('url'=>asset($this->CONTROLLER.'/home/'.$page_id),'title'=>$this->TITLE),
						array('url'=>'#','title'=>'Edit')				
						);
					$button =array(							
						array(
							'icon' => 'fa fa-reply',
							'href' => $this->CONTROLLER.'/home/'.$page_id,
							'title' => 'ย้อนกลับ',
							),
						);
					
					$form =array(
						//'post'=>$this->CONTROLLER.'/???',
						'success'=>$this->CONTROLLER.'/home/'.$page_id,
						'files'=>'1',
						'rule'=>array(		    		
							'title_1' =>'required',
							'picture'=>'max:1000|image',
							),
						'type'=>'edit',						
						'data'=>$result_data,
						'table'=>$this->TABLE,
						//'unset'=>array(''),
						'input'=>array(
							'text' => array('title_1','title_2'),
							'textarea' => array('description'),
							'image'=> array('picture'=>asset($result_data->picture))					
							)
						);
				
				$data['form']=$form;
				$data['sidebar']=$sidebar;
				$data['title']=$title;
				$data['button']=$button;
				$data['breadcrumb']=$breadcrumb;
				return view('backoffice/category/form',$data);
		
	}
	public function get_insert($page_id){
		$title= 'Insert';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home/'.$page_id),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);
			
			$form =array(
				//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/home/'.$page_id,
				'files'=>'1',
				'rule'=>array(
					'title_1' =>'required',
					'picture'=>'required|max:1000|image',
					),
				//'type'=>'edit',
				'table'=>$this->TABLE,
				'input'=>array(
					'hidden'=>array('page_id'=>$page_id),
					'text' => array('title_1','title_2'),
					'textarea' => array('description'),
					'image'=> array('picture'=>'')
					)
				);
		
		$data['form']=$form;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_form',$data);
		
	}	
	
}
?>