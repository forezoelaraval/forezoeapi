<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;

use App\Models\Homedisplay;

use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class HomedisplaymanagementController extends Controller {

	public $TITLE = 'display';
	public $CONTROLLER = 'homedisplaymanagement';
	public $TABLE = 'home_display';
	public $NAV = 'nav-home';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_delete(){
		
	}
	public function get_home(){
		$result_data = Homedisplay::get();
		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-plus',
					'href' => $this->CONTROLLER.'/insert',
					'title' => 'เพิ่ม',
					),				
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('Picture'=>'picture','Title'=>'title','Sub title'=>'subtitle'),
				'tbody' =>$result_data,
				'tools'=>array(
					
					array(
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>$this->CONTROLLER.'/edit',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						//'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}

	public function get_insert(){
		$title= 'Insert';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);
			
			$form =array(
				//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/profile',
				'files'=>'1',
				'rule'=>array(		    		
					'title' =>'required',
					'subtitle' =>'required',
					'picture'=>'required|max:1000|image',
					),
				'table'=>$this->TABLE,
				'input'=>array(
					'text' => array('title','url'),
					'textarea' => array('subtitle'),	
					'image'=> array('picture'=>'')
					)
				);
		
		$data['form']=$form;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/homedisplay/form',$data);
		
	}	
	public function get_edit($id){
		$result_data =\DB::table($this->TABLE)->where('id',$id)->first();
		
		$title= 'Edit';
					$sidebar = array(
						'sidebar'=>''							
					);
					$breadcrumb = array(
						array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
						array('url'=>'#','title'=>'Edit')				
						);
					$button =array(							
						array(
							'icon' => 'fa fa-reply',
							'href' => $this->CONTROLLER.'/home',
							'title' => 'ย้อนกลับ',
							),
						);
					
					$form =array(
						//'post'=>$this->CONTROLLER.'/???',
						'success'=>$this->CONTROLLER.'/profile',
						'files'=>'1',
						'rule'=>array(		    		
							'title' =>'required',
							'subtitle' =>'required',
							'picture'=>'max:1000|image',
							//'icon' => 'required'
							),
						'type'=>'edit',						
						'data'=>$result_data,
						'table'=>$this->TABLE,
						//'unset'=>array(''),
						'input'=>array(
							'text' => array('title','url'),
							'textarea' => array('subtitle'),	
							'image'=> array('picture'=>asset($result_data->picture))					
							)
						);
				
				$data['form']=$form;
				$data['sidebar']=$sidebar;
				$data['title']=$title;
				$data['button']=$button;
				$data['breadcrumb']=$breadcrumb;
				return view('backoffice/homedisplay/form',$data);
		
	}
	public function get_profile($id){
		$result_data =\DB::table($this->TABLE)->where('id',$id)->first();
		
		/*-------head ---------*/		
			$title= $this->TITLE.' : # '.$id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-1'							
			);
			$button =array(				
				array(
					'icon' => 'fa fa-pencil-square-o',
					'href' => $this->CONTROLLER.'/edit/'.$id,
					'title' => 'แก้ไข',
					),
			
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
			);
		/*------profile--------*/
		
			$profile = array(
				'title' =>$result_data->title,
				
				'picture'=>SubmitImage::get($result_data->picture),
				'label' =>array(
					'sub title' =>'subtitle', 					
					'url' =>'url', 					
				 ),
				'data' =>$result_data ,
				);
			
		
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_profile',$data);
	}
}
?>