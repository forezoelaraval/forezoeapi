<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Productcategory;
use App\Models\Product;
use App\Models\Shop;



use Formate;
use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class ProductmanagementController extends Controller {

	public $TITLE = 'Product';
	public $CONTROLLER = 'productmanagement';
	public $TABLE = 'product';
	public $NAV = 'nav-product';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}

	public function get_home(){
		$str_search ='';
		if(isset($_GET['search'])&&$_GET['search']!=''){
			$str_search =$_GET['search'];
		}
		$result_data = Product::queryall('','',$str_search,'');

		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-plus',
					'href' => $this->CONTROLLER.'/insert',
					'title' => 'เพิ่ม',
					),				
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('picture'=>'picture','title'=>'title','category'=>'category','created at'=>'created_at'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(

						'icon'=>'fa fa-file-text-o',
						'title'=>'View',
						'href'=>$this->CONTROLLER.'/view',
						'class'=>'btn btn-primary',
						),
					array(
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>$this->CONTROLLER.'/edit',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['str_search']=$str_search;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/product/table',$data);
	}
	
	public function post_delete(){
		
		Product::del($_POST['id']);
		
		Session::flash('message', 'Delete Success');
		return \Redirect::back();
	}
	public function get_insert($shop_id=''){

		$arr_shop = Shop::getselect();
		$arr_category = Productcategory::getselect();
		$title= 'Insert';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);
			
			$form =array(
				//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/view',
				'files'=>'1',
				'rule'=>array(
					'title' =>'required',
					'score' =>'required|integer',
					'coin_raw' =>'integer',
					'coin' =>'required|integer',
					'picture' => 'required|max:2000|image',							
					),
				//'type'=>'edit',
				'table'=>$this->TABLE,
				'slug'=>'title',
				'input'=>array(
					'text' => array('title','sku','score','coin_raw','coin'),

					'textarea' => array('description'),
					'select'=>array(
						'category_id'=>$arr_category,
						'shop_id'=>$arr_shop,
						'active'=>array('1'=>'Active','0'=>'Pending'),
						),
					'image'=>array('picture'=>'picture'),
					),

					
				);
		
		$data['form']=$form;
		$data['shop_id']=$shop_id;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/product/insert',$data);
		
	}	
	


	public function get_edit($id){
		$result_data = Product::find($id);
		$arr_shop = Shop::getselect();
		$arr_category = Productcategory::getselect();
		$title= 'Insert';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);
			
			$form =array(
				//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/view/'.$id,
				'files'=>'1',
				'rule'=>array(
					'title' =>'required',
					'score' =>'required|integer',
					'coin_raw' =>'integer',
					'coin' =>'required|integer',
					'picture' => 'max:2000|image',							
					),
				'type'=>'edit',
				'data'=>$result_data,
				'table'=>$this->TABLE,
				'slug'=>'title',
				'input'=>array(
					'text' => array('title','sku','score','coin_raw','coin'),
					'textarea' => array('description'),
					'select'=>array(
						'category_id'=>$arr_category,
						'shop_id'=>$arr_shop,
						'active'=>array('1'=>'Active','0'=>'Pending'),
						),
					'image'=>array('picture'=>asset($result_data->picture)),
					),

					
				);
		
		$data['form']=$form;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_form',$data);
	}
	public function get_view($id){
		
		$arr_product =Product::find($id);

		$active_icon ='<i class="fa fa-check" aria-hidden="true"></i>';
		if($arr_product->active=='0'){
			$active_icon ='<i class="fa fa-times" aria-hidden="true"></i>';
		}
		/*

		*/
		$arr_category =Productcategory::select('title')->where('id',$arr_product->category_id)->first();
		$arr_shop =Shop::find($arr_product->shop_id);
		
		/*-------head ---------*/		
			$title= $this->TITLE.' : # '.$id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-2'							
			);
			$button =array(				
				array(
					'icon' => 'fa fa-pencil-square-o',
					'href' => $this->CONTROLLER.'/edit/'.$id,
					'title' => 'แก้ไข',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => asset('productmanagement/home/'.$arr_product->category_id),
					'title' => 'ย้อนกลับ',
					),
			);
		/*------profile--------*/
			if($arr_product->active==1){
				$str_status = '(Active)';
			}else{
				$str_status = '(Pendding)';
			}
			$profile = array(
				'title' =>$arr_product->title .' ( active '.$active_icon.' )',				
				'picture'=>asset($arr_product->picture),
				'label' =>array(
					'Slug' =>'slug',
					'รายละเอียด' =>'description'
				 ),
				'data' =>$arr_product ,
				);
			
		
		$data['arr_shop']=$arr_shop;
		$data['arr_category']=$arr_category;
		$data['arr_product']=$arr_product;
		$data['arr_product_gallery']=array();
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['product_id']=$id;
		
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/product/view',$data);
	}








	/// -------gallery
	public function post_submitproductgallery(){
		$arr_post=\Input::all();
		
		$rule=array(	
			'picture'=>'required|max:1000|image',
		);

		$validator = \Validator::make($arr_post['data'],$rule);
			if($validator->fails()){					
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
		}	
		$arr_post['data']['picture'] = SubmitImage::insert($arr_post['data']['picture']);
		
		Productgallery::create($arr_post['data']);
		Session::flash('message', 'Success : Upload image to Database Success');
		return \Redirect::back();
	}
	public function post_gallerydel(){
		$result_gallery =Productgallery::where('id',$_POST['id'])->first();
		SubmitImage::delete($result_gallery->picture);
		$boolean = Productgallery::where('id',$_POST['id'])->delete();
		if($boolean){
			Session::flash('message','Product gallery # '.$_POST['id'].' has deleted!.');
			return \Redirect::back();
		}
	}
	public function get_gallery($product_id=''){
			// ----  check ---- 
		//  กรณี insert ครั้งแรก
		$arr_product = Session::get('product');
		
		if(count($arr_product)>0){
			$process['title']='2. Insert gallery !!';
			$process['subtitle']='Product insert progress';
			$process['process_text']='75%';
			$process['process']='75';	
			$process['nextpath']='productmanagement/successproduct/'.$product_id;		
			$data['process']=$process;
		}
	// ----  check ---- 	
			
		$arr_product_gallery =Productgallery::where('product_id',$product_id)->get();	


		$title= 'Product Gallery :#'.$product_id;
			$sidebar = array(
				'subsidebar'=>'nav-product-2'							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>asset($this->CONTROLLER.'/view/'.$product_id),'title'=>'Product : # '.$product_id),
				array('url'=>'#','title'=>'gallery')			
				);
			if(!isset($process)){
				$button =array(										
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/view/'.$product_id,
					'title' => 'ย้อนกลับ',
					),
				);
			}else{
				$button=null;
			}
			
		$data['product_id']=$product_id;
		
		$data['arr_product_gallery']=$arr_product_gallery;
		$data['sidebar']=$sidebar;
			$data['title']=$title;
			$data['button']=$button;
			$data['breadcrumb']=$breadcrumb;
		return view('backoffice/product/gallery',$data);
	}
	/// -------gallery
}
?>