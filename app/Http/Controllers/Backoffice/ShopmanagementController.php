<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\User;
use App\Models\Userprofile;

use App\Models\Payment;
use App\Models\Product;
use App\Models\Productcategory;
use App\Models\Shop;


use Debug;
use Formate;
use Session;
use SubmitImage;

class ShopmanagementController extends Controller {

	public $TITLE = 'Shop';
	public $CONTROLLER = 'shopmanagement';
	public $TABLE = 'user';
	public $NAV = 'nav-user';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_delete(){
		Userprofile::where('user_id',$_POST['id'])->delete();
		User::where('id',$_POST['id'])->delete();
		Session::flash('message', $_POST['table'].' # '.$_POST['id'].' has deleted!.');
		return \Redirect::back();
	}


	public function get_home(){
		$str_search ='';
		$str_role ='';
		if(isset($_GET['search'])){
			$str_search =  $_GET['search'];		
		}
		if(isset($_GET['str_role'])){
			$str_search =  $_GET['str_role'];		
		}

		$result_data = Shop::queryshop(30);

		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
					array(
						'icon' => 'fa fa-plus',
						'href' => $this->CONTROLLER.'/insert',
						'title' => 'เพิ่ม',
						),			
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('title'=>'title','created_at'=>'created_at'),
				'tbody' =>$result_data,
				'tools'=>array(

					array(
						'icon'=>'fa fa-file-text-o',
						'title'=>'Profile',
						'href'=>$this->CONTROLLER.'/profile',
						'class'=>'btn btn-primary',
						),
					array(
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>$this->CONTROLLER.'/edit',
						'class'=>'btn btn-primary',
						),
					array(
						'icon' => 'fa fa-list',
						'href' => $this->CONTROLLER.'/listproduct',
						'title' => 'รายชื่อสินค้า',
						'class'=>'btn btn-primary',
					),		
					),
				'toolsDel'=>array(
						'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['str_search']=$str_search;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/user/table',$data);
	}

	public function get_listproduct($shop_id){
		$str_search ='';
		if(isset($_GET['search'])&&$_GET['search']!=''){
			$str_search =$_GET['search'];
		}
		$arr_shop = Shop::find($shop_id);
		$result_data = Product::queryall('','',$str_search,$shop_id);

		/*-------head ---------*/
			$title= $arr_shop->title;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>asset($this->CONTROLLER.'/profile/'.$shop_id),'title'=>$title),
				array('url'=>'#','title'=>'list product')
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
					array(
						'icon' => 'fa fa-plus',
						'target_blank'=>1,
						'href' => 'productmanagement/insert/'.$shop_id,
						'title' => 'เพิ่ม',
						),			
				);	
		/*-------table ---------*/
			$table = array( 
				'thead' => array('picture'=>'picture','title'=>'title','category'=>'category','created at'=>'created_at'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'target_blank'=>1,
						'icon'=>'fa fa-file-text-o',
						'title'=>'View',
						'href'=>'productmanagement/view',
						'class'=>'btn btn-primary',
						),
					array(
						'target_blank'=>1,
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>'productmanagement/edit',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['shop_id']=$shop_id;
		$data['button']=$button;
		$data['str_search']=$str_search;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		
		return view('backoffice/shop/product_table',$data);
	}
	
	public function get_profile($shop_id){

		$result_data =Shop::queryprofile($shop_id);
		$active_icon ='<i class="fa fa-check" aria-hidden="true"></i>';
		if($result_data->active=='0'){
			$active_icon ='<i class="fa fa-times" aria-hidden="true"></i>';
		}
		/*-------head ---------*/		
			$title= $result_data->title.' : # '.$shop_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-1'							
			);
			$button =array(	
							
				array(
					'icon' => 'fa fa-pencil-square-o',
					'href' => $this->CONTROLLER.'/edit/'.$shop_id,
					'title' => 'แก้ไข',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);		
		/*------profile--------*/
		
			$profile = array(
				'title' =>$result_data->title.' ( active : '.$active_icon.')' ,
				'subtitle' =>'<p>'.$result_data->firstname.' '.$result_data->lastname.'<p/><i class="fa fa-mobile" aria-hidden="true"></i> : '.$result_data->phone,
				
				'picture'=>$result_data->picture,
				'label' =>array(

					'อีเมล'=>'email',
					array('value'=>$result_data->description),
				 ),
				'data' =>$result_data ,
				);
			
		
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_profile',$data);
	}

	


		public function get_edit($shop_id){
			$result_data = new \stdClass(); 
			$arr_shop=  Shop::find($shop_id);
			$arr_user=  User::find($arr_shop->user_id);
			$arr_userprofile=  Userprofile::where('user_id',$arr_shop->user_id)->first();
			$result_data->username =$arr_user->username;
			$result_data->email =$arr_user->email;

			$result_data->firstname=$arr_userprofile->firstname;
			$result_data->lastname=$arr_userprofile->lastname;
			$result_data->phone=$arr_userprofile->phone;

			$result_data->slug=$arr_shop->slug;
			$result_data->active=$arr_shop->active;
			$result_data->shop_title=$arr_shop->title;
			$result_data->shop_description=$arr_shop->description;
			$result_data->shop_picture=$arr_shop->picture;
			
		$title= 'Edit';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>asset($this->CONTROLLER.'/profile/'.$shop_id),'title'=>$arr_shop->title),
				array('url'=>'#','title'=>'edit')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/profile/'.$shop_id,
					'title' => 'ย้อนกลับ',
					),
				);		
			$form =array(
				'post'=>$this->CONTROLLER.'/edit',
				'files'=>'1',
				'type'=>'edit',
				'slug'=>'slug',
				'data'=>$result_data,
				'table'=>$this->TABLE,
				'input'=>array(
					'hidden'=>array('role'=>6,'shop_id'=>$shop_id,'user_id'=>$arr_shop->user_id,'userprofile_id'=>$arr_userprofile->id),
					'text' => array('username','email','firstname','lastname','phone','shop_title','slug'),
					'textarea' => array('shop_description'),
					'image'=>array('shop_picture'=>asset($result_data->shop_picture)),
					'select'=>array(
						'active'=>array('1'=>'Active','0'=>'Pending'),
					),
					'password'=>array('password','password_confirmation'),
					)
				);				
		
		
		$data['form']=$form;
		//$data['process']=$process;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/shop/insert',$data);
		
	}

	public function post_edit(){
			$arr_post = \Input::all();	
			$arr_input= $arr_post['data'];
			$rules = array(
			    'username' => 'required|alpha_dash',
			    'firstname' =>'required|alpha_dash',
			    'lastname'=>'required',
			    'email' => 'required|email',
			    'password' => 'min:6|confirmed',
	  			'shop_picture' => 'max:2000|image',		
	  			'shop_title' => 'required',		
			);

			$message =  array();
			$validator = \Validator::make($arr_input,$rules,$message);
			if($validator->fails()) {
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
				exit;
			}
			
			
			//check password
			if($arr_input['password']!=''){
				$arr_user['password'] =\Hash::make($arr_input['password']);				
			}
			//Debug::pre($arr_input);
			//check image
			if(isset($arr_input['shop_picture'])){
				echo '1';
				@SubmitImage::delete($arr_post['old']['file'][$value]);
				$var_image_url = SubmitImage::insert($arr_input['shop_picture']);

				// insert picture to array
				$arr_user['picture'] = $var_image_url;
				$arr_shop['picture']=$var_image_url;
			}
			
			//check slug
			if(isset($arr_post['slug'])&&$arr_post['data'][$arr_post['slug']]!=''){
				if(isset($arr_post['old']['slug'])&&$arr_post['old']['slug']!=''){
					// ถ้า slug เก่า เหมือน slug ใหม่ จะไม่มีการแก้ไข
					if($arr_post['old']['slug']==$arr_post['data'][$arr_post['slug']])
					{
						unset($arr_post['data'][$arr_post['slug']]);
					}else{
						$arr_shop['slug'] = \Thaislug::url($arr_input['shop_title'],'shop');
					}
				}
			}	
		/*-------------------- user update --------------------*/			

			$arr_user['username']=$arr_input['username'];
			$arr_user['email']=$arr_input['email'];
			User::where('id',$arr_input['user_id'])->update($arr_user);

		/*-------------------- user update --------------------*/			


		/*-------------------- Userprofile update --------------------*/			
			$arr_user_profile = array(
				'firstname'=>$arr_input['firstname'],
				'lastname'=>$arr_input['lastname'],
				'phone'=>$arr_input['phone'],
			);  
			Userprofile::where('user_id',$arr_input['user_id'])->update($arr_user_profile); 
		/*-------------------- Userprofile update --------------------*/			


		/*-------------------- shop update --------------------*/	
			$arr_shop['title']=$arr_input['shop_title'];
			$arr_shop['active']=$arr_input['active'];
			$arr_shop['description']=$arr_input['shop_description'];

			Shop::where('id',$arr_input['shop_id'])->update($arr_shop); 

			
		/*-------------------- shop update --------------------*/			

			return redirect($this->CONTROLLER.'/profile/'.$arr_input['shop_id']);

		}

	public function get_insert(){

		$title= 'Insert';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);		
			$form =array(
				'post'=>$this->CONTROLLER.'/insert',
				'files'=>'1',
				//'type'=>'edit',
				'table'=>$this->TABLE,
				'input'=>array(
					'hidden'=>array('role'=>6),
					'text' => array('username','email','firstname','lastname','phone','shop_title'),
					'textarea' => array('shop_description'),
					'image'=>array('shop_picture'=>'picture'),
					'select'=>array(
						'active'=>array('1'=>'Active','0'=>'Pending'),
					),

					'password'=>array('password','password_confirmation'),
					)
				);				
		
		
		$data['form']=$form;
		//$data['process']=$process;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/shop/insert',$data);
		
	}

	public function post_insert(){
		$arr_post = \Input::all();	
		$arr_input= $arr_post['data'];
		$rules = array(
		    'username' => 'required|unique:users|alpha_dash',
		    'firstname' =>'required|alpha_dash',
		    'lastname'=>'required',
		    'email' => 'required|email|unique:users',
		    'password' => 'required|min:6|confirmed',
  			'shop_picture' => 'required|max:2000|image',		
  			'shop_title' => 'required',		
		);

		$message =  array();
		$validator = \Validator::make($arr_input,$rules,$message);
		if($validator->fails()) {
			return \Redirect::back()->withInput($arr_post)->withErrors($validator);
			exit;
		}

		//image
		if(isset($arr_input['shop_picture'])){

				 $var_image_url = SubmitImage::insert($arr_input['shop_picture']);
				$arr_input['shop_picture']=$var_image_url;
		}

		/*-------------------- user update --------------------*/			
		$arr_user = array(
			'username'=>$arr_input['username'],			
			'email'=>$arr_input['email'],
			'picture'=>$arr_input['shop_picture'],
			'password'=>\Hash::make($arr_input['password']),	
		);
		$obj_user = User::create($arr_user);
		$user_id= $obj_user->id;
		/*-------------------- user update --------------------*/			


		/*-------------------- Userprofile update --------------------*/					
		$arr_user_profile = array(
			'user_id'=>$user_id,
			'firstname'=>$arr_input['firstname'],
			'lastname'=>$arr_input['lastname'],
			'phone'=>$arr_input['phone'],
			
		);
		Userprofile::create($arr_user_profile);
		/*-------------------- Userprofile update --------------------*/					


		/*-------------------- Shop update --------------------*/					
		$obj_shop = Shop::create(array(
			'user_id'=>$user_id,
			'title'=>$arr_input['shop_title'],
			'active'=>$arr_input['active'],
			'picture'=>$arr_input['shop_picture'],
			'slug'=>\Thaislug::url($arr_input['shop_title'],'shop'),
			'description'=>$arr_input['shop_description'],
			));
		/*-------------------- Shop update --------------------*/					

		return redirect($this->CONTROLLER.'/profile/'.$obj_shop->id);

	}	
}
?>