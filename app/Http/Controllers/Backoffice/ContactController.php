<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Contact;
use App\Models\Subject;
use Debug;

 
class ContactController extends Controller {

	public $TITLE = 'ข้อความติดต่อ';
	public $CONTROLLER = 'contact';
	public $TABLE = 'contact';

	public function __construct()
	{
		$this->middleware('backoffice');
	}

	
	
	public function get_view($id){
		$result_data = Contact::find($id);
		if($result_data->read==0){
			Contact::where('id',$id)->update(array('read'=>1));
		}

		/*-------head ---------*/		
			$title= $this->TITLE.' : # '.$id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER),'title'=>$this->TITLE),			
				array('url'=>'#','title'=>$title)				
				);	
			
			$button =array(				
						
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER,
					'title' => 'ย้อนกลับ',
					),
			);
			
		/*------profile--------*/
			$profile = array(
				'title' =>$result_data->name,				
				'label' =>array(
							
					'อีเมล์' =>'email', 	
					'โทรศัพท์' =>'phone',				
					'รายละเอียด' =>'message',				
					'เวลา' =>array('value'=>\Formate::board_date($result_data->created_at)),				
				 ),
				'data' =>$result_data ,
				);
			
		
		$data['profile']=$profile;
		$data['title']=$title;

		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_profile',$data);
	}
	public function get_index(){
		$result_data = Contact::orderby('created_at','desc')->paginate(30);
		/*-------head ---------*/
			$title= 'รายการติดต่อ';
			$table_name= $this->TABLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)				
				);								
		/*-------table ---------*/
			$table = array( 
				'thead' => array('ชื่อ'=>'name','อีเมล์'=>'email','โทรศัพท์'=>'phone'),
				'tbody' =>$result_data	,
				'tr_active'=>array(
					'field'=>'read',
					'class'=>'info',
					'active'=>'0'
					),
				'tools'=>array(
					
					array(

						'icon'=>'fa fa-file-text-o',
						'title'=>'view',
						'href'=>$this->CONTROLLER.'/view',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						//'post'=>$this->CONTROLLER.'/cagdel',
					   'table'=>$table_name,				
					)
				);
		$data['title']=$title;
	
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}
	
}
?>