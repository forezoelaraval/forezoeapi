<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Payment;

use Debug;

use Illuminate\Support\Str;

class TransactionmanagementController extends Controller {

	public $TITLE = 'Transaction';
	public $CONTROLLER = 'transactionmanagement';
	public $TABLE = 'payment';
	public $NAV = 'nav-transaction';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function get_update($payment_id){
		$obj_payment = Payment::find($payment_id);
		$obj_payment->view = 0;
		$obj_payment->update();
		return \Redirect::back();
	}
	public function get_home(){
		$result_data = Payment::queryview(100);
		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-plus',
					'href' => $this->CONTROLLER.'/insert',
					'title' => 'เพิ่ม',
					),				
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('username'=>'username','corn'=>'coin','remake'=>'remark','Created at'=>'created_at'),
				'tbody' =>$result_data,
				
				'tools'=>array(
					
					),
				
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}

	
	
}
?>