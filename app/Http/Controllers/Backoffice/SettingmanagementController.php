<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Setting;

use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class SettingmanagementController extends Controller {

	public $TITLE = 'Setting';
	public $CONTROLLER = 'settingmanagement';
	public $TABLE = 'setting';
	public $NAV = 'nav-setting';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_success(){
		$arr_post = \Input::all();
		if(isset($arr_post['rule'])){
			$validator = \Validator::make($arr_post['data'],$arr_post['rule']);
			if($validator->fails()){
				//  ----checkbox
					if(isset($arr_post['checkbox'])){
						unset($arr_post['checkbox']);
					}
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
				exit;
			}
		}
		foreach ($arr_post['data'] as $key_data => $value_data) {
			Setting::where('meta_key',$key_data)->update(array('meta_value'=>$value_data));
		}
		Session::flash('message', 'Success');
		return \Redirect::back();
	}

	public function get_download(){
		$obj_data = new \stdClass;
		$obj_data->download_title_1 =  Setting::where('meta_key','download_title_1')->first()->meta_value;
		$obj_data->download_url_1 =  Setting::where('meta_key','download_url_1')->first()->meta_value;
		$obj_data->download_title_2 =  Setting::where('meta_key','download_title_2')->first()->meta_value;
		$obj_data->download_url_2 =  Setting::where('meta_key','download_url_2')->first()->meta_value;
		$obj_data->download_title_3 =  Setting::where('meta_key','download_title_3')->first()->meta_value;
		$obj_data->download_url_3 =  Setting::where('meta_key','download_url_3')->first()->meta_value;
		$obj_data->download_title_4 =  Setting::where('meta_key','download_title_4')->first()->meta_value;
		$obj_data->download_url_4 =  Setting::where('meta_key','download_url_4')->first()->meta_value;
		$obj_data->download_title_5 =  Setting::where('meta_key','download_title_5')->first()->meta_value;
		$obj_data->download_url_5 =  Setting::where('meta_key','download_url_5')->first()->meta_value;
		/*-------head ---------*/
			$title= $this->TITLE.' : Download';
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			$form =array(
				'post'=>$this->CONTROLLER.'/success',
				'success'=>$this->CONTROLLER.'/download',
				'files'=>'1',
				'rule'=>array(		    		
					//'footer_address' =>'required',
				),
				'type'=>'edit',						
				'data'=>$obj_data,
				'table'=>$this->TABLE,
						//'unset'=>array(''),
				'input'=>array(
					'text' => array('download_title_1','download_url_1','download_title_2','download_url_2','download_title_3','download_url_3','download_title_4','download_url_4','download_title_5','download_url_5'),
					)
				);
				
		$data['form']=$form;
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/setting/footer',$data);
	}
	public function get_footer(){
		$obj_data = new \stdClass;
		$obj_data->footer_address =  Setting::where('meta_key','footer_address')->first()->meta_value;

		$obj_data->footer_social_facebook =  Setting::where('meta_key','footer_social_facebook')->first()->meta_value;
		$obj_data->footer_social_line =  Setting::where('meta_key','footer_social_line')->first()->meta_value;
		$obj_data->footer_social_ig =  Setting::where('meta_key','footer_social_ig')->first()->meta_value;
		
		/*-------head ---------*/
			$title= $this->TITLE.' : Footer';
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			$form =array(
				'post'=>$this->CONTROLLER.'/success',
				'success'=>$this->CONTROLLER.'/success',
				'files'=>'1',
				'rule'=>array(		    		
					'footer_address' =>'required',
			
				),
				'type'=>'edit',						
				'data'=>$obj_data,
				'table'=>$this->TABLE,
						//'unset'=>array(''),
				'input'=>array(
					
					'textarea' => array('footer_address'),
					'text' => array('footer_social_facebook','footer_social_line','footer_social_ig'),
					)
				);
				
		$data['form']=$form;
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/setting/footer',$data);
	}
	public function get_success(){
		$obj_data = new \stdClass;
		$obj_data->vat =  Setting::where('meta_key','vat')->first()->meta_value;

		$obj_data->success_title =  Setting::where('meta_key','success_title')->first()->meta_value;
		$obj_data->success_description =  Setting::where('meta_key','success_description')->first()->meta_value;
		$obj_data->success_payment =  Setting::where('meta_key','success_payment')->first()->meta_value;
		
		/*-------head ---------*/
			$title= $this->TITLE.' : Success';
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			$form =array(
				'post'=>$this->CONTROLLER.'/success',
				'success'=>$this->CONTROLLER.'/success',
				'files'=>'1',
				'rule'=>array(		    		
					'vat' =>'required',
					'success_title' =>'required',
					'success_description' =>'required',
					'success_payment' =>'required',
				),
				'type'=>'edit',						
				'data'=>$obj_data,
				'table'=>$this->TABLE,
						//'unset'=>array(''),
				'input'=>array(
					'text' => array('vat'),
					'textarea' => array('success_title','success_description','success_payment'),
					)
				);
				
		$data['form']=$form;
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/setting/success',$data);
	}
	public function get_shipping(){
		$result_data =  Setting::where('meta_key','shipping')->first();
		/*-------head ---------*/
			$title= $this->TITLE.' : Shipping';
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			$form =array(
					//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/shipping',
				'files'=>'1',
				'rule'=>array(		    		
					'meta_value' =>'required',
							),
				'type'=>'edit',						
				'data'=>$result_data,
				'table'=>$this->TABLE,
						//'unset'=>array(''),
				'input'=>array(
					'textarea' => array('meta_value'),
							)
						);
				
		$data['form']=$form;
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/setting/shipping',$data);
	}

	
}
?>