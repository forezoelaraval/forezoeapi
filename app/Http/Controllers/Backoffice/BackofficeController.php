<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Product;
use App\Models\Payment;
use App\Models\User;
use Session;
use Mail;
use SubmitImage;
use Formate;
use Debug;
use DB;
class BackofficeController extends Controller {
	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function post_submit(){				
		
		$arr_post = \Input::all();		
		
		if(isset($arr_post['rule'])){
			$validator = \Validator::make($arr_post['data'],$arr_post['rule']);
			if($validator->fails()){
				//  ----checkbox
					if(isset($arr_post['checkbox'])){
						unset($arr_post['checkbox']);
					}
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
				exit;
			}
		}
		if(isset($arr_post['unset'])){
			foreach ($arr_post['unset'] as  $value) {
				if(isset($arr_post['data'][$value])){
					unset($arr_post['data'][$value]);
				}
				
			}
		}
	
		//  ----checkbox
		if(isset($arr_post['checkbox'])){
			foreach ($arr_post['checkbox'] as  $value) {
				if(!isset($arr_post['data'][$value])){
					$arr_post['data'][$value]=0;
				}
			}
		}
		//  ----file insert
		if(isset($arr_post['file'])){
			foreach ($arr_post['file'] as  $value) {
				if(isset($arr_post['data'][$value])&&is_object($arr_post['data'][$value])){		
					$file =$arr_post['data'][$value];	
					if (\File::exists($file)) {	
						$arr_post['data'][$value]=SubmitImage::insert($file);
					}						
				}else{  //หากไม่มีUpdate file  ให้ลบ input::file ทิ้ง ไม่ให้ update ใน database
					unset($arr_post['data'][$value]);
				}				
			}
		}				

	//---  insert & edit :
		// Edit case  
		if(isset($arr_post['type'])&&$arr_post['type']=='edit'){//edit	
			
			//--- file has edit :  old file is delete
			if(isset($arr_post['file'])){
				foreach ($arr_post['file'] as  $value) {
					if(isset($arr_post['data'][$value]) && isset($arr_post['old']['file']) ){ // หากมีkey ใหม่ ให้ delete old key
						SubmitImage::delete($arr_post['old']['file'][$value]);
					}
				}
			}

			// check slug update			
			if(isset($arr_post['slug'])&&$arr_post['data'][$arr_post['slug']]!=''){
				if(isset($arr_post['old']['slug'])&&$arr_post['old']['slug']!=''){
					// ถ้า slug เก่า เหมือน slug ใหม่ จะไม่มีการแก้ไข
					if($arr_post['old']['slug']==$arr_post['data'][$arr_post['slug']])
					{
						unset($arr_post['data'][$arr_post['slug']]);
					}else{
						$arr_post['data']['slug']=\Thaislug::url($arr_post['data'][$arr_post['slug']],$arr_post['table']);
					}
				}
			}

			$arr_post['data']['updated_at'] = new \DateTime;		
							
			$boolean = DB::table($arr_post['table'])->where('id', $arr_post['id'])->update($arr_post['data']);
			if($boolean){ 
					Session::flash('message', 'Success');
					return \Redirect::to($arr_post['success'].'/'.$arr_post['id']);
				}else{
					Session::flash('error', 'Error Please try again');
					return \Redirect::back();
				}
		}else{ //insert

		if(isset($arr_post['slug'])&&$arr_post['data'][$arr_post['slug']]!=''){ //หากมี ตัวแปร slug และ input ที่เชื่อมกับ slug ต้องไม่เป็นค่าว่าง
			 $arr_post['data']['slug']=\Thaislug::url($arr_post['data'][$arr_post['slug']],$arr_post['table']);
		}

			$arr_post['data']['created_at']=new \DateTime;
			$arr_post['data']['updated_at']=new \DateTime;
			
			$result_id = DB::table($arr_post['table'])->insertGetId($arr_post['data']);
			if($result_id){ 
					Session::flash('message', 'Success');
					return \Redirect::to($arr_post['success'].'/'.$result_id);
				}else{
					Session::flash('error', 'Error Please try again');
					return \Redirect::back();
				}
		}
	}
	public function post_del(){
		$arr_post = \Input::all();

		
		SubmitImage::deleteimageIntable($arr_post['id'],$arr_post['table']);
		$boolean = DB::table($arr_post['table'])->where('id', $arr_post['id'])->delete();
		if($boolean){ 
			Session::flash('message', $arr_post['table'].' # '.$arr_post['id'].' has deleted!.');
		}else{
			Session::flash('error', 'Error Please try again');
		}
	
		return \Redirect::back();
	}
	public function get_index()
	{
		
		$int_user = User::queryconutrole('user');
		$int_idol = User::queryconutrole('idol');
		$int_supplier = User::queryconutrole('supplier');
		$int_product = Product::queryconut();
		$arr_payment = Payment::queryview(10);
		
		$data['title']='Dashboard';
		$data['int_user']=$int_user;
		$data['int_idol']=$int_idol;
		$data['arr_payment']=$arr_payment;
		$data['int_supplier']=$int_supplier;
		$data['int_product']=$int_product;
		return view('backoffice/dashborad',$data);
	}
	public function post_login(){
		$arr_post = \Input::all();
		$rules = array(
			'email' =>'required|login:'.$arr_post['email'].','.$arr_post['password'],
			'password' =>'required'
			);
		$message =  array('login' => 'Invalid email or password, please try' );
		\Validator::extend('login', function($attribute, $value, $parameters)
		{
			$arr_user = User::select('password')->where('email',$parameters[0])->first();
			if($arr_user){					
				if(\Hash::check( $parameters[1],$arr_user->password ))
				return true;	
			}
			return false;
		});

		$validator = \Validator::make($arr_post,$rules,$message);
		if($validator->fails()){
			return \Redirect::back()->withInput($arr_post)->withErrors($validator);
		} else {
			$userdata = array(
       			'email'     => \Input::get('email'),
       			'password'  => \Input::get('password')
 			 );
			if (\Auth::attempt($userdata)) {
				return \Redirect::to('backoffice/index');
			}				
		}
	}
	public function post_submitedit(){
	$arr_post = \Input::all();		

		if($arr_post['old']['email']==$arr_post['data']['email']){
			unset($arr_post['rule']['email']);
			unset($arr_post['data']['email']);
		}
		if($arr_post['data']['password']==''&&$arr_post['data']['password_confirmation']==''){
			unset($arr_post['rule']['password']);
			unset($arr_post['data']['password']);			
		}

		if(isset($arr_post['rule'])){
			$validator = \Validator::make($arr_post['data'],$arr_post['rule']);
			if($validator->fails()){
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
			}
		}
		if(isset($arr_post['unset'])){
			foreach ($arr_post['unset'] as  $value) {
				if(isset($arr_post['data'][$value])){
					unset($arr_post['data'][$value]);
				}				
			}
		}
		if(isset($arr_post['data']['password'])){
			$password = $arr_post['data']['password'];
			$arr_post['data']['password'] = \Hash::make($password);
		}

		User::where('id',$arr_post['id'])->update($arr_post['data']);
		\Session::flash('message','Success');
		return \Redirect::back();
	}
	public function get_adminedit(){
		$user_id =  \Auth::user()->id;
		$arr_users = User::find($user_id);
		$title= 'Admin ';
			$sidebar = array(
				'sidebar'=>'nav-user'							
			);
			$breadcrumb = array(
				array('url'=>'#','title'=>'Admin'),
			
				);
			
			$form =array(
				'post'=>'backoffice/submitedit',
				'success'=>'backoffice/adminedit',
				'files'=>'1',
				'rule'=>array(
		    		'email' => 'required|email|unique:users',
		    		'password' => 'required|min:6|confirmed',
					'name' =>'required',
					),
				'type'=>'edit',
				'data'=>$arr_users,	
				'table'=>'users',
				'unset'=>array('password_confirmation'),
				'old'=>array('email'=>$arr_users->email),
				'input'=>array(
				'text' => array('name'),
					'email'=>array('email'),
					'password'=>array('password','password_confirmation'),										
					)
				);
		$data['form']=$form;
	
		$data['title']=$title;

		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_form',$data);
	}
	public function get_login(){
		
		return view('backoffice/login');
	}
	public function get_logout(){
		\Auth::logout();
		return view('backoffice/login');
	}
}
?>