<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Banner;

use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class BannerController extends Controller {

	public $TITLE = 'Banner';
	public $CONTROLLER = 'banner';
	public $TABLE = 'banner';
	public $NAV = 'nav-banner';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	
	public function get_home(){
		$result_data = Banner::get();
		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-plus',
					'href' => $this->CONTROLLER.'/insert',
					'title' => 'เพิ่ม',
					),				
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('Picture'=>'picture','Title'=>'title','Sub title'=>'subtitle'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>$this->CONTROLLER.'/edit',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						//'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}
public function get_edit($id){
		$result_data =\DB::table($this->TABLE)->where('id',$id)->first();
		
		$title= 'Edit';
					$sidebar = array(
						'sidebar'=>''							
					);
					$breadcrumb = array(
						array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
						array('url'=>'#','title'=>'Edit')				
						);
					$button =array(							
						array(
							'icon' => 'fa fa-reply',
							'href' => $this->CONTROLLER.'/home',
							'title' => 'ย้อนกลับ',
							),
						);
					
					$form =array(
						//'post'=>$this->CONTROLLER.'/???',
						'success'=>$this->CONTROLLER.'/home',
						'files'=>'1',
						'rule'=>array(		    		
						//	'title' =>'required',
						//	'subtitle' =>'required',
							'picture'=>'max:1000|image',
							),
						'type'=>'edit',						
						'data'=>$result_data,
						'table'=>$this->TABLE,
						//'unset'=>array(''),
						'input'=>array(
							'textarea' => array('title','subtitle'),
							'image'=> array('picture'=>asset($result_data->picture))					
							)
						);
				
				$data['form']=$form;
				$data['sidebar']=$sidebar;
				$data['title']=$title;
				$data['button']=$button;
				$data['breadcrumb']=$breadcrumb;
				return view('backoffice/category/form',$data);
		
	}
	public function get_insert(){
		$title= 'Insert';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);
			
			$form =array(
				//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/home',
				'files'=>'1',
				'rule'=>array(
					//'title' =>'required',
					//'subtitle' =>'required',
					'picture'=>'required|max:1000|image',
					),
				//'type'=>'edit',
				'table'=>$this->TABLE,
				'input'=>array(
					'textarea' => array('title','subtitle'),
					'image'=> array('picture'=>'')
					)
				);
		
		$data['form']=$form;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_form',$data);
		
	}	
	
}
?>