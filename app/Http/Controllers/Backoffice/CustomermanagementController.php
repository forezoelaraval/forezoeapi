<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\User;
use App\Models\Userprofile;
use App\Models\Order;


use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class CustomermanagementController extends Controller {

	public $TITLE = 'ลูกค้า';
	public $CONTROLLER = 'customermanagement';
	public $TABLE = 'user';
	public $NAV = 'nav-user';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_delete(){
		Userprofile::where('user_id',$_POST['id'])->delete();
		User::where('id',$_POST['id'])->delete();
		Session::flash('message', $_POST['table'].' # '.$_POST['id'].' has deleted!.');
		return \Redirect::back();
	}

	
	public function get_list($user_id){
		$result_data = Order::querycustomer($user_id);
		/*-------head ---------*/
			$title= 'ประวัติการทำรายการ : # '.$user_id;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
			);
		/*-------table ---------*/
		
		$data['result_data']=$result_data;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/customer/order_table',$data);
	}

	public function get_home(){
		$str_search ='';
		if(isset($_GET['search'])){
			$str_search =  $_GET['search'];		
		}
		$result_data = User::queryallcustomer(15,$str_search);
		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
							
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('ชื่อ'=>'name','อีเมล์'=>'email','โทรศัพท์'=>'phone'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-file-text-o',
						'title'=>'Profile',
						'href'=>$this->CONTROLLER.'/profile',
						'class'=>'btn btn-primary',
						),
					
					array(
						'icon'=>'fa fa-list',
						'title'=>'History',
						'href'=>$this->CONTROLLER.'/list',
						'class'=>'btn btn-primary',
						),
					array(
						'icon'=>'fa fa-heart',
						'title'=>'Wishlist',
						'href'=>'wishlistmanagement/customer',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['str_search']=$str_search;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/customer/table',$data);
	}


	
	public function get_profile($id){
		$result_data =User::querycustomer($id);
		$arr_profile = json_decode($result_data->profile,true);

		 $result_data->company = $arr_profile['companyname'] ;
		 $result_data->address = $arr_profile['address'] ;
		 $result_data->taxpayers = $arr_profile['taxpayers'] ;
		 $result_data->province = $arr_profile['province'] ;
		 $result_data->postcode = $arr_profile['postcode'] ;
		 $result_data->email = $arr_profile['email'] ;
		

		/*-------head ---------*/		
			$title= $this->TITLE.' : # '.$id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-1'							
			);
			$button =array(				
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
			);
		/*------profile--------*/
		
			$profile = array(
				'title' =>$result_data->name.' '.$arr_profile['lastname'],
				'subtitle' =>$result_data->phone,
				
				//'picture'=>SubmitImage::get($result_data->picture),
				'label' =>array(
					'อีเมล'=>'email',
					'บริษัท'=>'company',
					'ที่อยู่'=>'address',
					'จังหวัด'=>'province',
					'รหัสไปรณีย์'=>'postcode',
					'รหัสผู้เสียภาษี'=>'taxpayers',
					//'หมายเหตุ'=>array('value'=>'sdf'),
	
				 ),
				
				'data' =>$result_data ,
				);
			
		
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_profile',$data);
	}
}
?>