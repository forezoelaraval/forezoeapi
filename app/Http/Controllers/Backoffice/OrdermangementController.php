<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;

use App\Models\Setting;
use App\Models\Order;
use App\Models\Orderstatus;

use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class OrdermangementController extends Controller {

	public $TITLE = 'รายการสั่งซื้อ';
	public $CONTROLLER = 'order';
	public $TABLE = 'order ';
	public $NAV = 'nav-order';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_changestatus(){
		date_default_timezone_set("Asia/Bangkok");
		$_POST['order_status']['status']=$_POST['status'];
		$arr_orderstatus['order_id']=$_POST['id'];
		$arr_orderstatus['description']=json_encode($_POST['order_status']);
		Orderstatus::create($arr_orderstatus);
		Order::where('id',$_POST['id'])->update(array('status'=>$_POST['status']));
		Session::flash('message', 'เปลี่ยนสถานะคำสั่งซื้อเป็น '.$_POST['status'].' เรียบร้อยแล้ว!!');
		return \Redirect::back();
	}
	public function get_history($order_id){
		$result_data = Orderstatus::getorderid($order_id);
		/*-------head ---------*/
			$title= 'ประวัติการแก้ไข : Order ID : # '.sprintf("%07d",$order_id);
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(	
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),			
				);
		$data['result_data']=$result_data;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/order/history',$data);	}
	public function get_home($status=''){
		$result_data = Order::queryorder($status);
		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'order/home','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
							
				);
		/*-------table ---------*/
		
		$data['result_data']=$result_data;
		$data['title']=$title;
		$data['status']=$status;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/order/table',$data);
	}
	
	public function get_view($id){
		
		$result_data =Order::where('id',$id)->first();
		if($result_data->read==0){
			Order::where('id',$id)->update(array('read'=>1));
		}
		/*-------head ---------*/	
			$title= 'Order ID : # '.sprintf("%07d",$id);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-1'							
			);
			$button =array(				
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
			);


		$arr_profile = json_decode($result_data->profile,true);
		$arr_vat = json_decode($result_data->vat,true);
		$arr_cart_list = json_decode($result_data->cart_list,true);
		$data['arr_profile']=$arr_profile;
		$data['arr_vat']=$arr_vat;
		$data['arr_cart_list']=$arr_cart_list;
		$data['result_data']=$result_data;
		$data['order_notes']= $data['result_data']->order_notes;

		$data['vat']=$vat = Setting::get_value('vat');
		$data['total']=0;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/order/view',$data);
	}
}
?>