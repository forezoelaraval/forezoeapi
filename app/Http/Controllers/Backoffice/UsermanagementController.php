<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\User;
use App\Models\Userprofile;
use App\Models\Usergift;
use App\Models\Payment;


use Debug;
use Session;
use SubmitImage;

class UsermanagementController extends Controller {

	public $TITLE = 'User';
	public $CONTROLLER = 'usermanagement';
	public $TABLE = 'user';
	public $NAV = 'nav-user';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_delete(){
		Userprofile::where('user_id',$_POST['id'])->delete();
		User::where('id',$_POST['id'])->delete();
		Session::flash('message', $_POST['table'].' # '.$_POST['id'].' has deleted!.');
		return \Redirect::back();
	}

	
	public function get_gift($user_id){
		$result_data = Usergift::queryhistory($user_id);
		/*-------head ---------*/
			$title= $this->TITLE.' : Gift History : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'Gift History')
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(	
			array(
				'icon'=>'fa fa-cube',
				'title'=>'gift',
				'href'=>$this->CONTROLLER.'/gift/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon'=>'fa fa-credit-card',
				'title'=>'Payment History',
				'href'=>$this->CONTROLLER.'/payment/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon'=>'fa fa-file-text-o',
				'title'=>'Profile',
				'href'=>$this->CONTROLLER.'/profile/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon' => 'fa fa-reply',
				'href' => $this->CONTROLLER.'/home',
				'title' => ' ย้อนกลับ',
					),
				
			);
			$table = array( 
				'thead' => array('Product'=>'product_title','Username get'=>'username_get','Remark'=>'remark','created_at'=>'created_at'),
				'tbody' =>$result_data
				);
		/*-------table ---------*/
		$data['table']=$table;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}

	public function get_payment($user_id){

		$result_data = Payment::where('user_id',$user_id)->paginate(30);

		/*-------head ---------*/
			$title= $this->TITLE.' : Payment History : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'Payment History')
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(	
			array(
				'icon'=>'fa fa-cube',
				'title'=>'gift',
				'href'=>$this->CONTROLLER.'/gift/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon'=>'fa fa-credit-card',
				'title'=>'Payment History',
				'href'=>$this->CONTROLLER.'/payment/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon'=>'fa fa-file-text-o',
				'title'=>'Profile',
				'href'=>$this->CONTROLLER.'/profile/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon' => 'fa fa-reply',
				'href' => $this->CONTROLLER.'/home',
				'title' => ' ย้อนกลับ',
					),
				
			);
			$table = array( 
				'thead' => array('coin'=>'coin','Remark'=>'remark','created_at'=>'created_at'),
				'tbody' =>$result_data
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}

	public function get_home(){
		$str_search ='';
		$str_role ='';
		if(isset($_GET['search'])){
			$str_search =  $_GET['search'];		
		}
		if(isset($_GET['str_role'])){
			$str_search =  $_GET['str_role'];		
		}

		$result_data = User::queryuser(30,'user',$str_role,$str_search);

		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
							
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('Username'=>'username','Email'=>'email','Role'=>'users_role','Coin'=>'coin','created_at'=>'created_at'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-file-text-o',
						'title'=>'Profile',
						'href'=>$this->CONTROLLER.'/profile',
						'class'=>'btn btn-primary',
						),
					
					array(
						'icon'=>'fa fa-credit-card',
						'title'=>'Payment History',
						'href'=>$this->CONTROLLER.'/payment',
						'class'=>'btn btn-primary',
						),
					array(
						'icon'=>'fa fa-cube',
						'title'=>'gift',
						'href'=>$this->CONTROLLER.'/gift',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['str_search']=$str_search;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/user/table',$data);
	}


	
	public function get_profile($user_id){
		$result_data =User::queryprofile($user_id);
		$result_data->gender = \Myfunction::returngender($result_data->gender);
		$result_data->birthdate = \Formate::format_display($result_data->birthdate);
		//Debug::pre($result_data);
		//$arr_profile = json_decode($result_data->profile,true);

		

		/*-------head ---------*/		
			$title= $this->TITLE.' '.$result_data->username.' : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-1'							
			);
			$button =array(	
			array(
				'icon'=>'fa fa-cube',
				'title'=>'gift',
				'href'=>$this->CONTROLLER.'/gift/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon'=>'fa fa-credit-card',
				'title'=>'Payment History',
				'href'=>$this->CONTROLLER.'/payment/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon'=>'fa fa-file-text-o',
				'title'=>'Profile',
				'href'=>$this->CONTROLLER.'/profile/'.$user_id,
				'class'=>'btn btn-primary',
				),
			array(
				'icon' => 'fa fa-reply',
				'href' => $this->CONTROLLER.'/home',
				'title' => ' ย้อนกลับ',
					),
				
			);
		/*------profile--------*/
		
			$profile = array(
				'title' =>$result_data->firstname.' '.$result_data->lastname.' ('.$result_data->users_role.')',
				'subtitle' =>'<i class="fa fa-mobile" aria-hidden="true"></i> : '.$result_data->phone,
				
				'picture'=>$result_data->picture,
				'label' =>array(
					'อีเมล'=>'email',
					'เพศ'=>'gender',
					'เหรียญ'=>'coin',
					'วันเกิด'=>'birthdate',
				
					//'หมายเหตุ'=>array('value'=>'sdf'),
	
				 ),
				
				'data' =>$result_data ,
				);
			
		
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_profile',$data);
	}
}
?>