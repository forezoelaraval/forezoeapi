<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\User;
use App\Models\Userprofile;
use App\Models\Usergift;
use App\Models\Payment;
use App\Models\Userfollow;
use App\Models\Userlike;
use App\Models\Userrole;


use Debug;
use Session;
use SubmitImage;

class IdolmanagementController extends Controller {

	public $TITLE = 'Idol';
	public $CONTROLLER = 'idolmanagement';
	public $TABLE = 'users';
	public $NAV = 'nav-user';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}
	public function post_delete(){
		Userprofile::where('user_id',$_POST['id'])->delete();
		User::where('id',$_POST['id'])->delete();
		Session::flash('message', $_POST['table'].' # '.$_POST['id'].' has deleted!.');
		return \Redirect::back();
	}

	
	public function get_gift($user_id){
		$result_data = Usergift::queryusergivehistory($user_id);
		/*-------head ---------*/
			$title= $this->TITLE.' : Gift History : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'Gift History')
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon'=>'fa fa-thumbs-o-up',
					'title'=>'user like',
					'href'=>$this->CONTROLLER.'/like/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-heart',
					'title'=>'gift',
					'href'=>$this->CONTROLLER.'/gift/'.$user_id,
					'class'=>'btn btn-primary',
					),				
				array(
					'icon'=>'fa fa-users',
					'title'=>'user follow',
					'href'=>$this->CONTROLLER.'/follow/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-file-text-o',
					'title'=>'Profile',
					'href'=>$this->CONTROLLER.'/profile/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => ' ย้อนกลับ',
					),
			);
			$table = array( 
				'thead' => array('Product'=>'product_title','Username give'=>'username_give','Remark'=>'remark','created_at'=>'created_at'),
				'tbody' =>$result_data
				);
		/*-------table ---------*/
		$data['table']=$table;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}
	public function get_like($user_id){

		$result_data = Userlike::getuser($user_id);

		/*-------head ---------*/
			$title= $this->TITLE.' : User Like : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'User Like')
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				
				array(
					'icon'=>'fa fa-thumbs-o-up',
					'title'=>'user like',
					'href'=>$this->CONTROLLER.'/like/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-heart',
					'title'=>'gift',
					'href'=>$this->CONTROLLER.'/gift/'.$user_id,
					'class'=>'btn btn-primary',
					),				
				array(
					'icon'=>'fa fa-users',
					'title'=>'user follow',
					'href'=>$this->CONTROLLER.'/follow/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-file-text-o',
					'title'=>'Profile',
					'href'=>$this->CONTROLLER.'/profile/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => ' ย้อนกลับ',
					),
				);
			$table = array( 
				'thead' => array('Username'=>'username','created_at'=>'created_at'),
				'tbody' =>$result_data
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}
	public function get_follow($user_id){

		$result_data = Userfollow::getuser($user_id);

		/*-------head ---------*/
			$title= $this->TITLE.' : User Follow : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'User Follow')
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon'=>'fa fa-thumbs-o-up',
					'title'=>'user like',
					'href'=>$this->CONTROLLER.'/like/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-heart',
					'title'=>'gift',
					'href'=>$this->CONTROLLER.'/gift/'.$user_id,
					'class'=>'btn btn-primary',
					),				
				array(
					'icon'=>'fa fa-users',
					'title'=>'user follow',
					'href'=>$this->CONTROLLER.'/follow/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-file-text-o',
					'title'=>'Profile',
					'href'=>$this->CONTROLLER.'/profile/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => ' ย้อนกลับ',
					),	
				);
			$table = array( 
				'thead' => array('Username'=>'username','created_at'=>'created_at'),
				'tbody' =>$result_data
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}

	public function get_home(){
		$str_search ='';
		$str_role ='';
		if(isset($_GET['search'])){
			$str_search =  $_GET['search'];		
		}
		if(isset($_GET['str_role'])){
			$str_search =  $_GET['str_role'];		
		}

		$result_data = User::queryuser(30,'idol',$str_role,$str_search);

		/*-------head ---------*/
			$title= $this->TITLE;
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
							
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('Username'=>'username','Email'=>'email','Role'=>'users_role','score'=>'score','created_at'=>'created_at'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-file-text-o',
						'title'=>'Profile',
						'href'=>$this->CONTROLLER.'/profile',
						'class'=>'btn btn-primary',
						),
					
					array(
						'icon'=>'fa fa-users',
						'title'=>'user follow',
						'href'=>$this->CONTROLLER.'/follow',
						'class'=>'btn btn-primary',
						),
					array(
						'icon'=>'fa fa-thumbs-o-up',
						'title'=>'user like',
						'href'=>$this->CONTROLLER.'/like',
						'class'=>'btn btn-primary',
						),
					array(
						'icon'=>'fa fa-heart',
						'title'=>'gift',
						'href'=>$this->CONTROLLER.'/gift',
						'class'=>'btn btn-primary',
						),
					),
				'toolsDel'=>array(
						'post'=>$this->CONTROLLER.'/delete',
					   'table'=>$this->TABLE,				
					)
				);
		$data['str_search']=$str_search;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/user/table',$data);
	}

	public function get_editrole($user_id){

		$result_data =\DB::table($this->TABLE)->where('id',$user_id)->first();
		$arr_select = Userrole::queryselect('idol');
		$title= 'Edir role';
			$sidebar = array(
				'sidebar'=>''							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>'insert')				
				);
			$button =array(							
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
				);
			$form =array(
				//'post'=>$this->CONTROLLER.'/???',
				'success'=>$this->CONTROLLER.'/profile/'.$user_id,
				'files'=>'1',
				'rule'=>array(
					'role' =>'required',
					),
				'type'=>'edit',
				'data'=>$result_data	,
				'table'=>$this->TABLE,
				'input'=>array(
					'select'=>array(
						'role'=>$arr_select,
						),
					)
				);

		
		$data['form']=$form;
		$data['sidebar']=$sidebar;
		$data['title']=$title;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_form',$data);
	}
	
	public function get_profile($user_id){
		$result_data =User::queryprofile($user_id);
		$result_data->gender = \Myfunction::returngender($result_data->gender);
		$result_data->birthdate = \Formate::format_display($result_data->birthdate);


		/*-------head ---------*/		
			$title= $this->TITLE.' '.$result_data->username.' : # '.$user_id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-1'							
			);
			$button =array(
				array(
					'icon'=>'fa fa-thumbs-o-up',
					'title'=>'user like',
					'href'=>$this->CONTROLLER.'/like/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-heart',
					'title'=>'gift',
					'href'=>$this->CONTROLLER.'/gift/'.$user_id,
					'class'=>'btn btn-primary',
					),				
				array(
					'icon'=>'fa fa-users',
					'title'=>'user follow',
					'href'=>$this->CONTROLLER.'/follow/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon'=>'fa fa-file-text-o',
					'title'=>'Profile',
					'href'=>$this->CONTROLLER.'/profile/'.$user_id,
					'class'=>'btn btn-primary',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => ' ย้อนกลับ',
					),

			);
		/*------profile--------*/
		
			$profile = array(
				'title' =>$result_data->firstname.' '.$result_data->lastname.' ('.$result_data->users_role.') <a href="'.url($this->CONTROLLER.'/editrole/'.$user_id).'"><i style="color:black;" class="fa fa-cog" aria-hidden="true"></i></a>',
				'subtitle' =>'<i class="fa fa-mobile" aria-hidden="true"></i> : '.$result_data->phone,
				
				'picture'=>$result_data->picture,
				'label' =>array(
					'อีเมล'=>'email',
					'เพศ'=>'gender',
					'คะแนน'=>'score',
					'วันเกิด'=>'birthdate',
				
					//'หมายเหตุ'=>array('value'=>'sdf'),
	
				 ),
				
				'data' =>$result_data ,
				);
			
		
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_profile',$data);
	}
}
?>