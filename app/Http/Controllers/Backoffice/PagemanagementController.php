<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Page;
use App\Models\Pagemeta;
use Debug;
use SubmitImage;
 
class PagemanagementController extends Controller {

	public $TITLE = 'หน้าเว็บไซต์';
	public $CONTROLLER = 'pagemanagement';
	public $TABLE = 'page';

	public function __construct()
	{
		$this->middleware('backoffice');
	}

	public function post_submit(){
			
		$arr_post = \Input::all();
		
		$arr_post['page_id']=$arr_post['data']['page_id'];
		unset($arr_post['data']['page_id']);

		$validator = \Validator::make($arr_post['data'],$arr_post['rule']);
			if($validator->fails()){					
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
			}			
     	// file
     	if(isset($arr_post['file'])){
			foreach ($arr_post['file'] as  $value) {
				if(isset($arr_post['data'][$value])&&is_object($arr_post['data'][$value])){		
					$file =$arr_post['data'][$value];	
					if (\File::exists($file)) {	
							$arr_post['data'][$value]=SubmitImage::insert($file);
							SubmitImage::delete($arr_post['old']['file'][$value]);
						
					}						
				}else{  //หากไม่มีUpdate file  ให้ลบ input::file ทิ้ง ไม่ให้ update ใน database
					unset($arr_post['data'][$value]);
				}				
			}
		}
	
		//--------------------
		// data
		foreach ($arr_post['data'] as $key => $value) {
			Pagemeta::where('page_id',$arr_post['page_id'])
				->where('meta_key', $key)
				->update(array('meta_value' =>$value ));
			
		}
		\Session::flash('message', 'Success');
		return \Redirect::back();
		//--------------------
	}
	public function get_about(){
		$arr_page=Page::getpage('about');
	

		$obj_data = new \stdClass;
		foreach ($arr_page['object'] as $key_object => $value_object) {
			$obj_data->$key_object = $value_object;
		}
		
		$title= 'information : About';
		
			$breadcrumb = array(
				
				array('url'=>'#','title'=>'about')				
				);
			
			$form =array(
				'post'=>$this->CONTROLLER.'/submit',
				'success'=>$this->CONTROLLER.'/view',
				'files'=>'1',
				'rule'=>array(		    									
					'block_1_title' =>'required',
					'block_1_description' => 'required',
					'block_1_image' => 'max:1500|image',
					'block_2_title' =>'required',
					'block_2_description' => 'required',
					'block_2_image' => 'max:1500|image',
					),
				'data'=>$obj_data,
				);	
	
		$data['page_id']=$arr_page['id'];
		$data['form']=$form;
		$data['array']['table']=$this->TABLE;						
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;

		return view('backoffice/page/about_form',$data);
	}
	public function get_service(){
		$arr_page=Page::getpage('services');
	

		$obj_data = new \stdClass;
		foreach ($arr_page['object'] as $key_object => $value_object) {
			$obj_data->$key_object = $value_object;
		}

		$title= 'information : Services';
		
			$breadcrumb = array(
				
				array('url'=>'#','title'=>'service')				
				);
			
			$form =array(
				'post'=>$this->CONTROLLER.'/submit',
				'success'=>$this->CONTROLLER.'/view',
				'files'=>'1',
				'rule'=>array(
					'block_0_title' =>'required',
					'block_0_description' => 'required',							
					'block_1_title' =>'required',
					'block_1_description' => 'required', 
					'block_2_title' => 'required',
					'block_2_description' => 'required',
					'block_3_title' => 'required',
					'block_3_description' => 'required',
					'block_4_description' => 'required',
					),
				'data'=>$obj_data,
				);	
	
		$data['page_id']=$arr_page['id'];
		$data['form']=$form;
		$data['array']['table']=$this->TABLE;						
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;

		return view('backoffice/page/services_form',$data);
	}
	public function get_header($page_slug){
		$arr_page=Page::getpage($page_slug);
		
		$obj_data = new \stdClass;
		$obj_data->header_background =  $arr_page['object']['header_background'];
		$obj_data->header_title =  $arr_page['object']['header_title'];
		$obj_data->header_description =  $arr_page['object']['header_description'];
		
		$title= 'Header : '.$obj_data->header_title;
		
			$breadcrumb = array(
				//array('url'=>'#','title'=>$this->TITLE),
				);
			
			$form =array(
				'post'=>$this->CONTROLLER.'/submit',
				'success'=>$this->CONTROLLER.'/header/'.$page_slug,
				'files'=>'1',
				'rule'=>array(		    									
					'header_title' =>'required',
					'header_description' => 'required',
					'header_background' => 'max:1500|image',
				),
				'type'=>'edit',	
				'data'=>$obj_data,
				'input'=>array(
					'hidden'=>array('page_id'=>$arr_page['id']),
					'text' => array('header_title'),					
					'textarea'=>array('header_description'),
					'image'=> array('header_background'=>asset($obj_data->header_background)),
							)
						);			
		$data['form']=$form;
		$data['array']['table']=$this->TABLE;						
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;

		return view('backoffice/page/page_header',$data);
	}
	public function get_index(){
		$arr_page=Page::getpage('about');
	
		$title= 'Home';
		
			$breadcrumb = array(
				array('url'=>'#','title'=>$this->TITLE),
				array('url'=>'#','title'=>'About')				
				);
			
			$form =array(
				'post'=>$this->CONTROLLER.'/edit',
				'success'=>$this->CONTROLLER.'/view',
				'files'=>'1',
				'rule'=>array(		    									
					'block_title_1' =>'required',
					'block_title_2' => 'required',
					'block_image' => 'max:1500|image',
					'block_description'=>'required',
				),
				'slug'=>'slug',
				'data'=>$arr_page['object'],
				'input'=>array(
					'text' => array('block_title_1','block_title_2','product_code','subtitle','size_remark','price','discount'),					
					'textarea'=>array('block_description'),
					'image'=> array('block_image'=>asset($arr_page->picture)),
							)
						);			
		$data['form']=$form;
		$data['array']['table']=$this->TABLE;						
		$data['title']=$title;
		$data['breadcrumb']=$breadcrumb;

		return view('backoffice/page/page_form',$data);
	}
	
}
?>