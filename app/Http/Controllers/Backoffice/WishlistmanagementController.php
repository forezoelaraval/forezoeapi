<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Models\Category;
use App\Models\User;
use App\Models\Product;
use App\Models\Productgallery;
use App\Models\Wishlist;



use Formate;
use Debug;
use Session;
use SubmitImage;
use Illuminate\Support\Str;

class WishlistmanagementController extends Controller {

	public $TITLE = 'wishlist';
	public $CONTROLLER = 'wishlistmanagement';
	public $TABLE = 'wishlist';
	public $NAV = 'nav-wishlist';

	public function __construct()
	{
		$this->middleware('backoffice');
	}
	
	public function get_index(){
		return redirect($this->CONTROLLER.'/home');
	}

	public function get_home($category_id=''){
		
		$result_data  =Wishlist::groupquery(); 
		$arr_topten  =Wishlist::groupquery(10);
		
		/*-------head ---------*/
			$title= 'Wishlist';
			$breadcrumb = array(
				array('url'=>$this->CONTROLLER,'title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				);
		
		$data['title']=$title;
		$data['arr_topten']=$arr_topten;
		$data['result_data']=$result_data;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/wishlist/wishlist',$data);
	}
	public function get_customer($user_id){
		$arr_users =  User::where('id',$user_id)->first();
		$result_data  =Wishlist::queryproductback($user_id); 
		/*-------head ---------*/
			$title= 'Wishlist ลูกค้า : '.$arr_users->name;
			$breadcrumb = array(
				array('url'=>$this->CONTROLLER,'title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(				
				array(
					'icon' => 'fa fa-plus',
					'href' => $this->CONTROLLER.'/insert',
					'title' => 'เพิ่ม',
					),				
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('Picture'=>'picture','Title'=>'title'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-pencil-square-o',
						'title'=>'Edit',
						'href'=>'productmanagement/view/',
						'class'=>'btn btn-primary',
						),
					),
				
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['table']=$table;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/template_table',$data);
	}
	public function get_wishlist($product_id){
		$arr_product  =Product::where('id',$product_id)->first(); 
		$result_data  =Wishlist::queryuser($product_id); 
		/*-------head ---------*/
			$title= 'Wishlist : product : '.$arr_product->title;
			$breadcrumb = array(
				array('url'=>$this->CONTROLLER,'title'=>$this->TITLE)
				);
			
			$sidebar = array(
				'sidebar'=>''							
				);
			$button =array(	
				array(
					'icon' => 'fa fa-reply',
					'href' => asset('productmanagement/view/'.$product_id),
					'title' => 'ย้อนกลับ',
					),			
				);
		/*-------table ---------*/
			$table = array( 
				'thead' => array('name'=>'name','email'=>'email'),
				'tbody' =>$result_data,
				'tools'=>array(
					array(
						'icon'=>'fa fa-file-text-o',
						'title'=>'View',
						'href'=>'customermanagement/profile',
						'class'=>'btn btn-primary',
						),
					),
				
				);
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		$data['table']=$table;
		return view('backoffice/wishlist/table',$data);

	}
	
	
	
	
	public function get_view($id){
		
		$arr_product =Product::where('id',$id)->first();
		$arr_category =Category::select('title')->where('id',$arr_product->category_id)->first();
		$arr_product_gallery =Productgallery::where('product_id',$id)->get();
		$arr_description = json_decode($arr_product->description_attribute);
		
		/*-------head ---------*/		
			$title= $this->TITLE.' : # '.$id;
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>'#','title'=>$title)
				);
			$sidebar = array(
				'sidebar'=>'nav-product-2'							
			);
			$button =array(				
				array(
					'icon' => 'fa fa-pencil-square-o',
					'href' => $this->CONTROLLER.'/edit/'.$id,
					'title' => 'แก้ไข',
					),
				array(
					'icon' => 'fa fa-heart',
					'href' => asset('productmanagement/wishlist/'.$id),
					'title' => 'Wishlist',
					),
				array(
					'icon' => 'fa fa-picture-o',
					'href' => asset('productmanagement/gallery/'.$id),
					'title' => 'Gallery',
					),
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/home',
					'title' => 'ย้อนกลับ',
					),
			);
		/*------profile--------*/
			if($arr_product->active==1){
				$str_status = '(Active)';
			}else{
				$str_status = '(Pendding)';
			}
			$profile = array(
				'title' =>$arr_product->title,				
				'picture'=>SubmitImage::get($arr_product->picture),
				'label' =>array(
					'Slug' =>'slug',
					'รายละเอียด' =>'description'
				 ),
				'data' =>$arr_product ,
				);
			
		
		$data['arr_category']=$arr_category;
		$data['arr_description']=$arr_description;
		$data['arr_product_gallery']=$arr_product_gallery;
		$data['profile']=$profile;
		$data['title']=$title;
		$data['sidebar']=$sidebar;
		
		$data['product_id']=$id;
		
		$data['button']=$button;
		$data['breadcrumb']=$breadcrumb;
		return view('backoffice/product/view',$data);
	}








	/// -------gallery
	public function post_submitproductgallery(){
		$arr_post=\Input::all();
		
		$rule=array(	
			'picture'=>'required|max:1000|image',
		);

		$validator = \Validator::make($arr_post['data'],$rule);
			if($validator->fails()){					
				return \Redirect::back()->withInput($arr_post)->withErrors($validator);
		}	
		$arr_post['data']['picture'] = SubmitImage::insert($arr_post['data']['picture']);
		
		Productgallery::create($arr_post['data']);
		Session::flash('message', 'Success : Upload image to Database Success');
		return \Redirect::back();
	}
	public function post_gallerydel(){
		$result_gallery =Productgallery::where('id',$_POST['id'])->first();
		SubmitImage::delete($result_gallery->picture);
		$boolean = Productgallery::where('id',$_POST['id'])->delete();
		if($boolean){
			Session::flash('message','Product gallery # '.$_POST['id'].' has deleted!.');
			return \Redirect::back();
		}
	}
	public function get_gallery($product_id=''){
			// ----  check ---- 
		//  กรณี insert ครั้งแรก
		$arr_product = Session::get('product');
		
		if(count($arr_product)>0){
			$process['title']='2. Insert gallery !!';
			$process['subtitle']='Product insert progress';
			$process['process_text']='75%';
			$process['process']='75';	
			$process['nextpath']='productmanagement/successproduct/'.$product_id;		
			$data['process']=$process;
		}
	// ----  check ---- 	
			
		$arr_product_gallery =Productgallery::where('product_id',$product_id)->get();	


		$title= 'Product Gallery :#'.$product_id;
			$sidebar = array(
				'subsidebar'=>'nav-product-2'							
			);
			$breadcrumb = array(
				array('url'=>asset($this->CONTROLLER.'/home'),'title'=>$this->TITLE),
				array('url'=>asset($this->CONTROLLER.'/profile/'.$product_id),'title'=>'Product : # '.$product_id),
				array('url'=>'#','title'=>'gallery')			
				);
			if(!isset($process)){
				$button =array(										
				array(
					'icon' => 'fa fa-reply',
					'href' => $this->CONTROLLER.'/view/'.$product_id,
					'title' => 'ย้อนกลับ',
					),
				);
			}else{
				$button=null;
			}
			
		$data['product_id']=$product_id;
		
		$data['arr_product_gallery']=$arr_product_gallery;
		$data['sidebar']=$sidebar;
			$data['title']=$title;
			$data['button']=$button;
			$data['breadcrumb']=$breadcrumb;
		return view('backoffice/product/gallery',$data);
	}
	/// -------gallery
}
?>