<?php namespace App\Http\Controllers;
use Debug;
use SubmitImage;
use App\Models\Product;
use App\Models\Productcategory;
use App\Models\Shop;


class ProductController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function get_singlebrand(){
		/*
			int page : 
			int brand_id 			
		*/
		$page =1;
		if(isset($_GET['page']))
		{
			$page = $_GET['page'];
		}	
		if(isset($_GET['brand_id']))
		{
			$arr_option['shop_id']=  $_GET['brand_id'];
		}	
		$arr_brand =   Shop::find($_GET['brand_id']);
		
		$arr_product =   Product::queryproduct($page ,100,$arr_option);
		$arr_return['brand'] = $arr_brand->toarray();
		$arr_return['product'] = $arr_product->toarray();
		return response()->json(array('status' => 1,'return'=>$arr_return));


	}

	public function get_brand(){
		$arr_brand =  Shop::query();
		$arr_return = $arr_brand->toarray();
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	
	public function get_singleproduct(){
		/*
			int product_id 
		*/

		if(isset($_GET['product_id']))
		{
			$product_id = $_GET['product_id'];
		}	
		$arr_product =  Product::select('title','sub_title','description','picture','coin','shop_id')->find($product_id);
		$arr_brand =  Shop::select('id','title','picture')->find($arr_product->shop_id);
		unset($arr_product->shop_id);

		$arr_return['product'] = $arr_product->toarray();
		$arr_return['brand'] = $arr_brand->toarray();
		
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	public function get_singlecategory(){
		/*
			int page : 
			int category_id
			int brand_id 			
		*/

		$page =1;
		if(isset($_GET['page']))
		{
			$page = $_GET['page'];
		}	

		$category_id =1;
		if(isset($_GET['category_id']))
		{
			$arr_option['category_id']=  $_GET['category_id'];
		}
		if(isset($_GET['brand_id']))
		{
			$arr_option['shop_id']=  $_GET['brand_id'];
		}	

		
		
		$arr_product =  Product::queryproduct($page ,100,$arr_option);
		$arr_return = $arr_product->toarray();
		return response()->json(array('status' => 1,'return'=>$arr_return));


	}
	public function get_all(){
		/*
			int page : 
		*/
		$page =1;
		if(isset($_GET['page']))
		{
			$page = $_GET['page'];
		}	
		$arr_product =  Product::queryproduct($page ,100);
		$arr_return = $arr_product->toarray();
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	public function get_allcategory(){
		$arr_product =  Productcategory::queryall();
		$arr_return = $arr_product->toarray();
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	public function get_type(){
		/*
			varchar  : type : 0 : gift ,
							  1 : electronuc
			int 	 : page : หน้า page
			boolearn : recommend : 1 
		*/
		$recommend ='';
		if(isset($_GET['recommend']))
		{
			$recommend = $_GET['recommend'];
		}

		$page =0;		
		if(isset($_GET['page']))
		{
			$page = $_GET['page'];
		}
		$type ='';
		if(isset($_GET['type']))
		{
			$type = $_GET['type'];
		}	

		$arr_option['recommend']= $recommend;
		$arr_option['type']= $type;
		$arr_product =  Product::queryproduct($page ,100,$arr_option);
		$arr_return = $arr_product->toarray();
		return response()->json(array('status' => 1,'return'=>$arr_return));
	}
	
}
