<?php namespace App\Http\Controllers;

use Facebook\Facebook;
use App\Models\User;
use App\Models\Userprofile;
use Myfunction;


class FacebookController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	
	public function get_index(){
	/*
		input 
			var token 
	*/

		$str_token = $_GET['token'];
		
		 $fb = new Facebook([
        'app_id' => env('FACEBOOK_APP_ID'),
        'app_secret' => env('FACEBOOK_APP_SECRET'),
        'default_graph_version' => 'v2.9',
        ]);
		 try {
			$response =$fb->get('/me?fields=id,name,email,gender', $str_token);
		}catch(\Exception $e) {
		    // When Facebook returns an error   
		    return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
		}

		$me = $response->getGraphUser();
		
		
		$obj_user = User::where('email',$me['email']);

		if($obj_user->get()->count()==0){
		 	$arr_name = explode(' ',$me['name']);
			$arr_user = array(
				'username'=>$arr_name[0],			
				'email'=>$me['email'],	
				'fbappid'=>$me['id'],	
			);
			try {
				$obj_user = User::create($arr_user);
			}catch (\Exception $e) {
				return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
			}	

			$user_id= $obj_user->id;

			$arr_user_profile = array(
				'user_id'=>$user_id,
				'gender'=>Myfunction::checkgender($me['gender']),	
			);
			try {
				Userprofile::create($arr_user_profile);
			} catch (\Exception $e) {
				return response()->json(array('status' => 0,'error'=>array('message'=>$e->getMessage())));
			}	
			
		}else{
			$user_id= $obj_user->first()->id;
		}
		

		// Auth
		if (\Auth::loginUsingId($user_id)) {
			$arr_return = User::getprofile(\Auth::user()->id);
			return response()->json(array('status' => 1,'return'=>$arr_return));	
		}else{
			return response()->json(array('status' => 0,'error'=>array('login'=>'Oops, something wrong happened :( Please try again later!')));
		}
		
	}
	public function post_index(){
		return json_encode(array('status'=>'0','error_message'=>'hi'));
			exit;
		$postdata = file_get_contents("php://input");
   		$arr_post = json_decode($postdata,true);
	}


}
