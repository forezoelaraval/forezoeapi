<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Routing\Route;

class backoffice {

	
	public function handle($request, Closure $next)
	{
		 date_default_timezone_set('Asia/Bangkok');
		if ($request->is('backoffice/login')){

			return $next($request);
		}
		if ($request->is('backoffice/logout')){
			return $next($request);
		}


		if(Auth::check() && Auth::user()->role=='1'){
			return $next($request); 
		}else{
		 	return redirect('backoffice/login');		 
		}
	}

}
