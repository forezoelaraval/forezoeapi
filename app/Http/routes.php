<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controller('home','HomeController');
Route::controller('facebook','FacebookController');
Route::controller('account','AccountController');
Route::controller('user','UserController');
Route::controller('post','PostController');
Route::controller('discover','DiscoverController');
Route::controller('product','ProductController');

Route::controller('backoffice', 'Backoffice\BackofficeController');
Route::controller('usermanagement', 'Backoffice\UsermanagementController');
Route::controller('idolmanagement', 'Backoffice\IdolmanagementController');
Route::controller('shopmanagement', 'Backoffice\ShopmanagementController');
Route::controller('productmanagement', 'Backoffice\ProductmanagementController');
Route::controller('transactionmanagement', 'Backoffice\TransactionmanagementController');


Route::get('/', function(){
	return Redirect::to('home/index');
});