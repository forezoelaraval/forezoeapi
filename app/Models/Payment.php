<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;
class Payment extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'payment';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['user_id', 'coin', 'remark'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function queryview($limit=30){
		$arr_return = self::select('payment.id as id','users.username','user_id','payment.coin', 'payment.remark','payment.created_at')
					->leftjoin('users','users.id','=','payment.user_id');

		return $arr_return->orderby('payment.created_at','DESC')->paginate($limit);
	}
	
}
