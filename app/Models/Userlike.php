<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Userlike extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'users_like';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['user_id','user_like_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function getuser($user_like_id){
		
			$result_data = self::select('user_id as id','username','users_like.created_at')
							->leftjoin('users','users.id', '=', 'users_like.user_id')
							->where('user_like_id',$user_like_id)
							->paginate(30);
			return $result_data;
	}
	
}
