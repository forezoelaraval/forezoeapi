<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username','score','coin','email','password','api_key','role','status','fbappid','fbsecret','picture'];

	// -- others
	protected $defaultprofilepicture = 'assets/img/users/default.jpg';

	
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	
	public static function getAge($birthday){  //yyyy-mm-dd
		
		$then = strtotime($birthday);
		return(floor((time()-$then)/31556926));
		
		
	}

	public static function hasEmail($email){
		
			$sql = self::select('id')							
						->where('email',$email)						
						->first();
			if($sql){
				return true;	
			}else{
				return false;	
			}
	}
	public static function hasUsername($username){
		
			$sql = self::select('id')							
						->where('username',$username)						
						->first();
			if($sql){
				return true;	
			}else{
				return false;	
			}
	}


	public static function getprofile($user_id){



		$arr_user = self::select('username','users_role.title as users_role','picture','bio','email','coin','firstname','lastname','birthdate','phone','gender')
					->leftjoin('users_profile','users.id', '=', 'users_profile.user_id')
					->leftjoin('users_role','users_role.id', '=', 'users.role')
					->where('users.id',$user_id)
					->first();

		if($arr_user){
			$arr_return['username'] = $arr_user->username;
			if($arr_user->picture){
				$arr_return['picture'] = asset($arr_user->picture);
			}else{
				$arr_return['picture'] ='';
			}
			
			$arr_return['user_id'] = $user_id;
			$arr_return['bio'] = $arr_user->bio;
			$arr_return['email'] = $arr_user->email;
			$arr_return['users_role'] = $arr_user->users_role;
			$arr_return['username'] = $arr_user->username;
			$arr_return['coin'] = $arr_user->coin;
			$arr_return['firstname'] = $arr_user->firstname;
			$arr_return['lastname'] = $arr_user->lastname;
			$arr_return['age'] = self::getAge($arr_user->birthdate);
			$arr_return['birthdate'] = $arr_user->birthdate;
			$arr_return['phone'] = $arr_user->phone;
			$arr_return['gender'] = $arr_user->gender;
			return $arr_return;
		}else{
			return null;
		}

		
	}
	public static function queryconutrole($user_category){



		$arr_user = self::select('id')
					->leftjoin('users_role','users_role.id', '=', 'users.role')
					->where('users_role.category',$user_category);

		
		return $arr_user->count();

		
	}


	public static function queryprofile($user_id){



		$arr_user = self::select('username','users_role.title as users_role','score','picture','email','coin','firstname','lastname','birthdate','phone','gender')
					->leftjoin('users_profile','users.id', '=', 'users_profile.user_id')
					->leftjoin('users_role','users_role.id', '=', 'users.role')
					->where('users.id',$user_id)
					->first();

		
			return $arr_user;

		
	}
	public static function queryuser($limit=30,$user_category,$user_role='',$search=''){
		$arr_user =  self::select('users.id','score','username','score','coin','email','users_role.title as users_role','users.created_at')
				->leftjoin('users_profile','users.id', '=', 'users_profile.user_id')
				->leftjoin('users_role','users_role.id', '=', 'users.role')
				->where('users_role.category',$user_category);

			if($search!=''){
				$arr_user =$arr_user
						->where(function ($query) use($search){
				    		$query->where('username','like','%'.$search.'%')
				    			  ->orwhere('firstname','like','%'.$search.'%')
				    			  ->orwhere('lastname','like','%'.$search.'%')
				        		  ->orwhere('email','like','%'.$search.'%');  
						});		
						//
						//->where('firstname','like','%'.$search.'%')
						//->orwhere('lastname','like','%'.$search.'%')
						//->orwhere('username','like','%'.$search.'%')
						//->orwhere('email','like','%'.$search.'%');
			}
		return $arr_user->orderby('users.created_at','DESC')->paginate($limit);
	}
}
