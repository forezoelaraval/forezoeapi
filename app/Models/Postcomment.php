<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Postcomment extends Model {
	
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'post_comment';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['post_id','user_id','comment'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function querycomment($post_id,$page=1,$per=6){
		$sql = self::select('post_comment.id','post_id','post_comment.user_id','comment','username','picture as user_picture','post_comment.created_at')
				->leftjoin('users','users.id', '=', 'post_comment.user_id')
				->where('post_id',$post_id)
				->offset(($page-1)*$per)
				->limit($per)
				->orderBy('post_comment.created_at','desc')
				->get();
 		
		if($sql){
			$arr_return = array();
			foreach ($sql as $key => $value) {
				array_push($arr_return, array(
					'id'=> $value->id,
					'post_id'=> $value->post_id,
					'user_id'=> $value->user_id,
					'comment'=> $value->comment,
					'username'=> $value->username,
					'user_picture'=> $value->user_picture,
					'created_at'=> $value->created_at->format('Y-m-d H:i:s'),
					));
			}
			return $arr_return;
		}
	}
	
}
