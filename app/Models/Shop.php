<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Shop extends Model {

	 /**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'shop';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['user_id', 'title', 'slug','recommend', 'description', 'picture','active'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function query(){
		$arr_shop = self::select('id','title','picture')->orderby('title','ASC')->get();

		
		return $arr_shop;
	}

	public static function getselect(){
		$arr_shop = self::select('id','title')->orderby('title','ASC')->get();

		$arr_return  =array(null=>'please select');
		foreach ($arr_shop as $key_shop => $value_shop) {
			$arr_return[$value_shop->id] = $value_shop->title;
		}
		return $arr_return;
	}

	public static function queryshop($limit=30,$search=''){
		$arr_shop =  self::select('id','user_id', 'title','shop.created_at');

			if($search!=''){
				$arr_shop =$arr_shop
						->where(function ($query) use($search){
				    		$query->where('title','like','%'.$search.'%');
						});		
			}
		return $arr_shop->orderby('shop.created_at','DESC')->paginate($limit);
	}
	
	public static function queryprofile($shop_id){
		$arr_shop = self::select('title','description','username','shop.picture','active','email','firstname','lastname','phone')
					->leftjoin('users','users.id', '=', 'shop.user_id')
					->leftjoin('users_profile','shop.user_id', '=', 'users_profile.user_id')
					->where('shop.id',$shop_id)
					->first();

		
			return $arr_shop;
	}
}
