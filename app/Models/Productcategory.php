<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Productcategory extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_category';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['title', 'description', 'picture'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
	public static function getselect(){
		$result =  self::get();
		
		$arr_return  =array(null=>'please select');
		foreach ($result as $key => $value) {
			$arr_return[$value->id]= $value->title;
		}
		return $arr_return;
	}
	public static function queryall(){
		$result =  self::select('product_category.id','product_category.title','product_category.picture',\DB::raw('count(product.id) as product_count'))

				->leftjoin('product','product.category_id','=','product_category.id')
				->groupBy('product_category.id')
				->get();
		
		
		return $result;
	}
}
