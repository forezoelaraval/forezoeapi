<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Usergift extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'users_gift';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	

	protected $fillable = ['product_id','user_id','user_get_id','remark','order_descption'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public static function queryusergivehistory($user_get_id){
			$arr_user = self::select('product.title as product_title','users.username as username_give','remark','users_gift.user_id as id','users_gift.created_at')
						->leftjoin('users','users.id', '=', 'users_gift.user_id')
						->leftjoin('product','product.id', '=', 'users_gift.product_id')
						->where('users_gift.user_get_id',$user_get_id)
						->orderby('users_gift.created_at','DESC')
						->paginate(30);

			
				return $arr_user;

			
		}

	public static function queryhistory($user_id){
			$arr_user = self::select('product.title as product_title','users.username as username_get','remark','user_get_id','users_gift.created_at')
						->leftjoin('users','users.id', '=', 'users_gift.user_get_id')
						->leftjoin('product','product.id', '=', 'users_gift.product_id')
						->where('users_gift.user_id',$user_id)
						->orderby('users_gift.created_at','DESC')
						->paginate(30);

			
				return $arr_user;

			
		}
	
}
