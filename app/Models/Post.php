<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Post extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */




	protected $table = 'post';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['id','user_id','like','view','post_image','post_text','comment_count','latitude','longitude',];


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function updatecomment($post_id,$type='comment'){
		$sql = self::select('id','comment_count')
				->where('id',$post_id)
				->first();
		if($type=='comment'){
			$sql->comment_count =($sql->comment_count)+1;	
		}else{
			$sql->comment_count =($sql->comment_count)-1;	
		}
		
		$sql->update();

	}

	public static function updatelike($post_id,$type='like'){
		$sql = self::select('id','like')
				->where('id',$post_id)
				->first();
		if($type=='up'){
			$sql->like =($sql->like)+1;	
		}else{
			$sql->like =($sql->like)-1;	
		}
		
		$sql->update();

	}
public static function querymypic($user_id,$page=1,$per=100){
		$sql = self::select('post.id as post_id','like','view','comment_count','post.user_id as user_post_id','username as user_username_post','picture as user_picture','post_image','post_text','post.created_at')
				->leftjoin('users','users.id', '=', 'post.user_id')
				->where('post.user_id',$user_id)
				->where('post_image','!=','');
		return	$sql->orderBy('post.created_at','desc')
					->offset(($page-1)*$per)
					->limit($per)
					->get();
	
	               
	}
	public static function querymypost($user_id,$page=1,$per=100){
		$sql = self::select('post.id as post_id','like','view','comment_count','post.user_id as user_post_id','username as user_username_post','picture as user_picture','post_image','post_text','post.created_at')
				->leftjoin('users','users.id', '=', 'post.user_id')
				->where('post.user_id',$user_id);
		return	$sql->orderBy('post.created_at','desc')
					->offset(($page-1)*$per)
					->limit($per)
					->get();
	
	               
	}


	public static function queryTimeline($user_id,$filter='0',$latitude,$longitude,$page=1,$per=100,$role_id=4){
		$sql = self::select('post.id as post_id','like','view','comment_count','post.user_id as user_post_id','username as user_username_post','picture as user_picture','post_image','post_text','post.created_at')
				->leftjoin('users','users.id', '=', 'post.user_id');
			switch ($filter) {
				case 1:  //nearby
					break;
				case 2: //favorite
				$sql->leftjoin('users_like','users_like.user_like_id', '=', 'post.user_id')
					->where('users_like.user_id',$user_id);
					break;
				case 3: //follow
				$sql->leftjoin('users_follow','users_follow.user_follow_id', '=', 'post.user_id')
					->where('users_follow.user_id',$user_id);
					break;
				default: //global
				//$sql->where('role',$role_id);
					break;
			}
		return	$sql->orderBy('post.created_at','desc')
					->offset(($page-1)*$per)
					->limit($per)
					->get();
 
                
	}
	public static function updateview($post_id){
		$sql = self::select('id','view')
				->where('id',$post_id)
				->first();
			$sql->view =($sql->view)+1;	
		$sql->update();

	}
	
}
