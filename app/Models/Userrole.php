<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Userrole extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'users_role';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['title', 'slug', 'category', 'remark', 'level'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function queryselect($role_category){
		$result_data = self::select('id','title')
							->where('category',$role_category)
							->orderBy('level','desc')
							->get();
		$arr = array();
		foreach ($result_data as $key => $value) {
			$arr[$value->id] = $value->title;
		}
		return $arr;
		exit;

	}
	
}
