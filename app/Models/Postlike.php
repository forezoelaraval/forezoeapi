<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Postlike extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'post_like';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['user_id','post_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public static function checklike($user_id,$post_id){
		//echo $user_id.','.$post_id;
		$sql = self::select('id')
				->where('user_id',$user_id)
				->where('post_id',$post_id)
				->first();
 		
 		if($sql){
 			return true;
 		}else{
 			return false;
 		}
			
	
 
                
	}
	
	
}
