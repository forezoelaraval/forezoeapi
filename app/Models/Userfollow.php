<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Userfollow extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'users_follow';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['user_id','user_follow_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function getuser($user_follow_id){
		
			$result_data = self::select('user_id as id','username','users_follow.created_at')
							->leftjoin('users','users.id', '=', 'users_follow.user_id')
							->where('user_follow_id',$user_follow_id)
							->paginate(30);
			return $result_data;
	}
	
	
}
