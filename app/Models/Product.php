<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;
class Product extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	protected $table = 'product';	

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */


	protected $fillable = ['title','type','sub_title','recommend','score','sku','coin_raw','slug','active', 'coin', 'shop_id', 'category_id', 'description', 'picture'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */


	public static function queryproduct($page=1,$per=100,$arr_option=array()){

		$sql = self::select('title','id','sub_title','picture','coin');
				if(isset($arr_option['type'])){
					$sql->where('type',$arr_option['type']);
				}
				if(isset($arr_option['recommend'])){
					$sql->where('recommend',$arr_option['recommend']);
				}
				if(isset($arr_option['category_id'])){
					$sql->where('category_id',$arr_option['category_id']);
				}
				if(isset($arr_option['shop_id'])){
					$sql->where('shop_id',$arr_option['shop_id']);
				}
				
				$sql->orderby('product.created_at','DESC');
			return	$sql->offset(($page-1)*$per)
					->limit($per)
					->get();
	
	               
	}


	public static function queryall($limit=30,$product_category='',$search='',$shop_id=''){
		$arr_product = self::select('product.id as id','product.created_at','product.title', 'score', 'coin_raw', 'active','product_category.title as category','product.picture')
				->leftjoin('product_category','product_category.id','=','product.category_id');

			if($search!=''){
				$arr_product =$arr_product
						->where(function ($query) use($search){
				    		$query->where('product.title','like','%'.$search.'%');
						});		
						//
						//->where('firstname','like','%'.$search.'%')
						//->orwhere('lastname','like','%'.$search.'%')
						//->orwhere('username','like','%'.$search.'%')
						//->orwhere('email','like','%'.$search.'%');
			}
			if($shop_id!=''){
				$arr_product->where('shop_id',$shop_id);
			}
		return $arr_product->orderby('product.created_at','DESC')->paginate($limit);
				
	}
	public static function singleproduct($product_id){
		$sql = self::select('product.title','product.sub_title','product.description', 'product.picture','coin','shop_id','shop.title as shop_title', 'shop.picture as shop_picture')
				->leftjoin('shop','shop.id','=','product.shop_id')
				->where('product.id',$product_id)
				->first();
		return $sql;
	}
	public static function queryconut(){




		
		return self::select('id')->count();

		
	}
}
