<?php 
namespace App\Libraries;  
use SubmitImage;
	class Mysubmit
	{
		public static function oldfile($arr_post){
			if(isset($arr_post['file'])){
				foreach ($arr_post['file'] as  $value) {
					if(isset($arr_post['data'][$value])){ // หากมีkey ใหม่ ให้ delete old key
						if(isset($arr_post['old']['file'][$value])){
							SubmitImage::delete($arr_post['old']['file'][$value]);	
						}																		
					}
				}
			}	
		}
			
		public static function file($arr_post){
			if(isset($arr_post['file'])){
				
				foreach ($arr_post['file'] as  $value) {
					if(isset($arr_post['data'][$value])&&is_object($arr_post['data'][$value])){		
						$file =$arr_post['data'][$value];	
						if (\File::exists($file)) {	
							$arr_post['data'][$value]=SubmitImage::insert($file);
						}						
					}else{  //หากไม่มีUpdate file  ให้ลบ input::file ทิ้ง ไม่ให้ update ใน database
						unset($arr_post['data'][$value]);
					}				
				}
			}
			return $arr_post;		
		}
		public static function error_arr($validator)
		{
			$arr_error = $validator->errors()->toArray(); //error ของ form 
			$arr_return_error =array();
			foreach ($arr_error as $key => $value) {
				$arr_return_error[$key]= $value[0];
			}
			return $arr_return_error;
		}
		
	}
?>