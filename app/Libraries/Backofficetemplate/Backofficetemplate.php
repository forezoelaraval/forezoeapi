<?php namespace App\Libraries\Backofficetemplate;
use App\Libraries\Debug;
use Form;
class Backofficetemplate {

	/*
		require jquery
	*/
	public static function testsubmit(){
		Debug::pre($_POST);
	}

	public static function process($array=''){
		if(is_array($array)){
			echo '<h1>'.$array['title'].'</h1>';
				echo '<h4>'.$array['subtitle'].'</h4>';
					echo '<div class="row">';
				    	echo '<div class="col-sm-8">';
				        echo '<div class="progress">';
				            echo '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width:'.$array['process'].'%">'.$array['process_text'].'</div>';
				       	echo '</div>';
			    	echo '</div>';
		    	echo '</div>';
			echo '<hr></hr>';	
		}
		
	}

	public static function form($array=''){
	
	//--- file form 0 =>'',1 =>'enctype="multipart/form-data"'
		$files=0;	
		if(isset($array['files'])){
			$files=$array['files'];
		}

	//---  post : form : action
		$default_post='backoffice/submit';
		if(isset($array['post'])){
			$default_post=$array['post'];
		}

	//---  type form -------  : insert , edit
		$type='insert';
		if(isset($array['type'])){
			$type=$array['type'];
		}

	//---  open Form -------
	echo Form::open(array('url' => asset($default_post), 'files' => $files,'class'=>'form-horizontal'));
		echo Form::hidden('type',$type);

		// ---id  : form  update table by ID
		if(isset($array['data']->id)){
			echo Form::hidden('id',$array['data']->id);
		}

		// ---table  : data table  to insert & update
		if(isset($array['table'])){
			echo Form::hidden('table',$array['table']);				
		}

		// ---success  : befor success submit to redirec :url/id
		if(isset($array['success'])){
			echo Form::hidden('success',$array['success']);				
		}

		// ---rule  : varidate input 
		if(isset($array['rule'])){
			foreach ($array['rule'] as $key => $value) {				
				echo Form::hidden('rule['.$key.']',$value);			
			}
		}

		// --- old input   : array
		if(isset($array['old'])&&is_array($array['old'])){
			foreach ($array['old'] as $key => $value) {				
				echo Form::hidden('old['.$key.']',$value);			
			}
					
		}


		// --- delete input before submit   ex . password_confirm
		if(isset($array['unset'])){
			foreach ($array['unset'] as $key => $value) {					
				echo Form::hidden('unset['.$key.']',$value);	
			}					
		}

		// --- slug : Input name to use Slug auto 
		// ---- old slug : use to check Update slug
		if(isset($array['slug'])){
			echo Form::hidden('slug',$array['slug']);
			if($type=='edit'){
				if(isset($array['data']->$array['slug'])&&$array['data']->$array['slug']!=''){
					echo Form::hidden('old[slug]',$array['data']->$array['slug']);					
				}
			}
		}

		// form 
		foreach ($array['input'] as $key => $input_value) {
			switch ($key) {
				case 'email':
					foreach ($input_value as $key => $value) {  
        				$value_data='';
        				if(isset($array['data']->$value)){        					      					
        					$value_data=$array['data']->$value;
        				}
        				echo '<div id="div_'.$value.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$value.'" class="col-sm-2 control-label">'.ucfirst($value).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				echo Form::email('data['.$value.']',$value_data,array('id'=>$value,'class'=>'form-control','placeholder'=>ucfirst($value)));	
        				echo '</div></div></div>';
        			}
					break;
				case 'password':
					foreach ($input_value as $key => $value) {          			
        				echo '<div id="div_'.$value.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$value.'" class="col-sm-2 control-label">'.ucfirst($value).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				echo Form::password('data['.$value.']',array('class'=>'form-control','placeholder'=>ucfirst($value)));	
        				echo '</div></div></div>';
        			}
					break;
    			case 'text':    				
        			foreach ($input_value as $key => $value) {  
        				$value_data='';
        				if(isset($array['data']->$value)){     					      					
        					$value_data=$array['data']->$value;
        				}
        				echo '<div id="div_'.$value.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$value.'" class="col-sm-2 control-label">'.ucfirst($value).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				echo Form::text('data['.$value.']',$value_data,array('id'=>$value,'class'=>'form-control','placeholder'=>ucfirst($value)));	
        				echo '</div></div></div>';
        			}
        			break;
        		case 'checkbox':    				
        			foreach ($input_value as $key => $value) {  
        				$value_data='';
        				if(isset($array['data']->$value)){     					      					
        					$value_data=$array['data']->$value;
        				}
        				echo '<div id="div_'.$value.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$value.'" class="col-sm-2 control-label">'.ucfirst($value).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				
        				echo Form::checkbox('data['.$value.']','1',$value_data);	
        				echo 'true </div></div></div>';
        				echo Form::hidden('checkbox[]',$value);	
        			}
        			break;
        		case 'textarea':    				
        			foreach ($input_value as $key => $value) {  
        				$value_data='';
        				if(isset($array['data']->$value)){        					      					
        					$value_data=$array['data']->$value;
        				}
        				echo '<div id="div_'.$value.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$value.'" class="col-sm-2 control-label">'.ucfirst($value).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				echo Form::textarea('data['.$value.']',$value_data,array('id'=>$value,'class'=>'form-control','placeholder'=>ucfirst($value)));	
        				echo '</div></div></div>';
        			}
        			break;
        		
    			case 'select':
        			foreach ($input_value as $key => $select) {  
        				$value_data='';
        				if(isset($array['data']->$key)){
        					$value_data=$array['data']->$key;
        				}        			        			
        				echo '<div id="div_'.$key.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$key.'" class="col-sm-2 control-label">'.ucfirst($key).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				echo Form::select('data['.$key.']',$select,$value_data,array('id'=>$key,'class'=>'form-control'));     			
        				echo '</div></div></div>';
        			}        		
        			break;
        		case 'hidden':
        			foreach ($input_value as $key => $value) {         				 
        				echo Form::hidden('data['.$key.']',$value);	        				
        			}        	
        			break;
        		case 'image':    		
        			$i=1;
        			foreach ($input_value as $key => $img_value) {  
        				$value_data='';
        				if(isset($array['data']->$key)){
        					$value_data=$array['data']->$key;
        					echo Form::hidden('old[file]['.$key.']',$value_data);       					      					
        					
        				}
        				echo Form::hidden('file[image_'.$i.']',$key);				
        				echo '<div id="div_'.$key.'" class="row"><div class="form-group">';
        				echo '<label id="label_'.$key.'" class="col-sm-2 control-label">'.ucfirst($key).'</label>';
        				echo '<div id="input_'.$value.'" class="col-sm-6">';
        				echo '<div class="single_img" id="img_'.$key.'">';
        				if($type=='edit'){  
        					echo '<img class="img-rounded" style="max-width:66%;" src='.$img_value.'>';
        				}
        				echo '</div>';
        				echo '<hr/>';
        				echo Form::file('data['.$key.']',array('id'=>$key,'onchange'=>'handleFiles(this.id,\'img_'.$key.'\')','accept'=>'image/x-png, image/gif, image/jpeg'));
        					
        				echo '</div></div></div>';
        				$i++;
        			}
        			break;
        
			}
			
			//Debug::pre($value);
		}
		echo '<hr></hr>';
		echo '<div class="row"><div class="form-group">';        				
        echo '<div class="col-sm-offset-2 col-sm-6">';        
        echo Form::submit('ตกลง',array('class'=>'btn btn-primary'));				
        echo '</div></div></div>';
		echo Form::close();
	}

	public static function rightbutton($array=''){
		if(is_array($array)){
  			foreach ($array as $value){
  				$btnclass = "btn-default";
  				if( $value['title'] == 'Edit' ) $btnclass = "btn-primary";
                echo "<a href='".asset($value["href"])."' class='btn ".$btnclass." pull-right' ><i class='".$value['icon']."'></i> ".ucfirst($value['title'])."</a>";
             }
		}
	}
	public static function profile($array=''){
		if(is_array($array)){
			 require_once('profile.php');

		}

	}	

	public static function breadcrumb($array=''){
		if(is_array($array)){
			echo '<ul class="breadcrumb">';
			foreach ($array as $value) {
				echo '<li><a href="'.asset($value['url']).'">'.ucfirst($value['title']).'</a></li>';			
			}
			echo '</ul>';
		}
		else
		{
			echo '<ul class="breadcrumb">';
			echo '</ul>';
			return false;
		}
	}
	
	public static function activeSidebar($array=''){
			
			if(isset($array['sidebar'])){				
				echo '<script> $("#'.$array['sidebar'].'").addClass("active");</script>';							
				echo '<script> $("#'.$array['sidebar'].'-a").addClass("active");</script>';							
			}
			if(isset($array['subsidebar'])){				
				
				echo '<script>$("#'.$array['subsidebar'].'").addClass("active"); </script>';							
			}
			//if(isset($array['']))
		
	}
	
	
	
	public static function table($array=''){

		//	Debug::pre($array);
		if(is_array($array)){
			$boo_toolsDel =0;
			$boo_tools =0;			
			$boo_paginate=0;  
			if(isset($array['tools'])){	 //check has tools
				$boo_tools =1;			
			}
			if(isset($array['toolsDel'])){  //check has toolsDel
				$boo_toolsDel =1;
				self::modaldel($array['toolsDel']);
			}
			if(method_exists($array['tbody'],'currentPage')){  //check is paginate 
				$boo_paginate =1;
			}

			echo '<br/><table class="table" >';
			/* ---- thead ----*/  
				echo ' <thead><tr><th>#</th>';
				foreach ($array['thead'] as $key => $value) {
					echo '<th>'.$key.'</th>';
				}
				 if($boo_tools||$boo_toolsDel){
					echo '<th width="250px"></th>';	
				}
				echo ' </tr></thead>';

			/* ---- tbody ----*/
				echo ' <tbody>';
				foreach ($array['tbody'] as $index => $value_tbody) {


					if($boo_paginate){
						$i =$index+1+(($array['tbody']->currentPage()-1)*$array['tbody']->perPage());
					}else{
						$i = $index+1;
					}

					// active class
					$class='';
					if(isset($array['tr_active'])){
						if($value_tbody->$array['tr_active']['field']==$array['tr_active']['active']){
							 $class=$array['tr_active']['class'];
						}
					}

					echo '<tr class="'.$class.'"><td>'.$i.'</td>';
                    foreach ($array['thead'] as  $value_thead){
                    	echo '<td  class="filed_'.$value_thead.'">';
                        if($value_thead=='picture'){
                            echo '<img src="'.asset($value_tbody->$value_thead).'" style="width:350px;">' ;
                        }else{
                            echo nl2br($value_tbody->$value_thead);
                        }
                        echo '</td>';                                            
                    }	
                    if($boo_tools||$boo_toolsDel){
                    	echo '<td>';
                    	if($boo_tools ){                 			
							self::tabletools($array['tools'],$value_tbody->id);
						}  
						if($boo_toolsDel){
							self::tabletoolsdel($value_tbody->id);							
						}	
						echo '</td>';
                    }
                 	
				
			}
			echo '</tbody></table>';
			if($boo_paginate){
				echo '<div align="center">'.str_replace('/?', '?',$array['tbody']->render()).'</div>';
			}
		}
	}
	public static function tabletools($array='',$id=''){
		if(is_array($array)){
			//Debug::pre($array);
			
			foreach ($array as $value){
				$target_blank='';
				if(isset($value['target_blank'])){
					$target_blank="target='_blank'";
				}
				echo "<a ".$target_blank." title='".$value['title']."'  href='".asset($value['href'])."/".$id."'   class='".$value['class']."'><i class='".$value['icon']."'></i></a> ";
			}				
			
		}
		
	}

	/*
		method :tabletoolsdel
			'table'=
			'message'=
			'post'=
			'input'=array()
	*/
	public static function tabletoolsdel($id=''){

		echo "<button onclick='set_model_del(".$id.");' type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteModal'><i class='fa fa-trash-o'></i></button>";
	}
	public static function modaldel($array){
		
		$default_post='backoffice/del';
			if(isset($array['post'])){
   			$default_post=$array['post'];
		}
		 include('model-del.php');
 	
	}
}
?>