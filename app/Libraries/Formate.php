<?php namespace App\Libraries;  

	class Formate
	{	
		
		public static function error_arr($validator)
		{
			$arr_error = $validator->errors()->toArray(); //error ของ form 
			$arr_return_error =array();
			foreach ($arr_error as $key => $value) {
				$arr_return_error[$key]= $value[0];
			}
			return $arr_return_error;
		}
		public static function gen_breadcrumb($arr_breadcrumb=''){
			if(count($arr_breadcrumb)!=0){
				echo ' <ol class="breadcrumb">';
				foreach ($arr_breadcrumb as $key_breadcrumb => $value_breadcrumb) {
					echo ' <li><a href="'.asset($value_breadcrumb).'">'.$key_breadcrumb.'</a></li>';
				}
				echo ' </ol>';
			}
		}

	
		public static function delptag($string){
			return $string = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $string);
		}
		public static function error_arr_breadcrumb($validator)
		{
			$arr_error = $validator->errors()->toArray(); //error ของ form 
			$arr_return_error =array();
			foreach ($arr_error as $key => $value) {
				$arr_return_error[$key]= $value[0];
			}
			return $arr_return_error;
		}
		public static function seturl($get)
		{
			$url = '?';
			$i=0;
			foreach ($get as $key => $value) {
				if($i==0){
					$url=$key.'='.$value;	
				}else{
					$url=$url.'&'.$key.'='.$value;	
				}
				
				$i++;
			}
			return $url;
		}
		public static function setday($day){
			return date_format($day , 'd/m/Y');
		}
		public static function board_date($date)
		{
			if(date("Y-m-d", strtotime($date))==date('Y-m-d')){
				return 'วันนี้  '.date("H:i", strtotime($date));
			}
			else{
				return date("M j Y,  H:i", strtotime($date));
			}
		}
		public static function format_database($date)
		{
			
			//  dd/mm/YYYY  --->   YYYY-mm-dd
			$return = explode("/",$date);
			// Debug::pre($return);
			return $return[2].'-'.$return[1].'-'.$return[0];
		}
		public static function format_display($date)
		{
			//YYYY-mm-dd --->  dd/mm/YYYY
			$return = explode("-",$date);
			// Debug::pre($return);
			return $return[2].'/'.$return[1].'/'.$return[0];
		}
		public static function dateDiff($date1, $date2) {
           
	$diff = abs(strtotime($date2) - strtotime($date1)); 
	$years   = floor($diff / (365*60*60*24)); 
	$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
	$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 
	$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
	$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 
	return array(
		"years" => $years,
		"months" => $months,
		"days" => $days,
		"hours" => $hours,
		"minuts" => $minuts,
		"seconds" => $seconds
	);
	}
}
?>