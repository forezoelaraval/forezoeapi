<?php namespace App\Libraries; 

	class S3 {

		/**
		 * Upload the image to Amazon S3 server
		 * @param  file $file   input file sent by <form>
		 * @param  bucketname $bucket Default bucket name is defined in libraries/S3.php
		 * @return string $key
		 */
		public static function insert($file, $bucket = '')
		{
			$key = rand(1000,9999).'_'.date('YmdHis');
			$s3 = \AWS::get('s3');
			$result = $s3->putObject(array(
				'Bucket' => empty($bucket) ? env("S3_BUCKET") : $bucket,
				'Key' => $key,
				'SourceFile' =>$file,
				'ACL' => 'public-read'
			));
			// \Debug::pre($result);
			if($result) return $key;
			else return false;
			//return $result['ObjectURL'];
		}
		public static function mock(){
			$s3 = \AWS::get('s3');
			$result = $s3->putObject(array(
				'Bucket' => empty($bucket) ? env("S3_BUCKET") : $bucket,
				'Key' => 'test',
				'SourceFile' => public_path().'/upload/s3test/test2.jpg',
				'ACL' => 'public-read'
			));
			\Debug::pre($result);

			$s3 = \AWS::get('s3');
			$result = $s3->putObject(array(
				'Bucket' => empty($bucket) ? env("S3_BUCKET") : $bucket,
				'Key' => '1491_20150620164935',
				'SourceFile' => public_path().'/upload/s3test/bg.jpg',
				'ACL' => 'public-read'
			));
			\Debug::pre($result);
		}
		public static function test()
		{
			$s3 = \AWS::get('s3');
			$result = $s3->putObject(array(
				'Bucket' => empty($bucket) ? env("S3_BUCKET") : $bucket,
				'Key' => 'test',
				'SourceFile' => public_path().'/upload/s3test/test2.jpg',
				'ACL' => 'public-read'
			));
			\Debug::pre($result);
			//echo $result['ObjectURL'];
		}

		/**
		 * Get object URL from the given key
		 * @param  string $key    key of the object
		 * @param  string $bucket name of the bucket
		 * @return string         Amazon S3 url of the object
		 */
		public static function get($key, $bucket = '')
		{
			$bucket = empty($bucket) ? (string) env("S3_BUCKET") : $bucket;
			if( strlen($bucket)< 1 || empty($bucket) )
				$bucket = getenv("S3_BUCKET");
			if( strlen($bucket)<1 )
				return null;
			$s3 = \AWS::get('s3');
			$result_url = $s3->getObjectUrl($bucket, $key);
			return $result_url;
		}

		/**
		 * Delete object with specified key from Amazon S3
		 * @param  string $key    object key to be deleted
		 * @param  string $bucket (optional) bucket name
		 * @return boolean         result
		 */
		public static function delete($key, $bucket = '')
		{
			$s3 = \AWS::get('s3');
			$boolean = $s3->deleteObject(array(
    					'Bucket' => empty($bucket) ? env("S3_BUCKET") : $bucket,
   						'Key'    => $key
				));
			return $boolean;
		}
	}
?>