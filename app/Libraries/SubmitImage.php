<?php 

namespace App\Libraries;  

	class SubmitImage
	{
		public static function html_path(){
			
			return base_path().'/public';
		}
		public static function get($image,$default_image=''){
			
			
			$filename =self::html_path().'/'.$image;
			if (\File::exists($filename)) {
				$path =asset($image);
			}
			else{
				$path =asset($default_image);
			}
			return $path;
		}
		public static function insert($file,$path='upload/image'){
			$key = rand(1000,9999).'_'.date('YmdHis');
			
			$extension = $file->getClientOriginalExtension();
			$destinationPath = self::html_path().'/'.$path;
			$filename = rand(100,999).'_'.date('YmdHis').'.'.$extension;
			$result = $file->move($destinationPath, $filename);
			$final_path = $path.'/'.$filename;
			if($result) return $final_path;
			else return false;
		}


		public static function moveimage($file,$path){	

//		exit();
		$extension = $file->getClientOriginalExtension();
		$destinationPath = self::html_path().'/'.$path;
		$filename = rand(100,999).'_'.date('YmdHis').'.'.$extension;
		$result = $file->move($destinationPath, $filename);
		return array('status'=> $result,'path_image'=>$path.'/'.$filename);
	}
	public static function moveimage_notchange($file,$path){
//	Debug::pre($file);	
//	echo $file->getClientOriginalName();
		$extension = $file->getClientOriginalExtension();

		$destinationPath = self::html_path().'/'.$path;
		$filename = $file->getClientOriginalName();
		$result = $file->move($destinationPath, $filename);
		return array('status'=> $result,'path_image'=>$path.'/'.$filename);
	}
	public static function changelocation($oldfile,$path,$chage_name=''){
		if(!$chage_name){					 	
		 	$newfile_path = str_replace("temp_file",$path,$oldfile);
		 	$oldfile = self::html_path().'/'.$oldfile;
		 	$newfile = self::html_path().'/'.$newfile_path;
		}else{
			
		 	$oldfile = self::html_path().'/'.$oldfile;
			$filetype = substr( $oldfile, strrpos($oldfile,'.'));
			 $newfile_path = 'image/'.$path.'/'.rand(1000,9999).'_'.date('YmdHis').$filetype;
		 	 $newfile = self::html_path().'/'.$newfile_path;
		}
		if(File::move($oldfile, $newfile)){
			return $newfile_path;
		}else{
			die('error');
		}
	}
	public static function deleteimageIntable($id,$table,$field='picture'){
		$result_image=0;
		$columns = \Schema::getColumnListing($table);

		if(in_array($field, $columns)){
			$result = \DB::table($table)->select($field)->where('id',$id)->first();
			$filename =self::html_path().'/'.$result->$field;		
			if (\File::exists($filename)) {
			 	$result_image= \File::delete($filename);
			} 
		}
		return $result_image;
	}
	public static function delete($file,$path=''){
		
	 	$destinationPath = self::html_path().'/'.$path;
	 	 $filename = $destinationPath.'/'.$file;
	 	$result=0;
		if (\File::exists($filename)) {
 		  $result= \File::delete($filename);
		} 
		return $result;
	}
		
	}
?>