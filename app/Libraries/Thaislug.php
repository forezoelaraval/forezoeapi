<?php namespace App\Libraries;
	class Thaislug {
		
		public static function url($title,$table,$separator='-')
		{

			$slug = self::slug($title,$separator);
			$slugs=  \DB::table($table)->select('slug')->whereRaw("slug REGEXP '^".$slug."(-[0-9]*)?$'");
			 $slugCount = count($slugs->get());
			if($slugCount===0){
				return $slug;
			}else{
				$lastSlugNumber = intval(str_replace($slug . '-', '', $slugs->orderBy('slug', 'desc')->first()->slug));
				if($lastSlugNumber==0){
					return $slug.'-1';
				}
				return $slug.'-'.($lastSlugNumber + 1);
			}
			
		}

		public static function slug($title,$separator='-')
		{
			$flip = $separator == '-' ? '_' : '-';

			$title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

		// Remove all characters that are not the separator, letters, numbers, or whitespace.
			$title = preg_replace('![^'.preg_quote($separator).'ก-๙\pL\pN\s]+!u', '', mb_strtolower($title));

		// Replace all separator characters and whitespace by a single separator
			$title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

			return trim($title, $separator);
		}
	}
    
   
?>